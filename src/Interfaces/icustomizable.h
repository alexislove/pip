/*!
 *\file icustomizable.h
 *\brief Customizable objects interface definition
 */

#ifndef INTERFACES_ICUSTOMIZABLE_H
	#define INTERFACES_ICUSTOMIZABLE_H

	#include "../Data/namedvariantmap.h"

	namespace Interfaces
	{
		/*!
		 *\class ICustomizable
		 *\brief This class is an interface for all customizable objects.
		 *\			 Customization is performed using NamedVariantMap, where most of necessary params
		 *\			 can be passed.
		 *\			 Typically format of params can be:
		 *\			 NamedVariantMap params;
		 *\			 params["radius"]							= ExtendedVariant(QVariant(radius));
		 *\			 params["someseveralfactors"] = ExtendedVaraint(ExtendedVariant::VariantList() << QVariant(someParam1)
		 *\																																										 << QVariant(someParam2));
		 *\			 someFilter->setParameters(params);
		 */
		struct ICustomizable
		{
			/*!
			 *\brief Destructor
			 */
			virtual ~ICustomizable() = 0
			{

			}

			/*!
			 *\brief Implement this method to set desired params for the object
			 *\param params Desired params
			 */
			virtual void setParameters(const Data::NamedVariantMap& params) = 0;

			/*!
			 *\brief Implement this methods to set default params for the object
			 */
			virtual void setDefaultParameters() = 0;

			/*!
			 *\brief Implement this method to get params of the object
			 *\return All params
			 */
			virtual const Data::NamedVariantMap& getParameters() const = 0;
		};
	} // namespace Interfaces

#endif