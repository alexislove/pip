/*!
 *\file iimagecontext.h
 *\brief Image context interface definition
 */

#ifndef INTERFACES_IIMAGECONTEXT_H
	#define INTERFACES_IIMAGECONTEXT_H

	#include <QSharedPointer>
	#include <QString>
	#include <QStringList>

	#include "../data/imagecontextstate.h"

	#include "ifilter.h"

	class  QPainter;
	class	 QUndoCommand;

	namespace Interfaces
	{
		/*!
		 *\class IImageContext
		 *\brief This class represents context for working with images
		 *\			 throughout the application's user interface. It performs the abstraction layer
		 *\			 between common image and widgets that do use it.
		 */
		struct IImageContext
		{
			/*!
			 *\brief Destructor (for proper sub classing)
			 */
			virtual ~IImageContext() = 0
			{

			}

			/*!
			 *\brief Image loading routine
			 *\param fileName Name of the image file
			 *\return True, in case data loading succeeded
			 */
			virtual bool load(const QString& fileName) = 0;

			/*!
			 *\brief Image saving routine. You can use it both for save and save as operations
			 *\param fileName Name of the image file to save
			 *\return True, if save operation completed
			 */
			virtual bool save(const QString& fileName) = 0;

			/*!
			 *\brief Apply command to context. Command will be pushed into internal undo/redo commands stack and redo will be immediately called
			 *\param command Command instance
			 */
			virtual void applyCommand(QUndoCommand* command) = 0;

			/*!
			 *\brief Redo top stack's command
			 */
			virtual void redo() = 0;

			/*!
			 *\brief Undo top stack's command
			 */
			virtual void undo() = 0;

			/*!
			 *\brief Only removes image
			 */
			virtual void releaseImage() = 0;

			/*!
			 *\brief Clear current context. This method will remove image and clear history
			 */
			virtual void clean() = 0;

			/*!
			 *\brief Apply given filter to the image 
			 *\param filter Filter interface pointer
			 */
			virtual void applyFilter(IFilterPointer filter) = 0;

			/*!
			 *\brief Render image using given QPainter instance. For example to show image on the widget
			 *\param painter Painter instance
			 */
			virtual void draw(QPainter* painter) const = 0;

			/*!
			 *\brief Check, whether any modifications was done
			 *\return True, if image is modified
			 */
			virtual bool isDirty() const = 0;

			/*!
			 *\brief Get name of the currently opened image
			 *\return Image file name, or empty string if nothing is opened
			 */
			virtual const QString& getCurrentImageFile() const = 0;

			/*!
			 *\brief Check, whether redo can be done
			 *\return True, in case forward action is available
			 */
			virtual bool canRedo() const = 0;

			/*!
			 *\brief Check, whether undo can be done
			 *\return True, in case reverse action is available
			 */
			virtual bool canUndo() const = 0;

			/*!
			 *\brief Get commands history. 
			 *\return List of command names, that were sequentially applied to context
			 */
			virtual QStringList getHistory() const = 0;

			/*!
			 *\brief Change context's state to the way it was after command with given index
			 *\			 If index is invalid, nothing will be done
			 *\param index Desired command index
			 */
			virtual void setCommandIndex(int index) = 0;

			/*!
			 *\brief Get index of current command in the stack
			 *\return Current index in the commands stack
			 */
			virtual int getCurrentCommandIndex() const = 0;

			/*!
			 *\brief Get current image context state
			 *\return Current state
			 */
			virtual Data::ImageContextStatePointer getCurrentState() const = 0;

			/*!
			 *\brief Update image context state with
			 *\param state State instance
			 */
			virtual void updateState(Data::ImageContextStatePointer state) = 0;
		};

		typedef QSharedPointer< IImageContext > IImageContextPointer;
	} // namespace Interfaces

#endif // INTERFACES_IWORKINGCONTEXT_H