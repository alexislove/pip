/*!
 *\file iserializable.h
 *\brief Serializable interface definition
 */

#ifndef INTERFACES_ISERIALIZABLE_H_
	#define INTERFACES_ISERIALIZABLE_H_

	class QIODevice;

	namespace Interfaces
	{
		/*!
		 *\class ISerializable
		 *\brief Java-like serializable interface
		 */
		struct ISerializable
		{
			/*!
			 *\brief Destructor
			 */
			virtual ~ISerializable() = 0
			{

			}

			/*!
			 *\brief Serialize object to given IO device
			 *\param device IO device instance
			 */
			virtual void write(QIODevice* device) const = 0;

			/*!
			 *\brief Deserialize object from given IO device
			 *\param device IO device instance
			 */
			virtual void read(QIODevice* device) = 0;
		};
	} // namespace Interfaces

#endif // INTERFACES_ISERIALIZABLE_H_