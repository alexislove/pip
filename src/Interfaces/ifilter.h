/*!
 *\file ifilter.h
 *\brief Filter interface
 */

#ifndef INTERFACES_IFILTER_H
	#define INTERFACES_IFILTER_H

	#include <QSharedPointer>

	#include "../data/safeimage.h"

	class QString;

	namespace Interfaces
	{
		/*!
		 *\class IFilter
		 *\brief Basic image filters interface
		 */
		struct IFilter
		{
			/*!
			 *\brief Destructor
			 */
			virtual ~IFilter() = 0
			{
			}

			/*!
			 *\brief Apply filter to the image. To identify, that image won't be changed during the filtering operation
			 *\			 it's passed by reference
			 *\param image Image instance
			 *\return Filtered image pointer
			 */
			virtual Data::SafeImagePointer apply(const Data::SafeImage& image) = 0;

			/*!
			 *\brief Get name of the filter
			 *\return Unique name
			 */
			virtual const QString& getName() const = 0;

			/*!
			 *\brief Get filter identifier name. Filter identifier is unique latin name, that describes the filter
			 *\			 and is placed in the registry, don't mess up with the name, that is given for user, to know what he's doing
			 *\return ASCII string - filter unique name
			 */
			virtual const char* getIdName() const = 0;
			 
		};

		typedef QSharedPointer< IFilter > IFilterPointer;
	} // namespace Interfaces

#endif // INTERFACES_IFILTER_H