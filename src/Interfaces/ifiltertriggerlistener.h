/*!
 *\file ifiltertriggerlistener.h
 *\brief Filter trigger listener interface definition
 */

#ifndef INTERFACES_IFILTERTRIGGERLISTENER_H
	#define INTERFACES_IFILTERTRIGGERLISTENER_H

	class QAction;
	class QTreeWidgetItem;

	namespace Interfaces
	{
		struct IFilterTriggerListener
		{
			/*!
			 *\brief Destructor
			 */
			virtual ~IFilterTriggerListener() = 0
			{

			}

			/*!
			 *\brief Handle tree widget item, related to some kind of filter click
			 *\param item Item, that was clicked
			 */
			virtual void onFilterItemClicked(QTreeWidgetItem* item) = 0;

			/*!
			 *\brief Handle action, related to some kind of filter click
			 *\param action Action, that was triggered
			 */
			virtual void onFilterActionTriggered(QAction* action) = 0;
		};
	} // namespace Interfaces
	 

#endif // INTERFACES_IFILTERTRIGGERLISTENER_H