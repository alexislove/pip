/*!
 *\file gridrenderer.cpp
 *\brief This file contains image grid renderer implementation
 */

#include <QPainter>
#include <QRect>

#include "gridrenderer.h"

namespace Tools
{
	//! Default parameters
	static const float  cDefaultWidthStep  = 10.f;							// Unit step per width
	static const float  cDefaultHeightStep = 10.f;							// Unit step per height
	static const QColor cDefaultColor			 = QColor(0, 0, 0); // Black color

	GridRenderer::GridRenderer(float dw, float dh, const QColor& color)
		: mdW(dw),
			mdH(dh),
			mColor(color)
	{

	}

	GridRenderer::~GridRenderer()
	{

	}


	void GridRenderer::draw(QPainter *painter, const QRect& region)
	{
		const float top    = static_cast<float>(region.top());
		const float bottom = static_cast<float>(region.bottom());

		const float left   = static_cast<float>(region.left());
		const float right  = static_cast<float>(region.right());

		QPen pen(QBrush(mColor), 1.f);
		painter->setPen(pen);
		// Render vertical lines
		for (float x = left; x <= right; x += mdW)
		{
			painter->drawLine(QPointF(x, top), QPointF(x, bottom));
		}

		// Render horizontal lines
		for (float y = top; y <= bottom; y += mdH)
		{
			painter->drawLine(QPointF(left, y), QPointF(right, y));
		}
	}


	void GridRenderer::setParameters(const Data::NamedVariantMap& params)
	{
		// Check all params for correctness
		if (params.find("width_step") != params.end())
		{
			mdW = params["width_step"].asVariant().toFloat();
		}
		if (params.find("height_step") != params.end())
		{
			mdH = params["height_step"].asVariant().toFloat();
		}
		if (params.find("color") != params.end())
		{
			mColor = params["color"].asColor();
		}
	}


	void GridRenderer::setDefaultParameters()
	{
		mdW		 = cDefaultWidthStep;
		mdH		 = cDefaultHeightStep;
		mColor = cDefaultColor;
	}

	const Data::NamedVariantMap& GridRenderer::getParameters() const
	{
		Q_ASSERT(! "This method is not implemented!");

		static Data::NamedVariantMap params;
		return params;
	}

} // namespace Tools