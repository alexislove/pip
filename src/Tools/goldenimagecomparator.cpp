/*!
 *\file goldenimagecomparator.h
 *\brief This file contains golden image comparator definition
 */

#include <QSettings>

#include "goldenimagecomparator.h"


namespace Tools
{
	static const char* const GoldenImagesDirSettingsKey = "GoldenImagesDir";

	/**
	 public:
	 */
	GoldenImageComparator::GoldenImageComparator()
		: mGoldenImagesDirectory(QSettings().value(GoldenImagesDirSettingsKey).toString())
	{

	}

	float GoldenImageComparator::compare(const QImage& test, const QString& name, QImage *difference, bool *ok) {
		QImage golden	= loadGoldenImage(name);
		return compare(test, golden, difference, ok);
	}


	float GoldenImageComparator::compare(const QImage& test, const QImage& golden, QImage *difference, bool *ok)
	{
		if (golden.isNull())
		{
			*ok = false;
			return 0.f;
		}
		if (test.width() != golden.width())
		{
			*ok = false;
			return 0.f;
		}
		if (test.height() != golden.height())
		{
			*ok = false;
			return 0.f;
		}

		// Ensure images have similar formats
		QImage argb32test   = test.convertToFormat(QImage::Format_ARGB32_Premultiplied);
		QImage argb32golden = golden.convertToFormat(QImage::Format_ARGB32_Premultiplied);

		const int width  = argb32test.width();
		const int height = argb32test.height();

		*difference = QImage(width, height, QImage::Format_ARGB32_Premultiplied);

		QRgb *src  = reinterpret_cast<QRgb*>(argb32test.bits());
		QRgb *dst  = reinterpret_cast<QRgb*>(argb32golden.bits());
		QRgb *dimg = reinterpret_cast<QRgb*>(difference->bits());

		// build difference histogram
		unsigned  diff[256] = { 0 };
		
		for (int jdx = 0; jdx < height; ++jdx)
		{
			for (int idx = 0; idx < width; ++idx)
			{
				unsigned index		= jdx * width + idx;
				QRgb		 srcColor = *(src + index);
				QRgb     dstColor = *(dst + index);
				unsigned char dRed	 = abs(qRed(srcColor) - qRed(dstColor));
				unsigned char dGreen = abs(qGreen(srcColor) - qGreen(dstColor));
				unsigned char dBlue	 = abs(qBlue(srcColor) - qBlue(dstColor));

				unsigned avg = (dRed + dGreen + dBlue) / 3;
				++diff[avg];

				*(dimg + index) = qRgba(dRed, dGreen, dBlue, 255);
			} 
		} 

		// Analyze histogram
		float numPixels = width * height;
		float avgDiff = 0.f;
		for (int idx = 0; idx < 256; ++idx)
		{
			avgDiff += idx * diff[idx];
		}

		return avgDiff / (255.f * numPixels);
	}


	void GoldenImageComparator::setParameters(const Data::NamedVariantMap& params)
	{
		if (params.find("goldenimagesdir") != params.end())
		{
			mGoldenImagesDirectory = params["goldenimagesdir"].asVariant().toString();
		}
	}


	void GoldenImageComparator::setDefaultParameters()
	{
		mGoldenImagesDirectory = QSettings().value(GoldenImagesDirSettingsKey).toString();
	}


	/**
	 private:
	 */
	QImage GoldenImageComparator::loadGoldenImage(const QString& fileName)
	{
		return QImage(mGoldenImagesDirectory + "/" + fileName);
	}

	const Data::NamedVariantMap& GoldenImageComparator::getParameters() const
	{
		Q_ASSERT(! "This method is not implemented!");

		static Data::NamedVariantMap params;
		return params;
	}

} // namespace Tools

