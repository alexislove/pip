/*!
 *\file goldenimagecomparator.h
 *\brief This file contains golden image comparator definition
 */

#ifndef TOOLS_GOLDENIMAGECOMPARATOR_H
	#define TOOLS_GOLDENIMAGECOMPARATOR_H

	#include <QImage>
	#include <QString>

	#include "../Interfaces/icustomizable.h"

	namespace Tools
	{
		class GoldenImageComparator : public Interfaces::ICustomizable
		{
		public:
			/*!
			 *\brief Constructor
			 */
			explicit GoldenImageComparator();

			/*!
			 *\brief Compare given test image to the image with given file name, located in the stored golden images directory
			 *\param test				Test image instance
			 *\param name				Test image name
			 *\param difference Test and golden images difference
			 *\param ok					Comparison success state
			 *\return Average difference value - error percent
			 */
			float compare(const QImage& test, const QString& name, QImage* difference, bool *ok);

			float compare(const QImage& test, const QImage& golden, QImage* difference, bool *ok);

			//! See ICustomizable.setParameters(const Data::NamedVariantMap&)
			virtual void setParameters(const Data::NamedVariantMap& params);
			
			//! See ICustomizable.setDefaultParameters()
			virtual void setDefaultParameters(); 

		private:
			/*!
			 *\brief Load golden image with given name
			 *\param fileName Name of the golden image, without path
			 *\return Loaded image instance, if it's not found, it will be null one, 
			 *\				state can be obtained using isNull method
			 */
			QImage loadGoldenImage(const QString& fileName);

			virtual const Data::NamedVariantMap& getParameters() const;

		private:
			//! Golden images directory
			QString mGoldenImagesDirectory;
		};
} // namespace Tools

#endif // TOOLS_GOLDENIMAGECOMPARATOR_H