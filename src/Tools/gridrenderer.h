/*!
 *\file gridrenderer.h
 *\brief This file contains image grid renderer definition
 */

#ifndef TOOLS_GRIDRENDERER_H
	#define TOOLS_GRIDRENDERER_H

	#include <QColor>

	#include "../Interfaces/icustomizable.h"

	class QPainter;
	class QRect;

	namespace Tools
	{
		class GridRenderer : public Interfaces::ICustomizable
		{
		public:
			/*!
			 *\brief Constructor
			 *\param dw Default width step
			 *\param dh Default height step
			 *\param color Default color
			 */
			explicit GridRenderer(float dw, float dh, const QColor& color);

			/*!
			 *\brief Destructor
			 */
			virtual ~GridRenderer();

			/*!
			 *\brief Render grid, using given QPainter
			 *\param painter QPainter instance, initialized with surface, containing image
			 *\param region  Image region, where grid should be drawn
			 */
			void draw(QPainter *painter, const QRect& region);

			/*!
			 *\brief Set width step
			 *\param dw Desired step
			 */
			void setWidthStep(float dw)
			{
				mdW = dw;
			}

			/*!
			 *\brief Set height step
			 *\param dh Desired height step
			 */
			void setHeightStep(float dh)
			{
				mdH = dh;
			}

			/*!
			 *\brief Set color
			 *\param color Desired color
			 */
			void setColor(const QColor& color)
			{
				mColor = color;
			}

			//! See ICustomizable.setParameters(const Data::NamedVariantMap&)
			virtual void setParameters(const Data::NamedVariantMap& params);
			
			//! See ICustomizable.setDefaultParameters()
			virtual void setDefaultParameters();

			virtual const Data::NamedVariantMap& getParameters() const;


		private:
			//! Width step
			float	 mdW;
			//! Height step
			float	 mdH;
			//! Color of grid
			QColor mColor;
		};
} // namespace Tools

#endif // TOOLS_GRIDRENDERER_H