/*!
 *\file filtercommand.h
 *\brief Filter command implementation
 */

#ifndef COMMANDS_FILTERCOMMAND_H
	#define COMMANDS_FILTERCOMMAND_H

	#include <QUndoCommand>

	#include "../Interfaces/ifilter.h"
	#include "../Interfaces/iimagecontext.h"

	namespace Commands
	{
		class FilterCommand : public QUndoCommand
		{
		public:
			/*!
			 *\brief Constructor
			 *\param filter Filter instance
			 *\param context Image context instance
			 */
			explicit FilterCommand(Interfaces::IFilterPointer filter, Interfaces::IImageContextPointer context);
			virtual ~FilterCommand();

			/*!
			 *\brief Perform forward action
			 */
			virtual void redo();

			/*!
			 *\brief Perform backward action
			 */
			virtual void undo();

			/*!
			 *\brief Get command text
			 */
			QString getText() const;

		private:
			//! Filter instance
			Interfaces::IFilterPointer			 mFilter;
			//! Image context instance
			Interfaces::IImageContextPointer mContext;
			//! Backuped image context's state for undo operation
			Data::ImageContextStatePointer	 mUndoState;
			//! Backuped image context's state for redo operation (speedup, in *this* command)
			Data::ImageContextStatePointer	 mRedoState;
		};
	} // namespace Commands

#endif // COMMANDS_FILTERCOMMAND_H
