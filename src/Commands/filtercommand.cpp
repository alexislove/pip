/*!
 *\file filtercommand.cpp
 *\brief Filter command implementation
 */

#include <QApplication>

#include "filtercommand.h"

namespace Commands
{
	/**
	 public:
	 */
	FilterCommand::FilterCommand(Interfaces::IFilterPointer filter, Interfaces::IImageContextPointer context)
		: mFilter(filter),
			mContext(context)
	{
		setText(getText()); // Set text for QUndoCommand
	}

	FilterCommand::~FilterCommand()
	{
		//mContext->releaseImage();
	}

	void FilterCommand::redo()
	{
		// If filter was already applied, just set existing state
		if (mRedoState)
		{
			mContext->updateState(mRedoState);
			return;
		}

		Q_ASSERT(mContext);
		mUndoState = mContext->getCurrentState();
		mContext->applyFilter(mFilter);
	}

	void FilterCommand::undo()
	{
		Q_ASSERT(mUndoState);
		mRedoState = mContext->getCurrentState();
		mContext->updateState(mUndoState);
	}

	QString FilterCommand::getText() const
	{
		return QApplication::translate("FilterCommand", "Filter: ") + mFilter->getName();
	}
} // namespace Commands