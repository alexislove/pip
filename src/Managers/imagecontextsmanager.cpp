/*!
 *\file imagecontextsmanager.cpp
 *\brief Image contexts manager implementation
 */

#include "Private/ImageContextsManager/imagecontextsmanagerregistry.h"

#include "imagecontextsmanager.h"

using namespace Interfaces;
using namespace Managers::Private::ImageContextsManager;

namespace Managers
{
	IImageContextPointer ImageContextsManager::newContext(QWidget* widget)
	{
		return ImageContextsManagerRegistry::getInstance().newContext(widget);
	}

	IImageContextPointer ImageContextsManager::getContext(QWidget* widget)
	{
		return ImageContextsManagerRegistry::getInstance().getContext(widget);
	}

	void ImageContextsManager::deleteContext(QWidget* widget)
	{
		ImageContextsManagerRegistry::getInstance().getContext(widget);
	}

	bool ImageContextsManager::ensureValid(IImageContextPointer context) const
	{
		return ImageContextsManagerRegistry::getInstance().ensureValid(context);
	}
}