/*!
 *\file imagecontextsmanagerregistry.h
 *\brief Private image contexts manager registry implementation
 */

#ifndef MANAGERS_PRIVATE_IMAGECONTEXTSMANAGER_IMAGECONTEXTSMANAGERREGISTRY_H
	#define MANAGERS_PRIVATE_IMAGECONTEXTSMANAGER_IMAGECONTEXTSMANAGERREGISTRY_H

	#include <QMap>
	#include <QScopedPointer>

	#include "../../../Interfaces/iimagecontext.h"
	
	class QWidget;

	namespace Managers
	{
		namespace Private
		{
			namespace ImageContextsManager
			{
				/*!
				 *\class ImageContextsManagerRegistry
				 *\brief This is a private class, used by ImageContextsManager to store contexts associations.
				 *\			 In general cases no one should directly use it, except within ImageContextsManager methods.
				 *\			 This is singleton class. Use getInstance() method to obtain instance of it.
				 *\			 To perform some little-cost optimizations this is not a lazy singleton or some of it kinds.
				 *\			 We guarantee, that if the canonical instance pointer will be created during static members initialization
				 *\			 everything will be ok.
				 */
				class ImageContextsManagerRegistry
				{
				private:
					//! Widget to contexts association map
					typedef QMap< QWidget*, Interfaces::IImageContextPointer > WidgetContextsMap; 

				public:
					/*!
					 *\brief Get reference to canonical instance
					 *\return Reference
					 */
					static ImageContextsManagerRegistry& getInstance()
					{
						return *mInstance;
					}

				public:
					/*!
					 *\brief Create new association. In case one exists, it will be replaced with the new one.
					 *\			 Make sure, that you do not use old version of context. This may be caused by usage of the shared pointers
					 *\			 to store contexts - they may stay valid even after they've been removed from register
					 *\param widget Desired widget
					 *\return Created context pointer
					 */
					Interfaces::IImageContextPointer newContext(QWidget* widget);

					/*!
					 *\brief Get existing association for given widget
					 *\param widget Desired widget
					 *\return Associated context pointer, or null-pointer, in case no association exists
					 */
					Interfaces::IImageContextPointer getContext(QWidget* widget);

					/*!
					 *\brief Remove association with given widget. If no such exists, nothing will be done
					 *\param widget Desired widget
					 */
					void deleteContext(QWidget* widget);

					/*!
					 *\brief Make sure, that context pointer is up-to date and association wasn't deleted before
					 *\param context Context pointer
					 *\return True, if context exists 
					 */
					bool ensureValid(Interfaces::IImageContextPointer context);

				private:
					//! Canonical instance
					static QScopedPointer< ImageContextsManagerRegistry > mInstance;

				private:
					/*!
					 *\brief Constructor
					 */
					ImageContextsManagerRegistry();

				private:
					//! Widget 2 context associations
					WidgetContextsMap mMap;
				};
			}
		} // namespace Private
	} // namespace Managers

#endif // MANAGERS_PRIVATE_IMAGECONTEXTSMANAGER_IMAGECONTEXTSMANAGERREGISTRY_H
