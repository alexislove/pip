/*!
 *\file imagecontextsmanagerregistry.cpp
 *\brief Private image contexts manager registry implementation
 */

#include "../../../Contexts/commonimagecontext.h"

#include "imagecontextsmanagerregistry.h"

using namespace Interfaces;
using namespace Contexts;

namespace Managers
{
	namespace Private
	{
		namespace ImageContextsManager
		{
			/**
			 private static:
			 */
			QScopedPointer< ImageContextsManagerRegistry > ImageContextsManagerRegistry::mInstance(new ImageContextsManagerRegistry);

			/**
			 public:
			 */
			IImageContextPointer ImageContextsManagerRegistry::newContext(QWidget* widget)
			{
				// Explicitly remove existing
				if (mMap.find(widget) != mMap.end())
				{
					mMap.remove(widget);
				}

				IImageContextPointer context(new CommonImageContext);
				mMap[widget] = context;
				return context;
			}

			IImageContextPointer ImageContextsManagerRegistry::getContext(QWidget* widget)
			{
				WidgetContextsMap::iterator item = mMap.find(widget);

				if (item == mMap.end())
					return IImageContextPointer();
				return *item;
			}

			void ImageContextsManagerRegistry::deleteContext(QWidget* widget)
			{
				mMap.remove(widget); // No matter, exists or not
			}

			bool ImageContextsManagerRegistry::ensureValid(IImageContextPointer context)
			{
				foreach (IImageContextPointer pointer, mMap)
				{
					// Pointers level comparison
					if (pointer == context)
						return true;
				}

				return false; // Invalid, not found in current registry state
			}

			/**
			 private
			 */
			ImageContextsManagerRegistry::ImageContextsManagerRegistry()
			{

			}
		} // namespace ImageContextsManager
	} // namespace Private
} // namespace Managers