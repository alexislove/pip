/*!
 *\file linearfiltersregistry.cpp
 *\brief Private linear filters descriptors registry implementation
 */

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QSettings>

#include "linearfiltersregistry.h"

using namespace Filters;

namespace Managers
{
	namespace Private
	{
		namespace LinearFiltersManager
		{
			static const char * const XmlNameSettingsKey	  = "App/FiltersRegistryXml";
			static const char * const FiltersDirSettingsKey = "App/FiltersDir";

			QScopedPointer< LinearFiltersRegistry > LinearFiltersRegistry::mInstance;

			/**
			 public:
			 */
			LinearFiltersRegistry::~LinearFiltersRegistry()
			{

			}

			void LinearFiltersRegistry::load()
			{
				QFile xmlFile(mFiltersXmlName);

				if (!xmlFile.open(QIODevice::ReadOnly))
				{
					return;
				}

				read(&xmlFile);

				mIsLoaded = true;
			}

			void LinearFiltersRegistry::save() const
			{
				QFile xmlFile(mFiltersXmlName);

				if (!xmlFile.open(QIODevice::WriteOnly))
				{
					return;
				}

				write(&xmlFile);
			}

			LinearFilterDescriptor LinearFiltersRegistry::getDescriptor(const QString& name, bool *ok) const
			{
				DescriptorsMap::ConstIterator desc = mDescriptors.find(name);
				if (desc == mDescriptors.end())
				{
					*ok = false;
					return LinearFilterDescriptor();
				}
				*ok = true;
				return *desc;
			}

			void LinearFiltersRegistry::registerDescriptor(const LinearFilterDescriptor& filter)
			{
				mDescriptors.insert(filter.Name, filter);
			}

			QStringList LinearFiltersRegistry::getRegisteredFiltersNames() const
			{
				return mDescriptors.keys();
			}

			void LinearFiltersRegistry::removeDescriptor(const QString& name)
			{
				static const QString FiltersFileExtension(".dat");

				DescriptorsMap::iterator desc = mDescriptors.find(name);
				if (desc != mDescriptors.end())
				{
					mDescriptors.remove(name);
					QFile::remove(mFiltersFolder + "/" + name + FiltersFileExtension); // Remove descriptor file
				}
			}

			/**
			 private:
			 */
			LinearFiltersRegistry::LinearFiltersRegistry()
				: mIsLoaded(false),
					mFiltersFolder(QDir(QSettings().value(FiltersDirSettingsKey).toString()).absolutePath()),
					mFiltersXmlName(QFileInfo(QSettings().value(XmlNameSettingsKey).toString()).absoluteFilePath())
			{
				
			}

			void LinearFiltersRegistry::write(QIODevice* device) const
			{
				static const QString FiltersFileExtension(".dat");

				QDomDocument document;
				document.appendChild(document.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\""));

				// Create <filters> element
				QDomElement filters = document.createElement("filters");
				document.appendChild(filters);

				for (DescriptorsMap::const_iterator desc = mDescriptors.begin(); desc != mDescriptors.end(); ++desc)
				{
					QDomElement element = addElement(document, filters, "filter");

					const QString& filterName = desc->Name;
					const QString& fileName		= filterName + FiltersFileExtension;
					addAttribute(element, "filter", filterName);
					addAttribute(element, "file",		fileName);

					// Serialize descriptor
					QFile filterFile(mFiltersFolder + "/" + fileName);

					if (!filterFile.open(QIODevice::WriteOnly))
					{
						return;
					}

					desc->write(&filterFile);
				}

				// Save XML document
				const QString xmlString = document.toString();
				if (xmlString.isEmpty())
				{
					return;
				}

				device->write(xmlString.toUtf8());
			}

			void LinearFiltersRegistry::read(QIODevice* device)
			{
				QDomDocument document;

				if (!document.setContent(device))
				{
					return;
				}

				QDomElement docElement = document.documentElement();

				// We're in <filters> node, so start reading from first child
				QDomNode currentNode = docElement.firstChild();

				while (!currentNode.isNull())
				{
					// Read attributes for filter name and the .dat file, containing filter params
					QString filterName;
					QString datFile;

					bool ok = readAttribute(currentNode, "filter", filterName);
					ok			= readAttribute(currentNode, "file",	 datFile);

					if (!ok)
					{
						return;
					}

					QFile filterFile(mFiltersFolder + "/" + datFile);

					if (!filterFile.open(QIODevice::ReadOnly))
					{
						return;
					}

					LinearFilterDescriptor descriptor;

					descriptor.read(&filterFile);

					mDescriptors.insert(descriptor.Name, descriptor);

					currentNode = currentNode.nextSibling();
				}
			}

		} // namespace CustomFiltersManager
	} // namespace Private
} // namespace Managers