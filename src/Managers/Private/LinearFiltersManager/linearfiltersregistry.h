/*!
 *\file customfiltersregistry.h
 *\brief Private linear filters descriptors registry implementation
 */

#ifndef MANAGERS_PRIVATE_FILTERSMANAGER_FILTERSREGISTRY_H
	#define MANAGERS_PRIVATE_FILTERSMANAGER_FILTERSREGISTRY_H

	#include <QMap>
	#include <QScopedPointer>
	#include <QString>

	#include "../../../Filters/linearfilterdescriptor.h"
	#include "../../../Interfaces/ixmlserializable.h"

	namespace Managers
	{
		namespace Private
		{
			namespace LinearFiltersManager
			{
				/*!
				 *\class LinearFiltersRegistry
				 *\brief This is a private class, used within LinearFiltersManager. No one should use it directly,
				 *\			 except extending LinearFiltersManager functionality.
				 *\			 This is singleton class. Use getInstance() method to obtain instance of it.
				 *\			 Also, it implements IXmlSerializable interface, because all filters list is stored in xml file.
				 */
				class LinearFiltersRegistry : public Interfaces::IXmlSerializable
				{ 
				private:
					typedef QMap<QString, Filters::LinearFilterDescriptor> DescriptorsMap;
				public:
					/*!
					 *\brief Get reference to canonical instance
					 *\return Reference
					 */
					static LinearFiltersRegistry& getInstance()
					{
						if (mInstance)
							return *mInstance;
						mInstance.reset(new LinearFiltersRegistry);
						return *mInstance;
					}

				public:
					/*!
					 *\brief Destructor
					 */
					virtual ~LinearFiltersRegistry();

					/*!
					 *\brief Get linear filter descriptor, associated with given name
					 *\param name Name of the filter
					 *\param ok   Success state of the methods
					 *\return Descriptor instance
					 */
					Filters::LinearFilterDescriptor getDescriptor(const QString& name, bool *ok) const;

					/*!
					 *\brief Register given filter descriptor. It will be saved on the disk, if user wants
					 *\param desc Descriptor to save
					 */
					void registerDescriptor(const Filters::LinearFilterDescriptor& filter);

					/*!
					 *\brief Get list of all registered descriptor names
					 *\return String list of filter names
					 */
					QStringList getRegisteredFiltersNames() const;

					/*!
					 *\brief Remove filter descriptor with given name
					 *\param name Name of the filter descriptor to remove
					 */
					void removeDescriptor(const QString& name);

					/*!
					 *\brief Load filters descriptors
					 */
					void load();

					/*!
					 *\brief Save filters descriptors on disc
					 */
					void save() const;

					/*!
					 *\brief Get registry loaded state
					 *\return Actual state
					 */
					bool isLoaded() const
					{
						return mIsLoaded;
					}

				private:
					/*!
					 *\brief Constructor
					 */
					LinearFiltersRegistry();

					/*!
					 *\brief See ISerializable.write(QIODevice*)
					 */
					virtual void write(QIODevice* device) const;

					/*!
					 *\brief See ISerilizable.read(QIODevice*)
					 */
					virtual void read(QIODevice* device);

				private:
					//! Canonical instance
					static QScopedPointer< LinearFiltersRegistry > mInstance;

				private:
					//! Linear filter name to descriptor map
					DescriptorsMap		mDescriptors;
					//! Loaded state
					bool							mIsLoaded;
					//! Path to the filters folder
					QString						mFiltersFolder;
					//! Path to the filters XML
					QString						mFiltersXmlName;
				};
			} // namespace CustomFiltersManager
		} // namespace Private
	} // namespace Managers

#endif // MANAGERS_PRIVATE_FILTERSMANAGER_FILTERSREGISTRY_H