/*!
 *\file imagecontextsmanager.h
 *\brief Image contexts manager implementation
 */

#ifndef MANAGERS_IMAGECONTEXTSMANAGER_H
	#define MANAGERS_IMAGECONTEXTSMANAGER_H

	#include "../interfaces/iimagecontext.h"

	class QWidget;

	namespace Managers
	{
		/*!
		 *\class ImageContextsManager
		 *\brief This class provides methods to access image contexts, related to different viewer widgets.
		 *\			 This is a value class, so whenever you want to obtain data, you just create a variable and use it.
		 *\			 There's no need to hold it as a class member or something like this.
		 *\			 Example of basic usage:
		 *\			 
		 *\			 ImageContextsManager manager;
		 *\			 // Get current context for some widget
		 *\			 IImageContextPointer currentContext = manager.getContext(/ * QWidget* * /someWidget);
		 *\			 if (!currentContext)
		 *\					return;
		 *\
		 *\			 Another thing, this class does - creation of image contexts. Whenever you need to add new context for some widget
		 *\			 you should call newContext(QWidget*). Created context will be returned to caller and also added to internal storage.
		 *\			 Example:
		 *\			 
		 *\			 QWidget* mySuperWidget = new MySuperWidget;
		 *\			 ImageContextsManager manager;
		 *\			 IImageContextPointer mySuperContext = manager.newContext(mySuperWidget);
		 *\			 // Go on, doing operations on context
		 *\				...
		 */
		struct ImageContextsManager
		{
			/*!
			 *\brief Create new context for given widget
			 *\param widget Widget to associate context with
			 *\return Created context pointer
			 */
			Interfaces::IImageContextPointer newContext(QWidget* widget);

			/*!
			 *\brief Get context for given widget, if such exists
			 *\param widget Desired widget
			 *\return Context pointer, if association exists, or null-pointer otherwise
			 */
			Interfaces::IImageContextPointer getContext(QWidget* widget);

			/*!
			 *\brief Remove context associated with given widget. If no association exists, nothing is done.
			 *\			 As soon as shared pointers are used, some of the may stay valid. Be sure not to use removed context!
			 *\param widget Desired widget
			 */
			void deleteContext(QWidget* widget);

			/*!
			 *\brief Make sure, that given context is valid and wasn't removed before
			 *\param context Context pointer
			 *\return True, if context stays valid in call-time
			 */
			bool ensureValid(Interfaces::IImageContextPointer context) const;
		};

	} // namespace Managers

#endif // MANAGERS_IMAGECONTEXTSMANAGER_H