/*!
 *\file customfitersmanager.h
 *\brief Filters manager implementation
 */

#include "private/LinearFiltersManager/linearfiltersregistry.h"

#include "linearfiltersmanager.h"

using namespace Filters;

namespace Managers
{
	using namespace Private::LinearFiltersManager;

	LinearFilterDescriptor LinearFiltersManager::getDescriptor(const QString& name, bool *ok) const
	{
		LinearFiltersRegistry& registry = LinearFiltersRegistry::getInstance();
		if (!registry.isLoaded())
		{
			registry.load();
		}
		return registry.getDescriptor(name, ok);
	}

	void LinearFiltersManager::registerDescriptor(const LinearFilterDescriptor& desc, bool save)
	{
		LinearFiltersRegistry& registry = LinearFiltersRegistry::getInstance();
		if (!registry.isLoaded())
		{
			registry.load();
		}

		registry.registerDescriptor(desc);

		if (save)
		{
			registry.save();
		}
	}

	QStringList LinearFiltersManager::getRegisteredFiltersNames() const
	{
		LinearFiltersRegistry& registry = LinearFiltersRegistry::getInstance();
		if (!registry.isLoaded())
		{
			registry.load();
		}

		return registry.getRegisteredFiltersNames();
	}

	void LinearFiltersManager::removeDescriptor(const QString& name)
	{
		LinearFiltersRegistry& registry = LinearFiltersRegistry::getInstance();

		if (!registry.isLoaded())
		{
			return;
		}

		registry.removeDescriptor(name);

		// Save updated registry in disc
		registry.save();
	}

	bool LinearFiltersManager::canBeShowOrEdited(const LinearFilterDescriptor& descriptor) const
	{
		const int rowsCount = descriptor.Matrix.size();
		if (rowsCount != 3 && rowsCount != 5)
		{
			return false;
		}
		const int columnsCount = descriptor.Matrix[0].size(); // For sure 0 element exists after first if
		if (columnsCount != 3 && columnsCount != 5)
		{
			return false;
		}

		return true;
	}
} // namespace Managers