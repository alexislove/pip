/*!
 *\file linearfitersmanager.h
 *\brief Linear filters manager implementation
 */

#ifndef MANAGERS_LINEARFILTERSMANAGER_H
	#define MANAGERS_LINEARFILTERSMANAGER_H

	#include <QStringList>

	#include "../Filters/linearfilterdescriptor.h"
	
	namespace Managers
	{
		/*!
		 *\class LinearFiltersManager
		 *\brief This class provides methods to create and manage filters.
		 *\			 This is a value class, so whenever you want to obtain data, you just create a variable and use it.
		 *\			 There's no need to hold it as a class member or something like this.
		 *\			 Example of basic usage:
		 *\			 
		 *\			 LinearFiltersManager manager;
		 *\			 
		 *\			 LinearFilterDescriptor desc = manager.getDescriptor(/ * const char* * / someFilterIdName);
		 *\			 
		 *\			 IFilterPointer filter = new LinearFilter(desc);
		 *\			 
		 *\
		 *\			 Another thing this class does - registration of new descriptors, which later will be saved on the disk.
		 *\			 For example:
		 *\			 LinearFilterDescriptor descriptor = { someMatrix, 1.f, "name" };
		 *\			 FiltersManager manager;
		 *\			 manager.registerDescriptor(descriptor);
		 *\				...
		 */
		struct LinearFiltersManager
		{
			/*!
			 *\brief Get linear filter descriptor, associated with given name
			 *\param name Name of the filter
			 *\param ok   Success state of the methods
			 *\return Descriptor instance
			 */
			Filters::LinearFilterDescriptor getDescriptor(const QString& name, bool *ok) const;

			/*!
			 *\brief Register given filter descriptor. It will be saved on the disk, if user wants
			 *\param desc Descriptor to save
			 *\param save	Should descriptor be saved on the disk or not
			 */
			void registerDescriptor(const Filters::LinearFilterDescriptor& desc, bool save);

			/*!
			 *\brief Get list of all registered descriptor names
			 *\return String list of filter names
			 */
			QStringList getRegisteredFiltersNames() const;

			/*!
			 *\brief Remove filter with given name, if such exists. Otherwise nothing will be done
			 *\param name Name of the filter to remove
			 */
			void removeDescriptor(const QString& name);

			/*!
			 *\brief Check, whether given descriptor can be editted or shown inside linear filter editor dialog
			 *\param descriptor Descriptor instance
			 *\return True, if descriptor has square matrix with dimensions 3x3 or 5x5 (task limits)
			 */
			bool canBeShowOrEdited(const Filters::LinearFilterDescriptor& descriptor) const;
		};

	} // namespace Managers

#endif // MANAGERS_FILTERSMANAGER_H