/*!
 *\file main.cpp
 *\brief Application entry point
 */

#include <QApplication>
#include <QCoreApplication>
#include <QSettings>

#include "ui/mainwindow.h"

int main(int argc, char *argv[])
{
	QCoreApplication::setOrganizationName("4057");
	QCoreApplication::setApplicationName("PIP");

	QApplication		application(argc, argv);

	QSettings::setDefaultFormat(QSettings::IniFormat);
	QSettings::setPath(QSettings::IniFormat, QSettings::UserScope, QApplication::applicationDirPath());

	Ui::MainWindow *window = new Ui::MainWindow();

	window->buildAppearance();
	window->showMaximized();

	return application.exec();
}
