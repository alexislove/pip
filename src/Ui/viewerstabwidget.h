/*!
 *\file viewerstabwidget.h
 *\brief Tab widget for image viewers implementation
 */

#ifndef UI_VIEWERSTABWIDGET_H
	#define UI_VIEWERSTABWIDGET_H
	
	#include <QStringList>
	#include <QTabWidget>

	namespace Interfaces
	{
		struct IFilterTriggerListener;
	}

	namespace Ui
  {

		/*!
		 *\class ViewersTabWidget
		 *\brief This class represents tab widget for a set of image viewers (See most modern browsers for details).
		 *\			 It provides several reactors for the main window activities, such as opening files, saving etc.
		 *\			 When a particular action occures, tab widget retrieved image context of current tab and redirect 
		 *\			 activity to it. Most of the time redirection is passed through commands (QUndoCommand subclasses) 
		 *\			 Here's the example of it:
		 *\				[open image action happened]
		 *\				QUndoCommand* openCommand = OpenCommand(fileName, currentContext);
		 *\				currentContext->applyCommand(openCommand);
		 *\			
		 *\			If you want to extend application with some more actions, see implementation on how to obtain current context
		 *\			and work with it. It can be found in already implemented methods
		 */
		class ViewersTabWidget : public QTabWidget
		{
			Q_OBJECT
		public:
			/*!
			 *\brief Constructor
			 *\param parent Parent widget
			 */
			explicit ViewersTabWidget(QWidget *parent = NULL);

			/*!
			 *\brief Destructor 
			 */
			virtual ~ViewersTabWidget();

			/*!
			 *\brief Get changes history for the current opened image
			 *\return Changes history as the string list of command names
			 */
			QStringList getCurrentImageHistory() const;

			/*!
			 *\brief Get index of current operation in the stack
			 *\return Current contexts operation index or -1, if no context exists
			 */
			int getCurrentOperationIndex() const;

			/*!
			 *\brief Set operation index for current context
			 *\param index Desired operation index
			 */
			void setCurrentOperationIndex(int index);

			/*!
			 *\brief Check, whether any image need to be saved
			 *\return True, if any image was modified
			 */
			bool isAnyDirty();

			/*!
			 *\brief Save all files 
			 *\param cancel True, if user cancelled to save any of the files
			 */
			void saveAll(bool *cancelled);

			/*!
			 *\brief Check whether current viewer can have forward action
			 *\return Actual redo state
			 */
			bool canCurrentRedo();

			/*!
			 *\brief Check whether current viewer can have backward action
			 *\return Actual redo state
			 */
			bool canCurrentUndo();

			/*!
			 *\brief Check whether current viewer can be saved
			 *\return Actual dirty state
			 */
			bool isCurrentDirty();

		public slots:
			/**
				Next group of methods represents reactors for different activities of the main window
			 **/

			/*!
			 *\brief Open file with given name at current tab if it's empty or at new one if not
			 *\param fileName Name of the desired file
			 */
			void openFile(const QString& fileName);

			/*!
			 *\brief Save current opened image, in case it exists, otherwise do nothing
			 */
			void saveCurrent();

			/*!
			 *\brief Save current opened image to the given file, in case image exists, otherwise do nothing
			 *\param fileName Name of the image file
			 */
			void saveAs(const QString& fileName);

			/*!
			 *\brief Perform undo operation on current image, if it exists
			 */
			void undo();

			/*!
			 *\brief Perform redo operation on current image, if it exists
			 */
			void redo();

			/*!
			 *\brief Update current widget
			 */
			void updateCurrent();

			/*!
			 *\brief Show grid on current image viewer
			 */
			void onShowGridOnCurrent();

			/*!
			 *\brief Hide grid on current image viewer
			 */
			void onHideGridOnCurrent();

			/*!
			 *\brief Handle application's grid visibility request
			 */
			void onCurrentGridVisibleRequest();

			/*!
			 *\brief Handle application's request to perform golden image comparison
			 */
			void onCurrentCompareToGolden();

			/*!
			 *\brief Handle application's request to show specific color channel
			 */
			void onShowChannel();

		signals:
			/*!
			 *\brief This signal is emitted, whenever tab is closed
			 */
			void tabClosed();

			/*!
			 *\brief This signal is emitted, whenever command was applied in processor widget
			 */
			void processorCommandApplied();

      /*!
       *\brief This signal is emitted, whenever new linear filter was added within some widget
       *\param whoAdded Instance of widget, that added filter
       */
      void someoneAddedLinearFilter(QWidget* whoAdded);

			/*!
			 *\brief This signal is emitted, whenever undo or redo is called
			 */
			void commandApplied();

			/*!
			 *\brief This signal is emitted, whenever grid on current widget is desired to be shown
			 *\param target Target widget, actually current one
			 */
			void showGrid(QWidget *target);

			/*!
			 *\brief This signal is emitted, whenever grid on current widget is desired to be hidden
			 *\param target Target widget, actually current one
			 */
			void hideGrid(QWidget *target);

			/*!
			 *\brief This signal is emitted, whenever grid customizer widget is desired to be hidden, for ex. when tab has changed
			 *\param target Target widget, actually current one
			 */
			void hideCustomizers(QWidget *target);

			/*!
			 *\brief This signal is emitted, whenever last visible state of grid customizer widget is desired to be restored
			 *\param target Target widget, actually current one
			 */
			void restoreCustomizers(QWidget *target);

			/*!
			 *\brief This signal is emitted, when application needs visibility state of current image viewer
			 */
			void viewerGridVisibleStateRequest(QWidget *target);

			/*!	
			 *\brief This signal is emitted, when viewer responds on visibility state request
			 */
			void currentGridVisibleStateResponse(bool visible);

			/*!
			 *\brief This signal is emitted, when given viewer is desired to perform comparison with golden image
			 */
			void compareToGolden(QWidget *target);

			/*!
			 *\brief This signal is emitted, when given viewer is desired to show specific color channel
			 */
			void showChannel(QWidget *target);

		private:
			/*!
			 *\brief Setup appearance and behavior
			 */
			void buildAppearance();

			/*!
			 *\brief Setup connections
			 */
			void setupConnections();

			/*!
			 *\brief Prompt to save file
			 *\param fileName Name of the file with extension, but without path
			 *\param cancel True, if user cancelled to close tab
			 *\return True, if user would like to save file
			 */
			bool promptToSaveFile(const QString& fileName, bool *cancelled); 

		private slots:
			/*!
			 *\brief Handle tab close request
			 *\param index Index of tab, that was requested to close
			 */
			void onTabCloseRequested(int index);

			/*!
			 *\brief Handle tab change
			 *\param index Index of new tab
			 */
			void onTabChanged(int index);

      /*!
       *\brief Handle current processor linear filter add signal
       */
      void onCurrentProcessorAddedLinearFilter();

		private:
			//! Vector of filter trigger event listeners
			QVector< Interfaces::IFilterTriggerListener* > mFilterTriggerListeners;
			//! Index of tab, that was shown before the current, -1 if no, or there's only one tab
			int																						 mPreviousTab;
		};
	} // namespace Ui

#endif // UI_VIEWERSTABWIDGET_H