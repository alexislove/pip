/*!
 *\file goldeimagecomparatorwidget.cpp
 *\brief This file contains golden images comparator widget implementation
 */

#include <QFileDialog>
#include <QMessageBox>
#include <QPaintEvent>
#include <QPainter>
#include <QSettings>

#include "../Tools/goldenimagecomparator.h"

#include "goldenimagecomparatorwidget.h"

using namespace Data;
using namespace Tools;

namespace Ui
{
	static const char* const GoldenImagesDirSettingsKey = "GoldenImagesDir";
	static const char* const DifferenceImagesDirSettingsKey = "DifferenceImagesDir";

	/**
	 public:
	 */
	GoldenImageComparatorWidget::GoldenImageComparatorWidget(const QImage& testImage, const QString& testImageFileName, QWidget *parent /*= NULL*/)
		: QDialog(parent),
			mTestImage(testImage),
			mTestImageFileName(testImageFileName),
			mComparator(new GoldenImageComparator()),
			mComparisonMade(false)

	{
		setupUi(this);
		setup();
	}

	GoldenImageComparatorWidget::~GoldenImageComparatorWidget()
	{

	}

	/**
	 protected:
	 */
	bool GoldenImageComparatorWidget::eventFilter(QObject* obj, QEvent* e)
	{
		if (!mComparisonMade)
		{
			return false;
		}

		if (obj == wImageDifference && e->type() == QEvent::Paint)
		{
			QPainter painter;
			painter.begin(wImageDifference);

			QRect windowRect  = painter.viewport();
			int		width			  = windowRect.width();
			int   height		  = windowRect.height();
			int   imageWidth  = mDifferenceImage.width();
			int   imageHeight = mDifferenceImage.height();

			int x = width / 2  - imageWidth  / 2;
			int y = height / 2 - imageHeight / 2;

			bool notScaled = true;
			QImage clipped;
			if (imageWidth > width || imageHeight > height)
			{
				clipped   = mDifferenceImage.scaled(qMin(imageWidth, width), qMin(imageHeight, height), Qt::KeepAspectRatio);
				// Recalculate coordinates once more
				x = width  / 2 - clipped.width()  / 2;
				y = height / 2 - clipped.height() / 2;
				notScaled = false;
			}
	
			painter.drawImage(x, y, notScaled ? mDifferenceImage : clipped);
			painter.end();
			return true;
		}
		

		return false;
	}

	/**
	 private:
	 */
	void GoldenImageComparatorWidget::setup()
	{
		QSettings settings;

		leGoldenImagesDir->setText(settings.value(GoldenImagesDirSettingsKey).toString());
		leDifferenceImagesDir->setText(settings.value(DifferenceImagesDirSettingsKey).toString());

		connect(pbBrowseGoldenDir,		 SIGNAL( pressed() ), this, SLOT( onBrowseGoldenImagesDir() ));
		connect(pbBrowseDifferenceDir, SIGNAL( pressed() ), this, SLOT( onBrowseDifferenceImagesDir() ));
		connect(leGoldenImagesDir,		 SIGNAL( textChanged(QString) ), this, SLOT( onGoldenImagesDirTextChanged(QString) ));
		connect(leDifferenceImagesDir, SIGNAL( textChanged(QString) ), this, SLOT( onDifferenceImagesDirTextChanged(QString) ));
		connect(pbCompare,						 SIGNAL( pressed() ), this, SLOT( onCompareButtonPressed() ));

		setWindowTitle(tr("Compare current image to reference one"));

		lErrorValue->setVisible(false);
		wImageDifference->setVisible(false);
		wImageDifference->setToolTip(tr("Images difference"));
		wImageDifference->installEventFilter(this);
		wImageDifference->setStyleSheet("color: black;");
	}

	/**
	 private slots:
	 */
	void GoldenImageComparatorWidget::onBrowseGoldenImagesDir()
	{
		QString directory = QFileDialog::getExistingDirectory(this, tr("Select golden images directory"), leGoldenImagesDir->text());

		leGoldenImagesDir->setText(directory);
	}

	void GoldenImageComparatorWidget::onBrowseDifferenceImagesDir()
	{
		QString directory = QFileDialog::getExistingDirectory(this, tr("Select difference images directory"), leDifferenceImagesDir->text());

		leDifferenceImagesDir->setText(directory);
	}

	void GoldenImageComparatorWidget::onGoldenImagesDirTextChanged(const QString& text)
	{
		QSettings().setValue(GoldenImagesDirSettingsKey, QVariant::fromValue(text));
		NamedVariantMap params;
		params["goldenimagesdir"] = ExtendedVariant::toExtendedVariant(QVariant::fromValue(text));
		mComparator->setParameters(params);
	}

	void GoldenImageComparatorWidget::onDifferenceImagesDirTextChanged(const QString& text)
	{
		QSettings().setValue(DifferenceImagesDirSettingsKey, QVariant::fromValue(text));
	}

	void GoldenImageComparatorWidget::onCompareButtonPressed()
	{
		bool ok		 = true;
		QImage difference;
		float errorPercent = mComparator->compare(mTestImage, mTestImageFileName, &difference, &ok) * 100;
		if (!ok)
		{
			QMessageBox::critical(this, 
														tr("Comparison with golden image failed"), 
														tr("Golden image ") + mTestImageFileName + tr(" not found or invalid!"));
			return;
		}

		lErrorValue->setText(tr("Mismatch percent: ") + QString::number(errorPercent) + "%");

		// Save image to differences folder
		difference.save(leDifferenceImagesDir->text() + "/" + mTestImageFileName);

		// Get difference image
		mDifferenceImage = difference;
		mComparisonMade  = true;

		lErrorValue->setVisible(true);
		wImageDifference->setVisible(true);
		update();
	}

} // namespace Ui