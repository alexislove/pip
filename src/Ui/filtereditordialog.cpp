/*!
 *\file filtereditordialog.cpp
 *\brief Filter editor dialog definition
 */

#include <QMessageBox>

#include "../Managers/linearfiltersmanager.h"

#include "filtereditordialog.h"

using namespace Filters;
using namespace Managers;

namespace Ui
{
	static const int cPage3x3 = 0;
	static const int cPage5x5 = 1;

	/**
	 public:
	 */
	FilterEditorDialog::FilterEditorDialog(QWidget *parent/* = NULL */)
		: QDialog(parent),
			mCurrentMode(MODE_CREATE_NEW)
	{
		setupUi(this);
		setWindowFlags(Qt::Tool);

		// Hide forever
		cbSave->setVisible(false);

		connect(pbAdd,		SIGNAL( pressed() ), this, SLOT( onAddButtonPressed()		 ));
		connect(pbCancel, SIGNAL( pressed() ), this, SLOT( onCancelButtonPressed() ));
	}

	FilterEditorDialog::FilterEditorDialog(const LinearFilterDescriptor& desc, ModeFlags mode, QWidget *parent/* = NULL */)
		: QDialog(parent),
			mCurrentMode(mode)
	{
		setupUi(this);
		setWindowFlags(Qt::Tool);

		// Hide forever
		cbSave->setVisible(false);

		connect(pbAdd,		SIGNAL( pressed() ), this, SLOT( onAddButtonPressed()		 ));
		connect(pbCancel, SIGNAL( pressed() ), this, SLOT( onCancelButtonPressed() ));

		fillFromDescriptor(desc);

		if (mode & MODE_READ_ONLY)
		{
			setReadOnly();
		}
		if (mode & MODE_EDIT_EXISTING)
		{
			cbSave->setVisible(false);
			pbAdd->setText(tr("Save"));
		}
	}

	bool FilterEditorDialog::execute(LinearFilterDescriptor *out /* = NULL*/)
	{
		int ret = exec();

		if (mCurrentMode & MODE_READ_ONLY)
		{
			return true;
		}

		Q_ASSERT(out);
		if (ret != QDialog::Accepted)
		{
			return false;
		}

		*out = createDescriptor();
		return true;
	}

	bool FilterEditorDialog::shouldSaveOnDisk() const
	{
		//return cbSave->isChecked();
		return true;
	}

	/**
	 private:
	 */
	LinearFilterDescriptor FilterEditorDialog::createDescriptor()
	{
		if (tbMatrix->currentIndex() == cPage3x3)
		{
			return create3x3Descriptor();
		}
		else if (tbMatrix->currentIndex() == cPage5x5)
		{
			return create5x5Descriptor();
		}

		return LinearFilterDescriptor();
	}

	#define LE3X3TOFLOAT(matrix, i, j) matrix[(i)][(j)] = p3x3_##i##j->text().toFloat();

	LinearFilterDescriptor FilterEditorDialog::create3x3Descriptor()
	{
		static const int cDim = 3;
		LinearFilterDescriptor descriptor;
		descriptor.Matrix.resize(cDim);
		for (int idx = 0; idx < cDim; ++idx)
		{
			descriptor.Matrix[idx].resize(cDim);
		}

		// Collect all data from all line edits
		LE3X3TOFLOAT(descriptor.Matrix, 0, 0);
		LE3X3TOFLOAT(descriptor.Matrix, 0, 1);
		LE3X3TOFLOAT(descriptor.Matrix, 0, 2);
		LE3X3TOFLOAT(descriptor.Matrix, 1, 0);
		LE3X3TOFLOAT(descriptor.Matrix, 1, 1);
		LE3X3TOFLOAT(descriptor.Matrix, 1, 2);
		LE3X3TOFLOAT(descriptor.Matrix, 2, 0);
		LE3X3TOFLOAT(descriptor.Matrix, 2, 1);
		LE3X3TOFLOAT(descriptor.Matrix, 2, 2);

		descriptor.Denominator = leDenominator->text().toFloat();
		descriptor.Name				 = leFilterName->text();

		return descriptor;
	}

	#define LE5X5TOFLOAT(matrix, i, j) matrix[(i)][(j)] = p5x5_##i##j->text().toFloat();

	LinearFilterDescriptor FilterEditorDialog::create5x5Descriptor()
	{
		static const int cDim = 5;
		LinearFilterDescriptor descriptor;
		descriptor.Matrix.resize(cDim);
		for (int idx = 0; idx < cDim; ++idx)
		{
			descriptor.Matrix[idx].resize(cDim);
		}

		// Collect all data from all line edits
		LE5X5TOFLOAT(descriptor.Matrix, 0, 0);
		LE5X5TOFLOAT(descriptor.Matrix, 0, 1);
		LE5X5TOFLOAT(descriptor.Matrix, 0, 2);
		LE5X5TOFLOAT(descriptor.Matrix, 0, 3);
		LE5X5TOFLOAT(descriptor.Matrix, 0, 4);
		LE5X5TOFLOAT(descriptor.Matrix, 1, 0);
		LE5X5TOFLOAT(descriptor.Matrix, 1, 1);
		LE5X5TOFLOAT(descriptor.Matrix, 1, 2);
		LE5X5TOFLOAT(descriptor.Matrix, 1, 3);
		LE5X5TOFLOAT(descriptor.Matrix, 1, 4);
		LE5X5TOFLOAT(descriptor.Matrix, 2, 0);
		LE5X5TOFLOAT(descriptor.Matrix, 2, 1);
		LE5X5TOFLOAT(descriptor.Matrix, 2, 2);
		LE5X5TOFLOAT(descriptor.Matrix, 2, 3);
		LE5X5TOFLOAT(descriptor.Matrix, 2, 4);
		LE5X5TOFLOAT(descriptor.Matrix, 3, 0);
		LE5X5TOFLOAT(descriptor.Matrix, 3, 1);
		LE5X5TOFLOAT(descriptor.Matrix, 3, 2);
		LE5X5TOFLOAT(descriptor.Matrix, 3, 3);
		LE5X5TOFLOAT(descriptor.Matrix, 3, 4);
		LE5X5TOFLOAT(descriptor.Matrix, 4, 0);
		LE5X5TOFLOAT(descriptor.Matrix, 4, 1);
		LE5X5TOFLOAT(descriptor.Matrix, 4, 2);
		LE5X5TOFLOAT(descriptor.Matrix, 4, 3);
		LE5X5TOFLOAT(descriptor.Matrix, 4, 4);
		

		descriptor.Denominator = leDenominator->text().toFloat();
		descriptor.Name				 = leFilterName->text();

		return descriptor;
	}

  #define LE3X3READONLY(i, j) p3x3_##i##j->setReadOnly(true);
  #define LE5X5READONLY(i, j) p5x5_##i##j->setReadOnly(true);
	void FilterEditorDialog::setReadOnly()
	{
    LE3X3READONLY(0, 0);
    LE3X3READONLY(0, 1);
    LE3X3READONLY(0, 2);
    LE3X3READONLY(1, 0);
    LE3X3READONLY(1, 1);
    LE3X3READONLY(1, 2);
    LE3X3READONLY(2, 0);
    LE3X3READONLY(2, 1);
    LE3X3READONLY(2, 2);

    LE5X5READONLY(0, 0);
    LE5X5READONLY(0, 1);
    LE5X5READONLY(0, 2);
    LE5X5READONLY(0, 3);
    LE5X5READONLY(0, 4);
    LE5X5READONLY(1, 0);
    LE5X5READONLY(1, 1);
    LE5X5READONLY(1, 2);
    LE5X5READONLY(1, 3);
    LE5X5READONLY(1, 4);
    LE5X5READONLY(2, 0);
    LE5X5READONLY(2, 1);
    LE5X5READONLY(2, 2);
    LE5X5READONLY(2, 3);
    LE5X5READONLY(2, 4);
    LE5X5READONLY(3, 0);
    LE5X5READONLY(3, 1);
    LE5X5READONLY(3, 2);
    LE5X5READONLY(3, 3);
    LE5X5READONLY(3, 4);
    LE5X5READONLY(4, 0);
    LE5X5READONLY(4, 1);
    LE5X5READONLY(4, 2);
    LE5X5READONLY(4, 3);
    LE5X5READONLY(4, 4);

    leDenominator->setReadOnly(true);
		leFilterName->setCursorPosition(0); // Move cursor to the string beginning
    leFilterName->setReadOnly(true);

    pbAdd->setVisible(false);
    cbSave->setVisible(false);

		// Instead of cancel, set close text
		pbCancel->setText(tr("Close"));
	}

	void FilterEditorDialog::fillFromDescriptor(const LinearFilterDescriptor& desc)
	{
		if (desc.Matrix.size() == 3)
		{
			fillMatrixFrom3x3Descriptor(desc);
			tbMatrix->setCurrentIndex(cPage3x3);
		}
		else if (desc.Matrix.size() == 5)
		{
			fillMatrixFrom5x5Descriptor(desc);
			tbMatrix->setCurrentIndex(cPage5x5);
		}

		leDenominator->setText(QString::number(desc.Denominator));
		leFilterName->setText(desc.Name);
	}

	#define LE3X3FROMFLOAT(matrix, i, j) p3x3_##i##j->setText(QString::number(matrix[(i)][(j)]));
	#define LE5X5FROMFLOAT(matrix, i, j) p5x5_##i##j->setText(QString::number(matrix[(i)][(j)]));
	void FilterEditorDialog::fillMatrixFrom3x3Descriptor(const LinearFilterDescriptor& desc)
	{
		LE3X3FROMFLOAT(desc.Matrix, 0, 0);
		LE3X3FROMFLOAT(desc.Matrix, 0, 1);
		LE3X3FROMFLOAT(desc.Matrix, 0, 2);
		LE3X3FROMFLOAT(desc.Matrix, 1, 0);
		LE3X3FROMFLOAT(desc.Matrix, 1, 1);
		LE3X3FROMFLOAT(desc.Matrix, 1, 2);
		LE3X3FROMFLOAT(desc.Matrix, 2, 0);
		LE3X3FROMFLOAT(desc.Matrix, 2, 1);
		LE3X3FROMFLOAT(desc.Matrix, 2, 2);
	}

	void FilterEditorDialog::fillMatrixFrom5x5Descriptor(const LinearFilterDescriptor& desc)
	{
		LE5X5FROMFLOAT(desc.Matrix, 0, 0);
		LE5X5FROMFLOAT(desc.Matrix, 0, 1);
		LE5X5FROMFLOAT(desc.Matrix, 0, 2);
		LE5X5FROMFLOAT(desc.Matrix, 0, 3);
		LE5X5FROMFLOAT(desc.Matrix, 0, 4);
		LE5X5FROMFLOAT(desc.Matrix, 1, 0);
		LE5X5FROMFLOAT(desc.Matrix, 1, 1);
		LE5X5FROMFLOAT(desc.Matrix, 1, 2);
		LE5X5FROMFLOAT(desc.Matrix, 1, 3);
		LE5X5FROMFLOAT(desc.Matrix, 1, 4);
		LE5X5FROMFLOAT(desc.Matrix, 2, 0);
		LE5X5FROMFLOAT(desc.Matrix, 2, 1);
		LE5X5FROMFLOAT(desc.Matrix, 2, 2);
		LE5X5FROMFLOAT(desc.Matrix, 2, 3);
		LE5X5FROMFLOAT(desc.Matrix, 2, 4);
		LE5X5FROMFLOAT(desc.Matrix, 3, 0);
		LE5X5FROMFLOAT(desc.Matrix, 3, 1);
		LE5X5FROMFLOAT(desc.Matrix, 3, 2);
		LE5X5FROMFLOAT(desc.Matrix, 3, 3);
		LE5X5FROMFLOAT(desc.Matrix, 3, 4);
		LE5X5FROMFLOAT(desc.Matrix, 4, 0);
		LE5X5FROMFLOAT(desc.Matrix, 4, 1);
		LE5X5FROMFLOAT(desc.Matrix, 4, 2);
		LE5X5FROMFLOAT(desc.Matrix, 4, 3);
		LE5X5FROMFLOAT(desc.Matrix, 4, 4);
	}

	/**
	 private slots:
	 */
	void FilterEditorDialog::onAddButtonPressed()
	{
    if (isWellFormed())
    {
      // Check, that such kind of filter doesn't exist
      LinearFiltersManager manager;

      QStringList existingFilters = manager.getRegisteredFiltersNames();

      if (existingFilters.contains(leFilterName->text()) && !(mCurrentMode & MODE_EDIT_EXISTING)) // If we're editing existing filter, name can be present
      {
        QMessageBox::critical(this, tr("Invalid filter name!"), tr("Filter with given name already exists, please choose another name or remove existing!"));
        return;
      }
		  accept();
		  close();

      return;
    }

    QMessageBox::critical(this, tr("Invalid filter parameters!"), tr("Some filter parameters are invalid, please, check input and try again!"));
	}

	void FilterEditorDialog::onCancelButtonPressed()
	{
		reject();
		close();
	}

	/**
	 private:
	 */
	#define LE3X3CHECKFLOAT(i, j) p3x3_##i##j->text().toFloat(&ok); if (!ok) return false
	#define LE5X5CHECKFLOAT(i, j) p5x5_##i##j->text().toFloat(&ok); if (!ok) return false

	bool FilterEditorDialog::isWellFormed() const
	{
		bool ok = true;
		if (tbMatrix->currentIndex() == cPage3x3)
		{
			LE3X3CHECKFLOAT(0, 0);
			LE3X3CHECKFLOAT(0, 1);
			LE3X3CHECKFLOAT(0, 2);
			LE3X3CHECKFLOAT(1, 0);
			LE3X3CHECKFLOAT(1, 1);
			LE3X3CHECKFLOAT(1, 2);
			LE3X3CHECKFLOAT(2, 0);
			LE3X3CHECKFLOAT(2, 1);
			LE3X3CHECKFLOAT(2, 2);
		}
		else if (tbMatrix->currentIndex() == cPage5x5)
		{
			LE5X5CHECKFLOAT(0, 0);
			LE5X5CHECKFLOAT(0, 1);
			LE5X5CHECKFLOAT(0, 2);
			LE5X5CHECKFLOAT(0, 3);
			LE5X5CHECKFLOAT(0, 4);
			LE5X5CHECKFLOAT(1, 0);
			LE5X5CHECKFLOAT(1, 1);
			LE5X5CHECKFLOAT(1, 2);
			LE5X5CHECKFLOAT(1, 3);
			LE5X5CHECKFLOAT(1, 4);
			LE5X5CHECKFLOAT(2, 0);
			LE5X5CHECKFLOAT(2, 1);
			LE5X5CHECKFLOAT(2, 2);
			LE5X5CHECKFLOAT(2, 3);
			LE5X5CHECKFLOAT(2, 4);
			LE5X5CHECKFLOAT(3, 0);
			LE5X5CHECKFLOAT(3, 1);
			LE5X5CHECKFLOAT(3, 2);
			LE5X5CHECKFLOAT(3, 3);
			LE5X5CHECKFLOAT(3, 4);
			LE5X5CHECKFLOAT(4, 0);
			LE5X5CHECKFLOAT(4, 1);
			LE5X5CHECKFLOAT(4, 2);
			LE5X5CHECKFLOAT(4, 3);
			LE5X5CHECKFLOAT(4, 4);
		}
		else
		{
			return false;
		}

		leDenominator->text().toFloat(&ok);
		if (!ok)
		{
			return false;
		}

    const QString filterName = leFilterName->text();
		if (filterName.isEmpty())
		{
			return false;
		}

		return true;
	}

} // namespace Ui
