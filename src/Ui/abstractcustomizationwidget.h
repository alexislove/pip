/*!
 *\file abstractcustomizationwidget.h
 *\brief This file contains abstract customization widget definition
 */

#ifndef UI_ABSTRACTCUSTOMIZATIONWIDGET_H
	#define UI_ABSTRACTCUSTOMIZATIONWIDGET_H

	#include <QDialog>

	#include "../Data/namedvariantmap.h"

	namespace Ui
	{
		/*!
		 
		 *\brief This class represents abstraction for all widgets, that perform customization operations
		 */
		class AbstractCustomizationWidget : public QWidget
		{
			Q_OBJECT
		public:
			/*!
			 *\brief Constructor
			 *\param parent Parent widget instance
			 */
			explicit AbstractCustomizationWidget(QWidget *parent = NULL)
				: QWidget(parent)
			{
			}

		public slots:
			/*!
			 *\brief Set parameters inside the dialog from the outside
			 *\param params Desired params
			 */
			virtual void setParams(const Data::NamedVariantMap& params) = 0;


		signals:
			/*!
			 *\brief This signal is emitted, whenever some parameters changed
			 *\param params Changed params
			 */
			void paramsChanged(const Data::NamedVariantMap& params);

		};
	}

#endif // UI_ABSTRACTCUSTOMIZATIONWIDGET_H