/*!
 *\file goldeimagecomparatorwidget.h
 *\brief This file contains golden images comparator widget definition
 */

#ifndef UI_GOLDENIMAGECOMPARATORWIDGET_H
	#define UI_GOLDENIMAGECOMPARATORWIDGET_H
	
	#include <QDialog>
	#include <QScopedPointer>

	#include "ui_goldenimagecomparatorwidgetform.h"

	namespace Tools
	{
		class GoldenImageComparator;
	} // namespace Tools

	namespace Ui
	{
		/*!
		 *\class GoldenImageComparatorWidget
		 *\brief This class represents the widget to perform golden image comparison
		 */
		class GoldenImageComparatorWidget : public QDialog,
																				private GoldenImageComparatorWidgetForm
		{
			Q_OBJECT
		public:
			/*!
			 *\brief Constructor
			 *\param testImage Image to compare with golden one
			 *\param parent Parent widget
			 */
			explicit GoldenImageComparatorWidget(const QImage& testImage, const QString& testImageFileName, QWidget *parent = NULL);
			
			/*!
			 *\brief Destructor
			 */
			virtual ~GoldenImageComparatorWidget();

		protected:
			/*!
			 *\brief Filter events on child objects
			 *\param obj Object, received event
			 *\param e	 Event descriptor
			 *\return True, if event shouldn't be passed to the object
			 */
			virtual bool eventFilter(QObject* obj, QEvent* e);

		private:
			/*!
			 *\brief Setup initial Ui
			 */
			void setup();

		private slots:
			/*!
			 *\brief Handle browse golden images path directory button press
			 */
			void onBrowseGoldenImagesDir();

			/*!
			 *\brief Handle browse difference images path directory button press
			 */
			void onBrowseDifferenceImagesDir();
			 
			/*!
			 *\brief Handle golden images line edit text change
			 *\param text New text value
			 */
			void onGoldenImagesDirTextChanged(const QString& text);

			/*!
			 *\brief Handle difference images line edit text change
			 *\param text New text value
			 */
			void onDifferenceImagesDirTextChanged(const QString& text);

			/*!
			 *\brief Handle compare button press
			 */
			void onCompareButtonPressed();

		private:
			//! Test image instance
			const QImage&																	 mTestImage;
			//! Test image file name
			const QString&																 mTestImageFileName;
			//! Comparator instance
			QScopedPointer< Tools::GoldenImageComparator > mComparator;
			//! Difference image
			QImage																				 mDifferenceImage;
			//! Comparison state
			bool																					 mComparisonMade;
		};
	}

#endif // UI_GICCUSTOMIZATIONWIDGET_H