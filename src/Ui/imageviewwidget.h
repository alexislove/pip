/*!
 *\file imageviewwidget.h
 *\brief Image processor widget implementation
 */

#ifndef UI_IMAGEVIEWWIDGET_H
	#define UI_IMAGEVIEWWIDGET_H

	#include "../Data/colormodelconversions.h"
	#include "../Data/namedvariantmap.h"

	#include <QGraphicsView>

	namespace Tools
	{
		class GridRenderer;
	} // namespace Tools

	namespace Ui
	{
		namespace Constants
		{
			//! Min/max allowed zoom factors
			static const float MinZoom = 0.1;
			static const float MaxZoom = 8.0;

			//! Default parameters for grid
			static const float  DefaultGridWidthStep  = 10.f;							// Unit step per width
			static const float  DefaultGridHeightStep = 10.f;							// Unit step per height
			static const QColor DefaultGridColor		  = QColor(0, 0, 0);	// Black color

			//! Minimal image dimension when showing grid is useful
			static const int ShowGridMinImageSize	= 300;
		} // namespace Constants

		class ImageViewWidget : public QGraphicsView
		{
			Q_OBJECT
		public:
			/*!
			 *\brief Constructor
			 *\param parent Parent widget instance
			 */
			explicit ImageViewWidget(QWidget* parent = NULL);

			/*!
			 *\brief Destructor
			 */
			virtual ~ImageViewWidget();

			/*!
			 *\brief Determine whether grid is being shown
			 *\return Grid visibility state
			 */
			bool isGridVisible() const { return mIsGridVisible; }

		public slots:
			virtual void paintEvent( QPaintEvent *e );
			virtual void mousePressEvent( QMouseEvent *e );
			virtual void mouseReleaseEvent( QMouseEvent *e );
			virtual void mouseDoubleClickEvent( QMouseEvent *e );
			virtual void mouseMoveEvent( QMouseEvent *e );
			virtual void wheelEvent( QWheelEvent *e );

			void redraw();

			void onZoomFit();
			void onZoomTo(float factor);

			void onColorModelChanged(Data::ColorModel model);
			void onColorChannelsChanged(bool ch1, bool ch2, bool ch3);
			void onGridParamsChanged(const Data::NamedVariantMap& params);
		  void onShowGrid(bool show = true);

		private:
			void imageToScene(const float factor);
			void zoomStep(int direction);			
			void renderGridPixmap();

			virtual void drawForeground( QPainter *painter, const QRectF &rect );
			virtual void drawItems( QPainter *painter, int numItems, QGraphicsItem *items[], const QStyleOptionGraphicsItem options[] );

		private:
			// FIXME: hack
			QWidget *mContextWidget;

			//! Graphics scene
			QGraphicsScene										 *mScene;
			//! Current image pixmap
			QGraphicsPixmapItem								 *mPixmapItem;

			//! Image zoom stuff
			enum ZoomMode
			{
				ZoomFit,		// Fit to window mode
				ZoomNonFit	// Managed by user mode
			};
			ZoomMode														mZoomMode;
			float															  mZoomFactor;

			//! Steps for wheel zoom
			QList<float>												mZoomFactors;

			//! Dragging image with mouse stuff
			QLine															 *mDragDelta;
			bool																mLeftMousePressed;

			//! Color model stuff
			Data::ColorModel										mColorModel;
			bool																mColorChannel1Enabled;
			bool																mColorChannel2Enabled;
			bool																mColorChannel3Enabled;

			//! Grid rendering state
			bool																mIsGridVisible;
			//! Grid renderer instance
			QSharedPointer<Tools::GridRenderer> mGridRenderer;
			QImage                              mGridImage;
			QGraphicsPixmapItem								 *mGridPixmapItem;
		};

	} // namespace Ui

#endif // UI_IMAGEVIEWWIDGET_H