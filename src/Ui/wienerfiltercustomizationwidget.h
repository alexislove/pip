/*!
 *\file wienerfiltercustomizationwidget.h
 *\brief This file contains definition of Wiener filter customization widget
 */

#ifndef UI_WIENERFILTERCUSTOMIZATIONWIDGET_H
	#define UI_WIENERFILTERCUSTOMIZATIONWIDGET_H

	#include "abstractcustomizationwidget.h"

	class QSlider;

	namespace Ui
	{
		/*!
		 *\class WienerFilterCustomizationWidget
		 *\brief This class implements customization widget for Wiener filter, show over image
		 */
		class WienerFilterCustomizationWidget : public AbstractCustomizationWidget
		{
			Q_OBJECT
		public:
			/*!
			 *\brief Constructor
			 *\param parent Parent widget
			 */
			explicit WienerFilterCustomizationWidget(QWidget *parent = NULL);
			
		public slots:
			//! See AbstractCustomizationWidget.setParams(const Data::NamedVariantMap&)
			virtual void setParams(const Data::NamedVariantMap& params);

		private slots:
			void onRadiusChanged(int value);
			void onSigmaChanged(int value);

		private:
			void setupUi(QWidget *widget);
			QSlider *hsRadius;
			QSlider *hsSigma;

		private:
			bool mIgnoreChanges;
			Data::NamedVariantMap mParams;
		};
	}
	

#endif // UI_WIENERFILTERCUSTOMIZATIONWIDGET_H
