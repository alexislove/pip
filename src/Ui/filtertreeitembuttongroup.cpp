/*!
 *\file filtertreeitembuttongroup.cpp
 *\brief Filter's tree widget item button group implementation
 */

#include <QHBoxLayout>
#include <QSizePolicy>

#include "filtertreeitembutton.h"
#include "filtertreeitembuttongroup.h"

namespace Ui
{
	static const int cButtonDim = 16;

	/**
	 public static:
	 */

	int FilterTreeItemButtonGroup::getGroupUniformWidth()
	{
		return cButtonDim * 3;
	}

	int FilterTreeItemButtonGroup::getGroupUniformHeight()
	{
		return cButtonDim;
	}

	/**
	 public:
	 */
	FilterTreeItemButtonGroup::FilterTreeItemButtonGroup(const QString& filter, QWidget* parent /* = NULL */)
		: QWidget(parent),
		  mShowButton(new FilterTreeItemButton(QIcon(":/icons/mShowFilterAction.png"), tr(""), filter, this)),
			mEditButton(new FilterTreeItemButton(QIcon(":/icons/mEditFilterAction.png"), tr(""), filter, this)),
			mRemoveButton(new FilterTreeItemButton(QIcon(":/icons/mRemoveFilterAction.png"), tr(""), filter, this))
	{
		// Setup layout
		mLayout = new QHBoxLayout(this);

		mLayout->setSpacing(0);
		
		mLayout->addWidget(mShowButton);
		mLayout->addWidget(mEditButton);
		mLayout->addWidget(mRemoveButton);

		mLayout->setSizeConstraint(QLayout::SetFixedSize);
		
		//mLayout->setAlignment(Qt::AlignLeft | Qt::AlignTop);


		setLayout(mLayout);
		setContentsMargins(0, 0, 0, 0);

		const QSizePolicy policy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		setSizePolicy(policy);
		mShowButton->setSizePolicy(policy);
		mEditButton->setSizePolicy(policy);
		mRemoveButton->setSizePolicy(policy);

		mShowButton->setFixedSize(cButtonDim, cButtonDim);
		mEditButton->setFixedSize(cButtonDim, cButtonDim);
		mRemoveButton->setFixedSize(cButtonDim, cButtonDim);

		mShowButton->setAutoFillBackground(true);
		mEditButton->setAutoFillBackground(true);
		mRemoveButton->setAutoFillBackground(true);
		setAutoFillBackground(true);

		connect(mShowButton,	 SIGNAL( pressed(QString) ), this, SIGNAL( showButtonPressed(QString) ));
		connect(mEditButton,	 SIGNAL( pressed(QString) ), this, SIGNAL( editButtonPressed(QString) ));
		connect(mRemoveButton, SIGNAL( pressed(QString) ), this, SIGNAL( removeButtonPressed(QString) ));

		resize(cButtonDim * 3, cButtonDim);		
		adjustSize();

		mShowButton->setToolTip(tr("Show filter parameters"));
		mEditButton->setToolTip(tr("Edit filter"));
		mRemoveButton->setToolTip(tr("Remove filter"));

		mShowButton->setStyleSheet("color: black;");
		mEditButton->setStyleSheet("color: black;");
		mRemoveButton->setStyleSheet("color: black;");
	}

	void FilterTreeItemButtonGroup::hideShowButton()
	{
		mShowButton->setVisible(false);
	}

	void FilterTreeItemButtonGroup::hideEditButton()
	{
		mEditButton->setVisible(false);
	}

	void FilterTreeItemButtonGroup::hideRemoveButton()
	{
		mRemoveButton->setVisible(false);
	}
} // namespace Ui