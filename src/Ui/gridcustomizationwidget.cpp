/*!
 *\file gridcustomizationwidget.cpp
 *\brief This file contains implementation of grid customization widget
 */

#include "gridcustomizationwidget.h"

namespace Ui
{

	/**
	 public:
	 */
	GridCustomizationWidget::GridCustomizationWidget(QWidget *parent /* = NULL */)
		: AbstractCustomizationWidget(parent)
	{
		setupUi(this);
		setFixedSize(this->size());

		cbColorBox->setStandardColors();

		// Set black color as start one
		cbColorBox->setCurrentColor(QColor(Qt::black));

		connect(dsbCellWidth,  SIGNAL( valueChanged(double) ),		 this, SLOT( onGridCellWidthChanged(double) ));
		connect(dsbCellHeight, SIGNAL( valueChanged(double) ),		 this, SLOT( onGridCellHeightChanged(double) ));
		connect(cbColorBox,		 SIGNAL( currentIndexChanged(int) ), this, SLOT( onGridColorChanged(int) ));
	}

	/**
	 public slots:
	 */
	void GridCustomizationWidget::setParams(const Data::NamedVariantMap& params)
	{
		if (params.find("width_step") != params.end())
		{
			dsbCellWidth->setValue(params["width_step"].asVariant().toFloat());
		}
		if (params.find("height_step") != params.end())
		{
			dsbCellHeight->setValue(params["height_step"].asVariant().toFloat());
		}
		if (params.find("color") != params.end())
		{
			cbColorBox->setCurrentColor(params["color"].asColor());
		}
	}

	/**
	 private slots:
	 */

	void GridCustomizationWidget::onGridCellWidthChanged(double value)
	{
		Data::NamedVariantMap params;

		params["width_step"] = Data::ExtendedVariant::toExtendedVariant(QVariant(value));

		emit paramsChanged(params);
	}

	void GridCustomizationWidget::onGridCellHeightChanged(double value)
	{
		Data::NamedVariantMap params;

		params["height_step"] = Data::ExtendedVariant::toExtendedVariant(QVariant(value));

		emit paramsChanged(params);
	}

	void GridCustomizationWidget::onGridColorChanged(int index)
	{
		Data::NamedVariantMap params;

		params["color"] = Data::ExtendedVariant().fromValue(cbColorBox->color(index));

		emit paramsChanged(params);
	}
} // namespace Ui