/*!
 *\file medianfiltercustomizationwidget.cpp
 *\brief This file contains implementation of median filter customization widget
 */

#include "medianfiltercustomizationwidget.h"
#include "Filters/medianfilter.h"

#include <QIntValidator>

namespace Ui
{

	/**
	 public:
	 */
	MedianFilterCustomizationWidget::MedianFilterCustomizationWidget(QWidget *parent /* = NULL */)
		: AbstractCustomizationWidget(parent),
		  mMinRadius(1),
			mMaxRadius(10),
		  mIgnoreRadiusChanges(false),
			mParams(Data::NamedVariantMap())
	{
		setupUi(this);
		setFixedSize(size());

		// Restrict too high radius values for both line edit & slider
		QSharedPointer<const QIntValidator> validator(new QIntValidator(mMinRadius, mMaxRadius));
		leRadius->setValidator(validator.data());
		hsRadius->setRange(mMinRadius, mMaxRadius);

		connect(leRadius,  SIGNAL( textEdited(QString) ), this, SLOT( onLineEditTextChanged(QString) ));
		connect(hsRadius,  SIGNAL( valueChanged(int) ),		 this, SLOT( onSliderValueChanged(int) ));
		connect(hsRadius,  SIGNAL( sliderPressed() ),		 this, SLOT( onSliderPressed() ));
		connect(hsRadius,  SIGNAL( sliderReleased() ),		 this, SLOT( onSliderReleased() ));
		connect(cbHybrid, SIGNAL( clicked(bool) ),		 this, SLOT( onMedianFilterTypeChanged(bool) ));
	}


	/**
	 public slots:
	 */
	void MedianFilterCustomizationWidget::setParams(const Data::NamedVariantMap& params)
	{
		mIgnoreRadiusChanges = true;

		if (params.find("radius") != params.end())
		{
			leRadius->setText(params["radius"].asVariant().toString());
			hsRadius->setValue(params["radius"].asVariant().toInt());
		}

		if (params.find("type") != params.end())
		{
			cbHybrid->setCheckState(params["type"].asVariant().toInt() == Filters::MedianFilter::Normal ? Qt::Unchecked :
																																																		Qt::Checked);
		}

		mParams = params;
		mIgnoreRadiusChanges = false;
	}

	/**
	 private slots:
	 */

	void MedianFilterCustomizationWidget::onSliderPressed()
	{
		mIgnoreRadiusChanges = true;
	}

	void MedianFilterCustomizationWidget::onSliderReleased()
	{
		mIgnoreRadiusChanges = false;

		onSliderValueChanged(hsRadius->value());
	}

	void MedianFilterCustomizationWidget::onSliderValueChanged(int value)
	{
		leRadius->setText(QVariant(value).toString());

		// If slider pressed, we're only updating value in line edit
		if (mIgnoreRadiusChanges)
			return;

		// Apply filter with new radius value otherwise		
		mParams["radius"] = Data::ExtendedVariant::toExtendedVariant(QVariant(value));
		emit paramsChanged(mParams);
	}

	void MedianFilterCustomizationWidget::onLineEditTextChanged(const QString &value)
	{
		if (mIgnoreRadiusChanges)
			return;

		int newValue = value.toInt();

		if (newValue > mMaxRadius)
		{
			newValue = mMinRadius;
			leRadius->setText(QString::number(newValue));
		}

		hsRadius->setValue(newValue);

		Data::NamedVariantMap params;
		params["radius"] = Data::ExtendedVariant::toExtendedVariant(newValue);
		emit paramsChanged(params);
	}

	void MedianFilterCustomizationWidget::onMedianFilterTypeChanged(bool isHybridChecked)
	{
		mParams["type"] = Data::ExtendedVariant::toExtendedVariant(QVariant(isHybridChecked ? Filters::MedianFilter::Hybrid :
																																												 Filters::MedianFilter::Normal));
		emit paramsChanged(mParams);
	}
} // namespace Ui