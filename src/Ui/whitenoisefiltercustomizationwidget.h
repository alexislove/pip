/*!
 *\file whitenoisefiltercustomizationwidget.h
 *\brief This file contains definition of white noise filter customization widget
 */

#ifndef UI_WHITENOISEFILTERCUSTOMIZATIONWIDGET_H
	#define UI_WHITENOISEFILTERCUSTOMIZATIONWIDGET_H

	#include "abstractcustomizationwidget.h"

	#include "ui_whitenoisefiltercustomizationwidgetform.h"

	namespace Ui
	{
		/*!
		 *\class WhiteNoiseFilterCustomizationWidget
		 *\brief This class implements customization widget for white noise filter, show over image
		 */
		class WhiteNoiseFilterCustomizationWidget : public AbstractCustomizationWidget, 
																						private WhiteNoiseFilterCustomizationWidgetForm
		{
			Q_OBJECT
		public:
			/*!
			 *\brief Constructor
			 *\param parent Parent widget
			 */
			explicit WhiteNoiseFilterCustomizationWidget(QWidget *parent = NULL);
			
		public slots:

			//! See AbstractCustomizationWidget.setParams(const Data::NamedVariantMap&)
			virtual void setParams(const Data::NamedVariantMap& params);

		private slots:

			void onProbabilityChanged(int value);
			void onMaxDisplacementChanged(int value);

		private:
			bool mIgnoreChanges;
			Data::NamedVariantMap mParams;
		};
	}
	

#endif // UI_WHITENOISEFILTERCUSTOMIZATIONWIDGET_H