/*!
 *\file mainwindow.cpp
 *\brief Application main window implementation
 */

#include <QAction>
#include <QApplication>
#include <QCloseEvent>
#include <QComboBox>
#include <QFileDialog>
#include <QIcon>
#include <QKeySequence>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QSettings>
#include <QToolBar>

#include "../Dispatchers/filtertriggersdispatcher.h"
#include "../Managers/linearfiltersmanager.h"

#include "filtereditordialog.h"
#include "viewerstabwidget.h"

#include "mainwindow.h"

using namespace Dispatchers;
using namespace Filters;
using namespace Managers;

namespace Ui
{
	namespace Private
	{
		// Common constants, that should be defined only for main window
		static const int csToolbarIconSize = 32;
		// Default open directory key for QSettings
		static const QString DEFAULT_DIR_KEY("default_dir");
	}

	struct MainWindow::Appearance
	{
		Appearance()
			: mActions(new Actions),
				mBuiltinFilters(new BuiltinFilters),
				mMenus(new Menus),
				mToolBars(new Toolbars),
				mMainMenuBar(NULL)
		{

		}

		//! Actions for main window
		struct Actions
		{
			//! Open file action
			QAction *mOpenFileAction;
			//! Save file action
			QAction *mSaveFileAction;
			//! Save as file action
			QAction *mSaveFileAsAction;
			//! Quit action
			QAction *mQuitAction;
			//! Undo action
			QAction *mUndoAction;
			//! Redo action
			QAction *mRedoAction;
			//! Filter action
			QAction *mFilterAction;
			//! Grid action
			QAction *mShowGridAction;
			//! Compare to golden image action
			QAction *mGoldenImageCompareAction;
			//! Show channel action
			QAction *mShowChannelAction;
			//! About action
			QAction *mAboutAction;
			//! Help action
			QAction *mHelpAction;
			//! Add linear filter action
			QAction *mAddLinearFilterAction;

      //! Morphological filters separator in filters menu
      QAction *mMorphologicalSeparator;
      //! Linear filters separator in filters menu
      QAction *mLinearSeparator;
      //! Add new linear filter action separator
      QAction *mAddNewLinearFilterSeparator;
		};

		//! Built-in filter actions
		struct BuiltinFilters
		{
			//! Inversion filter action
			QAction *mInversionFilterAction;
			//! Dilatation filter action
			QAction *mDilatationFilterAction;
			//! Erosion filter action
			QAction *mErosionFilterAction;
			//! Median filter action
			QAction *mMedianFilterAction;
			//! White noise filter action
			QAction *mWhiteNoiseFilterAction;
			//! Dust noise filter action
			QAction *mDustNoiseFilterAction;
			//! Bilateral filter action
			QAction *mBilateralFilterAction;
			//! Wiener filter action
			QAction *mWienerFilterAction;
			//! Warning: do not edit this line!!! $QActionDef$
		};

		//! Menus for main window
		struct Menus
		{
			//! File menu
			QMenu *mFileMenu;
			//! Edit menu
			QMenu *mEditMenu;
			//! Tools menu2
			QMenu *mToolsMenu;
			//! Filters menu
			QMenu *mFiltersMenu;
			//! Help menu
			QMenu *mHelpMenu;
		};

		//! Toolbars for main window
		struct Toolbars
		{
			//! File toolbar
			QToolBar *mFileToolBar;
			//! Edit toolbar
			QToolBar *mEditToolBar;
			//! History toolbar
			QToolBar *mHistoryToolBar;
			//! Tools toolbar
			QToolBar *mToolsToolBar;
			//! Help toolbar
			QToolBar *mHelpToolBar;
		};

		//! Actions collection
		QScopedPointer< Actions >				 mActions;
		//! Builtin filters actions collection
		QScopedPointer< BuiltinFilters > mBuiltinFilters;
		//! Menus collection
		QScopedPointer< Menus >					 mMenus;
		//! ToolBars collection
		QScopedPointer< Toolbars >			 mToolBars;
		//! Menu bar instance to hold menus
		QMenuBar												*mMainMenuBar;
		//! Status bar instance
		QStatusBar											*mStatusBar;
		//! Tab widget for viewers
		ViewersTabWidget								*mViewersTab;
		//! Keep track of custom filters actions
		QList< QAction* >							   mLinearFilterActions;
		//! History combo box
		QComboBox												*mHistoryBox;
	};

	/**
	 public:
	 */
	MainWindow::MainWindow(QWidget *parent, Qt::WindowFlags flags)
		: QMainWindow(parent, flags),
			mUi(new Appearance)
	{
		QSettings MySettings;
		QDir dir;
		// Default file open path should set to directory containing test images
		if (dir.exists("images"))
			MySettings.setValue(Private::DEFAULT_DIR_KEY, dir.absoluteFilePath("images"));
	}

	MainWindow::~MainWindow()
	{
		// No memory management, all objects with parents will be deleted
	}

	void MainWindow::buildAppearance()
	{
		createActions();
		createToolBars();
		createMenus();
		createStatusBar();
		createComponents();
		setupConnections();
		switchStartupConfiguration();
	}

	/**
	 protected:
	 */
	void MainWindow::closeEvent(QCloseEvent* e)
	{
		if (mUi->mViewersTab->isAnyDirty())
		{
			bool cancelled = false;
			mUi->mViewersTab->saveAll(&cancelled);
			// Continue work
			if (cancelled)
			{
				e->ignore();
				return;
			}
		}

		QMainWindow::closeEvent(e);
	}

	/**
	 private:
	 */
	void MainWindow::createActions()
	{
		mUi->mActions->mOpenFileAction = new QAction(QIcon(":/icons/mOpenFileAction.png"), tr("&Open file"), this);
		mUi->mActions->mOpenFileAction->setToolTip(tr("Select image to open"));
		mUi->mActions->mOpenFileAction->setStatusTip(tr("Select image to open (Ctrl + O)"));
		mUi->mActions->mOpenFileAction->setWhatsThis(tr("Select image to open"));
		mUi->mActions->mOpenFileAction->setShortcut(QKeySequence("Ctrl+O"));

		mUi->mActions->mSaveFileAction = new QAction(QIcon(":/icons/mSaveFileAction.png"), tr("&Save file"), this);
		mUi->mActions->mSaveFileAction->setToolTip(tr("Save current image"));
		mUi->mActions->mSaveFileAction->setStatusTip(tr("Save current image (Ctrl + S)"));
		mUi->mActions->mSaveFileAction->setWhatsThis(tr("Save current image"));
		mUi->mActions->mSaveFileAction->setShortcut(QKeySequence("Ctrl+S"));

		mUi->mActions->mSaveFileAsAction = new QAction(QIcon(":/icons/mSaveFileAsAction.png"), tr("&Save file as"), this);
		mUi->mActions->mSaveFileAsAction->setToolTip(tr("Select file and save image there"));
		mUi->mActions->mSaveFileAsAction->setStatusTip(tr("Select file and save image there (Ctrl + Shift + S)"));
		mUi->mActions->mSaveFileAsAction->setWhatsThis(tr("Select file and save image there"));
		mUi->mActions->mSaveFileAsAction->setShortcut(QKeySequence("Ctrl+Shift+S"));

		mUi->mActions->mQuitAction = new QAction(QIcon(":/icons/mQuitAction.png"), tr("&Quit application"), this);
		mUi->mActions->mQuitAction->setToolTip(tr("Quit application"));
		mUi->mActions->mQuitAction->setStatusTip(tr("Quit application (Ctrl + Q)"));
		mUi->mActions->mQuitAction->setWhatsThis(tr("Quit application"));
		mUi->mActions->mQuitAction->setShortcut(QKeySequence("Ctrl+Q"));

		mUi->mActions->mUndoAction = new QAction(QIcon(":/icons/mUndoAction.png"), tr("Revert last operation"), this);
		mUi->mActions->mUndoAction->setToolTip(tr("Remove changes from the last operation"));
		mUi->mActions->mUndoAction->setStatusTip(tr("Remove changes from the last operation (Ctrl + Z)"));
		mUi->mActions->mUndoAction->setWhatsThis(tr("Remove changes from the last operation"));
		mUi->mActions->mUndoAction->setShortcut(QKeySequence("Ctrl+Z"));

		mUi->mActions->mRedoAction = new QAction(QIcon(":/icons/mRedoAction.png"), tr("Apply last operation"), this);
		mUi->mActions->mRedoAction->setToolTip(tr("Apply changes from the last operation"));
		mUi->mActions->mRedoAction->setStatusTip(tr("Apply changes from the last operation (Ctrl + Y)"));
		mUi->mActions->mRedoAction->setWhatsThis(tr("Apply changes from the last operation"));
		mUi->mActions->mRedoAction->setShortcut(QKeySequence("Ctrl+Y"));

		mUi->mActions->mShowGridAction = new QAction(QIcon(":/icons/mShowGridAction.png"), tr("Show &grid"), this);
		mUi->mActions->mShowGridAction->setToolTip(tr("Show grid over image"));
		mUi->mActions->mShowGridAction->setStatusTip(tr("Show grid over image (Ctrl + G)"));
		mUi->mActions->mShowGridAction->setWhatsThis(tr("Show grid over image"));
		mUi->mActions->mShowGridAction->setShortcut(QKeySequence("Ctrl+G"));
		mUi->mActions->mShowGridAction->setCheckable(true); // Action has 2 states
		mUi->mActions->mShowGridAction->setChecked(false);	// Defaults to not checked

		mUi->mActions->mGoldenImageCompareAction = new QAction(QIcon(":/icons/mGoldenImageCompareAction.png"), tr("Compare current image with &reference one"), this);
		mUi->mActions->mGoldenImageCompareAction->setToolTip(tr("Compare current image with the reference image"));
		mUi->mActions->mGoldenImageCompareAction->setStatusTip(tr("Compare current image with the reference image (Ctrl + R)"));
		mUi->mActions->mGoldenImageCompareAction->setWhatsThis(tr("Compare current image with the reference image"));
		mUi->mActions->mGoldenImageCompareAction->setShortcut(QKeySequence("Ctrl+R"));

		mUi->mActions->mShowChannelAction = new QAction(QIcon(":/icons/mShowChannelAction.png"), tr("Show specific color channel in selected model"), this);
		mUi->mActions->mShowChannelAction->setToolTip(tr("Show specific color channel in selected model"));
		mUi->mActions->mShowChannelAction->setStatusTip(tr("Show specific color channel in selected model (Ctrl + C)"));
		mUi->mActions->mShowChannelAction->setWhatsThis(tr("Show specific color channel in selected model"));
		mUi->mActions->mShowChannelAction->setShortcut(QKeySequence("Ctrl+C"));

		mUi->mActions->mAboutAction = new QAction(QIcon(":/icons/mAboutAction.png"), tr("About"), this);
		mUi->mActions->mAboutAction->setToolTip(tr("Show application info"));
		mUi->mActions->mAboutAction->setStatusTip(tr("Show application info (F2)"));
		mUi->mActions->mAboutAction->setWhatsThis(tr("Show application info"));
		mUi->mActions->mAboutAction->setShortcut(QKeySequence("F2"));

		mUi->mActions->mHelpAction = new QAction(QIcon(":/icons/mHelpAction.png"), tr("Help"), this);
		mUi->mActions->mHelpAction->setToolTip(tr("Show help contents"));
		mUi->mActions->mHelpAction->setStatusTip(tr("Show help contents (F1)"));
		mUi->mActions->mHelpAction->setWhatsThis(tr("Show help contents"));
		mUi->mActions->mHelpAction->setShortcut(QKeySequence("F1"));

		mUi->mActions->mAddLinearFilterAction = new QAction(QIcon(":/icons/mAddLinearFilterAction.png"), tr("Add &linear filter"), this);
		mUi->mActions->mAddLinearFilterAction->setToolTip(tr("Add new linear filter to the collection"));
		mUi->mActions->mAddLinearFilterAction->setStatusTip(tr("Add new linear filter to the collection (Ctrl + Shift + L)"));
		mUi->mActions->mAddLinearFilterAction->setWhatsThis(tr("Add new linear filter to the collection"));
		mUi->mActions->mAddLinearFilterAction->setShortcut(QKeySequence("Ctrl+Shift+L"));

		// Create builtin filters' actions
		mUi->mBuiltinFilters->mInversionFilterAction = new QAction(tr("Inversion"), this);
		mUi->mBuiltinFilters->mInversionFilterAction->setToolTip(tr("Inversion filter"));
		mUi->mBuiltinFilters->mInversionFilterAction->setStatusTip(tr("Inversion filter (Ctrl + Shift + 1)"));
		mUi->mBuiltinFilters->mInversionFilterAction->setWhatsThis(tr("Inversion filter"));
		//mUi->mBuiltinFilters->mInversionFilterAction->setShortcut(QKeySequence("Ctrl+Shift+1"));

		mUi->mBuiltinFilters->mDilatationFilterAction = new QAction(tr("Dilation"), this);
		mUi->mBuiltinFilters->mDilatationFilterAction->setToolTip(tr("Dilation filter"));
		mUi->mBuiltinFilters->mDilatationFilterAction->setStatusTip(tr("Dilation filter (Ctrl + Shift + 2)"));
		mUi->mBuiltinFilters->mDilatationFilterAction->setWhatsThis(tr("Dilation filter"));
		mUi->mBuiltinFilters->mDilatationFilterAction->setShortcut(QKeySequence("Ctrl+Shift+2"));

		mUi->mBuiltinFilters->mErosionFilterAction = new QAction(tr("Erosion"), this);
		mUi->mBuiltinFilters->mErosionFilterAction->setToolTip(tr("Erosion filter"));
		mUi->mBuiltinFilters->mErosionFilterAction->setStatusTip(tr("Erosion filter (Ctrl + Shift + 3)"));
		mUi->mBuiltinFilters->mErosionFilterAction->setWhatsThis(tr("Erosion filter"));
		mUi->mBuiltinFilters->mErosionFilterAction->setShortcut(QKeySequence("Ctrl+Shift+3"));

		mUi->mBuiltinFilters->mMedianFilterAction = new QAction(tr("Median"), this);
		mUi->mBuiltinFilters->mMedianFilterAction->setToolTip(tr("Median filter"));
		mUi->mBuiltinFilters->mMedianFilterAction->setStatusTip(tr("Median filter"));
		mUi->mBuiltinFilters->mMedianFilterAction->setWhatsThis(tr("Median filter"));
		mUi->mBuiltinFilters->mMedianFilterAction->setShortcut(QKeySequence("Ctrl+Shift+4"));

		mUi->mBuiltinFilters->mWhiteNoiseFilterAction = new QAction(tr("White noise"), this);
		mUi->mBuiltinFilters->mWhiteNoiseFilterAction->setToolTip(tr("White noise filter"));
		mUi->mBuiltinFilters->mWhiteNoiseFilterAction->setStatusTip(tr("White noise filter"));
		mUi->mBuiltinFilters->mWhiteNoiseFilterAction->setWhatsThis(tr("White noise filter"));
		mUi->mBuiltinFilters->mWhiteNoiseFilterAction->setShortcut(QKeySequence("Ctrl+Shift+5"));

		mUi->mBuiltinFilters->mDustNoiseFilterAction = new QAction(tr("Dust noise"), this);
		mUi->mBuiltinFilters->mDustNoiseFilterAction->setToolTip(tr("Dust noise filter"));
		mUi->mBuiltinFilters->mDustNoiseFilterAction->setStatusTip(tr("Dust noise filter"));
		mUi->mBuiltinFilters->mDustNoiseFilterAction->setWhatsThis(tr("Dust noise filter"));
		mUi->mBuiltinFilters->mDustNoiseFilterAction->setShortcut(QKeySequence("Ctrl+Shift+6"));

		mUi->mBuiltinFilters->mBilateralFilterAction = new QAction(tr("Bilateral"), this);
		mUi->mBuiltinFilters->mBilateralFilterAction->setToolTip(tr("Bilateral filter"));
		mUi->mBuiltinFilters->mBilateralFilterAction->setStatusTip(tr("Bilateral filter"));
		mUi->mBuiltinFilters->mBilateralFilterAction->setWhatsThis(tr("Bilateral filter"));
		mUi->mBuiltinFilters->mBilateralFilterAction->setShortcut(QKeySequence("Ctrl+Shift+7"));

		mUi->mBuiltinFilters->mWienerFilterAction = new QAction(tr("Wiener"), this);
		mUi->mBuiltinFilters->mWienerFilterAction->setToolTip(tr("Wiener filter"));
		mUi->mBuiltinFilters->mWienerFilterAction->setStatusTip(tr("Wiener filter"));
		mUi->mBuiltinFilters->mWienerFilterAction->setWhatsThis(tr("Wiener filter"));
		mUi->mBuiltinFilters->mWienerFilterAction->setShortcut(QKeySequence("Ctrl+Shift+1"));

		//! Warning: do not edit this line!!! $QActionCreate$
	}

	void MainWindow::createToolBars()
	{
		using namespace Private;
		mUi->mToolBars->mFileToolBar = addToolBar(tr("File toolbar"));
		mUi->mToolBars->mFileToolBar->addAction(mUi->mActions->mOpenFileAction);
		mUi->mToolBars->mFileToolBar->addAction(mUi->mActions->mSaveFileAction);
		mUi->mToolBars->mFileToolBar->addAction(mUi->mActions->mSaveFileAsAction);
		mUi->mToolBars->mFileToolBar->setIconSize(QSize(csToolbarIconSize, csToolbarIconSize));
		

		mUi->mToolBars->mEditToolBar = addToolBar(tr("Edit toolbar"));
		mUi->mToolBars->mEditToolBar->addAction(mUi->mActions->mUndoAction);
		mUi->mToolBars->mEditToolBar->addAction(mUi->mActions->mRedoAction);
		mUi->mToolBars->mEditToolBar->setIconSize(QSize(csToolbarIconSize, csToolbarIconSize));

		mUi->mToolBars->mHistoryToolBar = addToolBar(tr("History toolbar"));
		mUi->mToolBars->mHistoryToolBar->setIconSize(QSize(csToolbarIconSize, csToolbarIconSize));

		mUi->mHistoryBox = new QComboBox(this);
		mUi->mHistoryBox->setMinimumWidth(200);
		mUi->mToolBars->mHistoryToolBar->addWidget(mUi->mHistoryBox);

		mUi->mToolBars->mToolsToolBar = addToolBar(tr("Tools toolbar"));
		mUi->mToolBars->mToolsToolBar->addAction(mUi->mActions->mShowGridAction);
		mUi->mToolBars->mToolsToolBar->addAction(mUi->mActions->mGoldenImageCompareAction);
		mUi->mToolBars->mToolsToolBar->addAction(mUi->mActions->mShowChannelAction);
		mUi->mToolBars->mToolsToolBar->setIconSize(QSize(csToolbarIconSize, csToolbarIconSize));

		mUi->mToolBars->mHelpToolBar = addToolBar(tr("Help toolbar"));
		mUi->mToolBars->mHelpToolBar->addAction(mUi->mActions->mAboutAction);
		mUi->mToolBars->mHelpToolBar->addAction(mUi->mActions->mHelpAction);
		mUi->mToolBars->mHelpToolBar->setIconSize(QSize(csToolbarIconSize, csToolbarIconSize));
	}

	void MainWindow::createMenus()
	{
		mUi->mMainMenuBar = menuBar();

		mUi->mMenus->mFileMenu = mUi->mMainMenuBar->addMenu(tr("File"));
		mUi->mMenus->mFileMenu->addAction(mUi->mActions->mOpenFileAction);
		mUi->mMenus->mFileMenu->addAction(mUi->mActions->mSaveFileAction);
		mUi->mMenus->mFileMenu->addAction(mUi->mActions->mSaveFileAsAction);
		mUi->mMenus->mFileMenu->addAction(mUi->mActions->mQuitAction);

		mUi->mMenus->mEditMenu = mUi->mMainMenuBar->addMenu(tr("Edit"));
		mUi->mMenus->mEditMenu->addAction(mUi->mActions->mUndoAction);
		mUi->mMenus->mEditMenu->addAction(mUi->mActions->mRedoAction);

		mUi->mMenus->mToolsMenu			 = mUi->mMainMenuBar->addMenu(tr("Tools"));
		mUi->mMenus->mFiltersMenu		 = mUi->mMenus->mToolsMenu->addMenu(tr("Filters")); // Add submenu
		mUi->mActions->mMorphologicalSeparator = mUi->mMenus->mFiltersMenu->addSeparator();
		mUi->mMenus->mFiltersMenu->addAction(mUi->mBuiltinFilters->mInversionFilterAction);
		mUi->mMenus->mFiltersMenu->addAction(mUi->mBuiltinFilters->mDilatationFilterAction);
		mUi->mMenus->mFiltersMenu->addAction(mUi->mBuiltinFilters->mErosionFilterAction);
		mUi->mActions->mLinearSeparator = mUi->mMenus->mFiltersMenu->addSeparator();		
		mUi->mMenus->mFiltersMenu->addAction(mUi->mBuiltinFilters->mMedianFilterAction);
		mUi->mMenus->mFiltersMenu->addAction(mUi->mBuiltinFilters->mWhiteNoiseFilterAction);
		mUi->mMenus->mFiltersMenu->addAction(mUi->mBuiltinFilters->mDustNoiseFilterAction);
		mUi->mMenus->mFiltersMenu->addAction(mUi->mBuiltinFilters->mBilateralFilterAction);
		mUi->mMenus->mFiltersMenu->addAction(mUi->mBuiltinFilters->mWienerFilterAction);
		//! Warning: do not edit this line!!! $QActionAddToMenu$
		mUi->mActions->mLinearSeparator = mUi->mMenus->mFiltersMenu->addSeparator();		

    // Add separator and add new filter action
    mUi->mActions->mAddNewLinearFilterSeparator = mUi->mMenus->mFiltersMenu->addSeparator();
		// Load linear filters
		// Register actions at dispatcher 
		FilterTriggersDispatcher& dispatcher = FilterTriggersDispatcher::getInstance();
		
		dispatcher.addFilterAction(mUi->mBuiltinFilters->mInversionFilterAction);
		dispatcher.addFilterAction(mUi->mBuiltinFilters->mDilatationFilterAction);
		dispatcher.addFilterAction(mUi->mBuiltinFilters->mErosionFilterAction);
		dispatcher.addFilterAction(mUi->mBuiltinFilters->mMedianFilterAction);
		dispatcher.addFilterAction(mUi->mBuiltinFilters->mWhiteNoiseFilterAction);
		dispatcher.addFilterAction(mUi->mBuiltinFilters->mDustNoiseFilterAction);
		dispatcher.addFilterAction(mUi->mBuiltinFilters->mBilateralFilterAction);
		dispatcher.addFilterAction(mUi->mBuiltinFilters->mWienerFilterAction);
		//! Warning: do not edit this line!!! $QActionAddToDispatcher$

    onRefreshLinearFilters();

		mUi->mMenus->mFiltersMenu->addAction(mUi->mActions->mAddLinearFilterAction);

		mUi->mActions->mFilterAction = mUi->mMenus->mFiltersMenu->menuAction();			 // Store menu action
		
		// Continue with tools menu
		mUi->mMenus->mToolsMenu->addAction(mUi->mActions->mShowGridAction);
		mUi->mMenus->mToolsMenu->addAction(mUi->mActions->mGoldenImageCompareAction);
		mUi->mMenus->mToolsMenu->addAction(mUi->mActions->mShowChannelAction);

		mUi->mMenus->mHelpMenu = mUi->mMainMenuBar->addMenu(tr("Help"));
		mUi->mMenus->mHelpMenu->addAction(mUi->mActions->mAboutAction);
		mUi->mMenus->mHelpMenu->addAction(mUi->mActions->mHelpAction);
	}

	void MainWindow::createStatusBar()
	{
		mUi->mStatusBar = statusBar();
	}

	void MainWindow::createComponents()
	{
		mUi->mViewersTab = new ViewersTabWidget(this);
	}

	// Auxiliary macro to simplify actions to window connection
	#define CONNECT_ACTION(action, slot) connect((action), SIGNAL( triggered() ), this, SLOT( slot ))

	void MainWindow::setupConnections()
	{
		CONNECT_ACTION(mUi->mActions->mOpenFileAction,					 onOpenFileAction());
		CONNECT_ACTION(mUi->mActions->mSaveFileAction,					 onSaveFileAction());
		CONNECT_ACTION(mUi->mActions->mSaveFileAsAction,				 onSaveFileAsAction());
		CONNECT_ACTION(mUi->mActions->mQuitAction,							 onQuitAction());
		CONNECT_ACTION(mUi->mActions->mUndoAction,							 onUndoAction());
		CONNECT_ACTION(mUi->mActions->mRedoAction,							 onRedoAction());
		CONNECT_ACTION(mUi->mActions->mShowGridAction,					 onShowGridAction());
		CONNECT_ACTION(mUi->mActions->mGoldenImageCompareAction, onGoldenImageCompareAction());
		CONNECT_ACTION(mUi->mActions->mShowChannelAction,				 onShowChannelAction());
		CONNECT_ACTION(mUi->mActions->mAboutAction,							 onAboutAction());
		CONNECT_ACTION(mUi->mActions->mHelpAction,							 onHelpAction());
		CONNECT_ACTION(mUi->mActions->mAddLinearFilterAction,		 onAddLinearFilterAction());

		connect(mUi->mViewersTab, SIGNAL( currentChanged(int) ),									 this, SLOT( onCurrentViewerChanged(int) ));
		connect(mUi->mViewersTab, SIGNAL( tabClosed() ),													 this, SLOT( onViewerClosed() ));
		connect(mUi->mViewersTab, SIGNAL( processorCommandApplied() ),						 this, SLOT( onProcessorCommandApplied() ));
    connect(mUi->mViewersTab, SIGNAL( someoneAddedLinearFilter(QWidget*) ),		 this, SLOT( onLinearFiltersSetUpdated(QWidget*) ));
		connect(mUi->mViewersTab, SIGNAL( currentGridVisibleStateResponse(bool) ), this, SLOT( onCurrentGridVisibleStateResponse(bool) ));

		connect(mUi->mHistoryBox, SIGNAL( currentIndexChanged(int) ), this, SLOT( onCurrentHistoryItemChanged(int) ));

    connect(this, SIGNAL( newLinearFilterAdded(QWidget*) ),		 mUi->mViewersTab, SIGNAL( someoneAddedLinearFilter(QWidget*) ));
		connect(this, SIGNAL( showGrid() ),												 mUi->mViewersTab, SLOT( onShowGridOnCurrent() ));
		connect(this, SIGNAL( hideGrid() ),												 mUi->mViewersTab, SLOT( onHideGridOnCurrent() ));
		connect(this, SIGNAL( currentGridVisibleStateRequest() ),  mUi->mViewersTab, SLOT( onCurrentGridVisibleRequest() ));
		connect(this, SIGNAL( compareCurrentToGolden() ),					 mUi->mViewersTab, SLOT( onCurrentCompareToGolden() ));
		connect(this, SIGNAL( showChannel() ),										 mUi->mViewersTab, SLOT( onShowChannel() ));
	}

	#undef CONNECT_ACTION

	void MainWindow::switchStartupConfiguration()
	{
		// Set viewers tab as the central widget
		setCentralWidget(mUi->mViewersTab);

		mUi->mActions->mSaveFileAction->setEnabled(false);
		mUi->mActions->mSaveFileAsAction->setEnabled(false);
		mUi->mActions->mUndoAction->setEnabled(false);
		mUi->mActions->mRedoAction->setEnabled(false);

		updateFiltersActionsState();
		updateToolsActionsState();
		updateHistoryBox();
	}

	void MainWindow::updateUndoRedoActionsState()
	{
		mUi->mActions->mUndoAction->setEnabled(mUi->mViewersTab->canCurrentUndo());
		mUi->mActions->mRedoAction->setEnabled(mUi->mViewersTab->canCurrentRedo());
	}

	void MainWindow::updateSaveActionsState()
	{
		mUi->mActions->mSaveFileAction->setEnabled(mUi->mViewersTab->isCurrentDirty());
		mUi->mActions->mSaveFileAsAction->setEnabled(mUi->mViewersTab->count() != 0);
	}

	void MainWindow::updateFiltersActionsState()
	{
		bool enable = (mUi->mViewersTab->count() != 0);
		mUi->mBuiltinFilters->mInversionFilterAction->setEnabled(enable);
		mUi->mBuiltinFilters->mDilatationFilterAction->setEnabled(enable);
		mUi->mBuiltinFilters->mErosionFilterAction->setEnabled(enable);
		mUi->mBuiltinFilters->mMedianFilterAction->setEnabled(enable);
		mUi->mBuiltinFilters->mWhiteNoiseFilterAction->setEnabled(enable);
		mUi->mBuiltinFilters->mDustNoiseFilterAction->setEnabled(enable);
		mUi->mBuiltinFilters->mBilateralFilterAction->setEnabled(enable);
		mUi->mBuiltinFilters->mWienerFilterAction->setEnabled(enable);
		//! Warning: do not edit this line!!! $QActionEnable$

		foreach (QAction* action, mUi->mLinearFilterActions)
		{
			action->setEnabled(enable);
		}
	}

	void MainWindow::updateToolsActionsState()
	{
		bool enable = (mUi->mViewersTab->count() != 0);
		mUi->mActions->mShowGridAction->setEnabled(enable);
		mUi->mActions->mGoldenImageCompareAction->setEnabled(enable);
		mUi->mActions->mShowChannelAction->setEnabled(enable);
	}

	void MainWindow::updateHistoryBox()
	{
		// Disconnect corresponding signal for some time to avoid recursive updates
		disconnect(mUi->mHistoryBox, SIGNAL( currentIndexChanged(int) ), this, SLOT( onCurrentHistoryItemChanged(int) ));
		mUi->mHistoryBox->clear();

		if (mUi->mViewersTab->count() == 0)
		{
			mUi->mHistoryBox->addItem(tr("No actions performed yet..."));
			// Restore connection
			connect(mUi->mHistoryBox, SIGNAL( currentIndexChanged(int) ), this, SLOT( onCurrentHistoryItemChanged(int) ));
			return;
		}

		const QStringList currentHistory = mUi->mViewersTab->getCurrentImageHistory();
		
		mUi->mHistoryBox->addItems(QStringList(tr("Initial")) << currentHistory); // Start from empty string

		const int currentIndex = mUi->mViewersTab->getCurrentOperationIndex();
		if (currentIndex >= 0)
		{
			Q_ASSERT(currentIndex < mUi->mHistoryBox->count());			
			mUi->mHistoryBox->setCurrentIndex(currentIndex);
			
		}
		// Restore connection
		connect(mUi->mHistoryBox, SIGNAL( currentIndexChanged(int) ), this, SLOT( onCurrentHistoryItemChanged(int) ));
	}

	/**
	 private slots:
	 */

	void MainWindow::onOpenFileAction()
	{
		QSettings MySettings;
		QDir CurrentDir;

		QString fileName = QFileDialog::getOpenFileName(this,
																										tr("Select image file to open"),
																										MySettings.value(Private::DEFAULT_DIR_KEY).toString(),
																										tr("Images (*.jpg *.png *.bmp *.tif)"));
		if (fileName.isEmpty())
		{
			return;
		}

		// Save default directory for later use
		MySettings.setValue(Private::DEFAULT_DIR_KEY, CurrentDir.absoluteFilePath(fileName));

		// Open image in new tab
		mUi->mViewersTab->openFile(fileName);

		// Enable save as action
		mUi->mActions->mSaveFileAsAction->setEnabled(true);
	}

	void MainWindow::onSaveFileAction()
	{
		mUi->mViewersTab->saveCurrent();
	}

	void MainWindow::onSaveFileAsAction()
	{
		QString selectedFilter;
		QString fileName = QFileDialog::getSaveFileName(this,
																										tr("Select image file to save"),
																										QApplication::applicationDirPath(),
																										tr("JPG image (*.jpg);;PNG image (*.png);;BMP image (*.bmp);;TIFF image (*.tif)"),
																										&selectedFilter);
		if (fileName.isEmpty())
		{
			return;
		}

		mUi->mViewersTab->saveAs(fileName);
	}

	void MainWindow::onQuitAction()
	{
		// Close window, all the others will be closed too
		close();
	}

	void MainWindow::onUndoAction()
	{
		mUi->mViewersTab->undo();
		updateSaveActionsState();
		updateUndoRedoActionsState();
	}

	void MainWindow::onRedoAction()
	{
		mUi->mViewersTab->redo();
		updateSaveActionsState();
		updateUndoRedoActionsState();
	}

	void MainWindow::onShowGridAction()
	{
		if (mUi->mActions->mShowGridAction->isChecked())
		{
			emit showGrid();
		}
		else
		{
			emit hideGrid();
		}
	}

	void MainWindow::onGoldenImageCompareAction()
	{
		emit compareCurrentToGolden();
	}

	void MainWindow::onShowChannelAction()
	{
		emit showChannel();
	}

	void MainWindow::onAboutAction()
	{
	}

	void MainWindow::onHelpAction()
	{
	}

	void MainWindow::onAddLinearFilterAction()
	{
		FilterEditorDialog* dialog = new FilterEditorDialog(this);

		LinearFilterDescriptor descriptor;
		if (!dialog->execute(&descriptor))
		{
			return;
		}

		// One of the validness checks
		if (descriptor.Name.isEmpty())
		{
			return;
		}

		LinearFiltersManager manager;
		manager.registerDescriptor(descriptor, dialog->shouldSaveOnDisk());

		dialog->deleteLater();
		
    // Update filters actions
    onRefreshLinearFilters();

    emit newLinearFilterAdded(this);
	}

	void MainWindow::onCurrentViewerChanged(int)
	{
		emit currentGridVisibleStateRequest();

		updateSaveActionsState();
		updateUndoRedoActionsState();	
		updateFiltersActionsState();
		updateToolsActionsState();
		updateHistoryBox();
	}

	void MainWindow::onViewerClosed()
	{
		updateSaveActionsState();
		updateUndoRedoActionsState();
		updateFiltersActionsState();
		updateToolsActionsState();
		updateHistoryBox();
	}

	void MainWindow::onProcessorCommandApplied()
	{
		updateSaveActionsState();
		updateUndoRedoActionsState();
		updateHistoryBox();
	}

  void MainWindow::onLinearFiltersSetUpdated(QWidget* whoUpdated)
  {
    if (whoUpdated == this)
    {
      return;
    }

    onRefreshLinearFilters();
  }

  void MainWindow::onRefreshLinearFilters()
  {
    foreach (QAction* action, mUi->mLinearFilterActions)
    {
      mUi->mMenus->mFiltersMenu->removeAction(action);
    }

    mUi->mLinearFilterActions.clear();

    // Create new actions for builtin filters
    FilterTriggersDispatcher& dispatcher = FilterTriggersDispatcher::getInstance();
    LinearFiltersManager manager;
    QStringList customFilters = manager.getRegisteredFiltersNames();

    foreach (const QString& filter, customFilters)
    {
      QAction* action = new QAction(filter, this);
      dispatcher.addFilterAction(action);
      mUi->mLinearFilterActions.push_back(action);
    }

    mUi->mMenus->mFiltersMenu->insertActions(mUi->mActions->mAddNewLinearFilterSeparator, mUi->mLinearFilterActions);
  }

	void MainWindow::onCurrentHistoryItemChanged(int index)
	{
		if (mUi->mViewersTab->count() == 0)
			return;

		mUi->mViewersTab->setCurrentOperationIndex(index);
		mUi->mViewersTab->updateCurrent();
		updateSaveActionsState();
		updateUndoRedoActionsState();
		updateHistoryBox();
	}

	void MainWindow::onCurrentGridVisibleStateResponse(bool visible)
	{
		mUi->mActions->mShowGridAction->setChecked(visible);
	}
} // namespace Ui
