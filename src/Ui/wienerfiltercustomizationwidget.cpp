/*!
 *\file wienerfiltercustomizationwidget.cpp
 *\brief This file contains implementation of Wiener filter customization widget
 */

#include "wienerfiltercustomizationwidget.h"
#include "../Filters/wienerfilter.h"

#include <QFormLayout>
#include <QSlider>

namespace Ui
{
	/**
	 public:
	 */
	WienerFilterCustomizationWidget::WienerFilterCustomizationWidget(QWidget *parent /* = NULL */)
		: AbstractCustomizationWidget(parent),
		  mIgnoreChanges(false),
			mParams(Data::NamedVariantMap())
	{
		setupUi(this);

		hsRadius->setRange(1, 10);
		hsSigma->setRange(1, 100);

		connect(hsRadius, SIGNAL( valueChanged(int) ), this, SLOT( onRadiusChanged(int) ));
		connect(hsSigma,	SIGNAL( valueChanged(int) ), this, SLOT( onSigmaChanged(int) ));
	}

	void WienerFilterCustomizationWidget::setupUi(QWidget *widget)
	{
		if (widget->objectName().isEmpty())
		{
			widget->setObjectName("WienerFilterCustomizationWidgetForm");
		}
		widget->resize(260, 100);
		widget->setFixedSize(widget->size());
		widget->setStyleSheet(QLatin1String("background-color: rgb(0, 0, 0);\n"
																				"color: rgb(255, 255, 255);"));

		hsRadius = new QSlider(widget);
		hsRadius->setOrientation(Qt::Horizontal);
		hsSigma = new QSlider(widget);
		hsSigma->setOrientation(Qt::Horizontal);
		
		QFormLayout *formLayout = new QFormLayout;
		formLayout->addRow(tr("&Radius"), hsRadius);
		formLayout->addRow(tr("&Sigma"), hsSigma);
		setLayout(formLayout);

		QMetaObject::connectSlotsByName(widget);
	}

	/**
	 public slots:
	 */
	void WienerFilterCustomizationWidget::setParams(const Data::NamedVariantMap& params)
	{
		mIgnoreChanges = true;

		if (params.find("radius") != params.end())
		{
			hsRadius->setValue(params["radius"].asVariant().toInt());
		}

		if (params.find("sigma") != params.end())
		{
			hsSigma->setValue(params["sigma"].asVariant().toFloat() * 100.f);
		}

		mParams = params;
		mIgnoreChanges = false;
	}

	/**
	 private slots:
	 */

	void WienerFilterCustomizationWidget::onRadiusChanged(int value)
	{
		//lRadius->setText(QVariant(value).toString());

		if (mIgnoreChanges)
			return;

		mParams["radius"] = Data::ExtendedVariant::toExtendedVariant(QVariant(value));

		emit paramsChanged(mParams);
	}

	void WienerFilterCustomizationWidget::onSigmaChanged(int value)
	{
		if (mIgnoreChanges)
			return;

		mParams["sigma"] = Data::ExtendedVariant::toExtendedVariant(QVariant(value / 100.f));

		emit paramsChanged(mParams);
	}

} // namespace Ui