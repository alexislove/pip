/*!
 *\file gridcustomizationwidget.h
 *\brief This file contains definition of grid customization widget
 */

#ifndef UI_GRIDCUSTOMIZATIONWIDGET_H
	#define UI_GRIDCUSTOMIZATIONWIDGET_H

	#include "abstractcustomizationwidget.h"

	#include "ui_gridcustomizationwidgetform.h"

	namespace Ui
	{
		/*!
		 *\class GridCustomizationWidget
		 *\brief This class implements customization widget for grid, show over image
		 */
		class GridCustomizationWidget : public AbstractCustomizationWidget, 
																		private GridCustomizationWidgetForm
		{
			Q_OBJECT
		public:
			/*!
			 *\brief Constructor
			 *\param parent Parent widget
			 */
			explicit GridCustomizationWidget(QWidget *parent = NULL);
			
		public slots:

			//! See AbstractCustomizationWidget.setParams(const Data::NamedVariantMap&)
			virtual void setParams(const Data::NamedVariantMap& params);

		private slots:

			/*!
			 *\brief Handle grid cell width value changed
			 *\param value New value
			 */
			void onGridCellWidthChanged(double value);

			/*!
			 *\brief Handle grid cell height value changed
			 *\param value New value
			 */
			void onGridCellHeightChanged(double value);

			/*!
			 *\brief Handle grid color change
			 *\param index Index of new color
			 */
			void onGridColorChanged(int index);
		};
	}
	

#endif // UI_GRIDCUSTOMIZATIONWIDGET_H