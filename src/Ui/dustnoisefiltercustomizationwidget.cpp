/*!
 *\file dustnoisefiltercustomizationwidget.cpp
 *\brief This file contains implementation of dust noise filter customization widget
 */

#include "dustnoisefiltercustomizationwidget.h"
#include "Filters/dustnoisefilter.h"

namespace Ui
{

	/**
	 public:
	 */
	DustNoiseFilterCustomizationWidget::DustNoiseFilterCustomizationWidget(QWidget *parent /* = NULL */)
		: AbstractCustomizationWidget(parent),
		  mIgnoreChanges(false),
			mParams(Data::NamedVariantMap())
	{
		setupUi(this);
		setFixedSize(size());

		hsProbability->setRange(0, 100);
		hsMinAddition->setRange(0, 255);

		connect(hsProbability,  SIGNAL( valueChanged(int) ),		 this, SLOT( onProbabilityChanged(int) ));
		connect(hsMinAddition,  SIGNAL( valueChanged(int) ),		 this, SLOT( onMinAdditionChanged(int) ));
	}


	/**
	 public slots:
	 */
	void DustNoiseFilterCustomizationWidget::setParams(const Data::NamedVariantMap& params)
	{
		mIgnoreChanges = true;

		if (params.find("Probability") != params.end())
		{
			hsProbability->setValue(params["Probability"].asVariant().toFloat() * 100);
		}

		if (params.find("MinAddition") != params.end())
		{
			hsMinAddition->setValue(params["MinAddition"].asVariant().toFloat() * 255);
		}

		mParams = params;
		mIgnoreChanges = false;
	}

	/**
	 private slots:
	 */

	void DustNoiseFilterCustomizationWidget::onProbabilityChanged(int value)
	{
		lProbability->setText(QVariant(value).toString());

		if (mIgnoreChanges)
			return;

		mParams["Probability"] = Data::ExtendedVariant::toExtendedVariant(QVariant(value / 100.f));
		emit paramsChanged(mParams);
	}

	void DustNoiseFilterCustomizationWidget::onMinAdditionChanged(int value)
	{
		lMinAddition->setText(QVariant(value).toString());

		if (mIgnoreChanges)
			return;

		mParams["MinAddition"] = Data::ExtendedVariant::toExtendedVariant(QVariant(value / 255.f));
		emit paramsChanged(mParams);
	}

} // namespace Ui