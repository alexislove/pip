/*!
 *\file bilateralfiltercustomizationwidget.cpp
 *\brief This file contains implementation of bilateral filter customization widget
 */

#include "bilateralfiltercustomizationwidget.h"
#include "../Filters/bilateralfilter.h"

#include <QFormLayout>
#include <QSlider>

namespace Ui
{
	/**
	 public:
	 */
	BilateralFilterCustomizationWidget::BilateralFilterCustomizationWidget(QWidget *parent /* = NULL */)
		: AbstractCustomizationWidget(parent),
		  mIgnoreChanges(false),
			mParams(Data::NamedVariantMap())
	{
		setupUi(this);

		mSigmaSpatialScale = 1.f;
		mSigmaRadimetricScale = 0.3f;

		hsRadius->setRange(1, 4);
		hsSigmaSpatial->setRange(1, mSigmaSpatialScale * 100);
		hsSigmaRadiometric->setRange(1, mSigmaRadimetricScale * 100);

		connect(hsRadius,						SIGNAL( valueChanged(int) ), this, SLOT( onRadiusChanged(int) ));
		connect(hsSigmaSpatial,			SIGNAL( valueChanged(int) ), this, SLOT( onSigmaSpatialChanged(int) ));
		connect(hsSigmaRadiometric, SIGNAL( valueChanged(int) ), this, SLOT( onSigmaRadiometricChanged(int) ));
	}

	void BilateralFilterCustomizationWidget::setupUi(QWidget *widget)
	{
		if (widget->objectName().isEmpty())
		{
			widget->setObjectName("BilateralFilterCustomizationWidgetForm");
		}
		widget->resize(260, 100);
		widget->setFixedSize(widget->size());
		widget->setStyleSheet(QLatin1String("background-color: rgb(0, 0, 0);\n"
																				"color: rgb(255, 255, 255);"));

		hsRadius = new QSlider(widget);
		hsRadius->setOrientation(Qt::Horizontal);
		hsSigmaSpatial = new QSlider(widget);
		hsSigmaSpatial->setOrientation(Qt::Horizontal);
		hsSigmaRadiometric = new QSlider(widget);
		hsSigmaRadiometric->setOrientation(Qt::Horizontal);

		QFormLayout *formLayout = new QFormLayout;
		formLayout->addRow(tr("&Radius"), hsRadius);
		formLayout->addRow(tr("&Sigma spatial"), hsSigmaSpatial);
		formLayout->addRow(tr("&Sigma radiometric"), hsSigmaRadiometric);
		setLayout(formLayout);

		QMetaObject::connectSlotsByName(widget);
	}

	/**
	 public slots:
	 */
	void BilateralFilterCustomizationWidget::setParams(const Data::NamedVariantMap& params)
	{
		mIgnoreChanges = true;

		if (params.find("Radius") != params.end())
		{
			hsRadius->setValue(params["Radius"].asVariant().toInt());
		}
		
		if (params.find("SigmaSpatial") != params.end())
		{
			hsSigmaSpatial->setValue(params["SigmaSpatial"].asVariant().toFloat() * 100);
		}

		if (params.find("SigmaRadiometric") != params.end())
		{
			hsSigmaRadiometric->setValue(params["SigmaRadiometric"].asVariant().toFloat() * 100);
		}

		mParams = params;
		mIgnoreChanges = false;
	}

	/**
	 private slots:
	 */

	void BilateralFilterCustomizationWidget::onRadiusChanged(int value)
	{
		if (mIgnoreChanges)
			return;

		mParams["Radius"] = Data::ExtendedVariant::toExtendedVariant(QVariant(value));
		emit paramsChanged(mParams);
	}

	void BilateralFilterCustomizationWidget::onSigmaSpatialChanged(int value)
	{
		if (mIgnoreChanges)
			return;

		mParams["SigmaSpatial"] = Data::ExtendedVariant::toExtendedVariant(QVariant(value / 100.f));
		emit paramsChanged(mParams);
	}

	void BilateralFilterCustomizationWidget::onSigmaRadiometricChanged(int value)
	{
		if (mIgnoreChanges)
			return;

		mParams["SigmaRadiometric"] = Data::ExtendedVariant::toExtendedVariant(QVariant(value / 100.f));
		emit paramsChanged(mParams);
	}

} // namespace Ui