/*!
 *\file imageviewwidget.cpp
 *\brief Image view widget implementation
 */

#include "imageviewwidget.h"

#include "../Commands/filtercommand.h"
#include "../Dispatchers/filtertriggersdispatcher.h"
#include "../Interfaces/ifilter.h"
#include "../Filters/builtinfiltersfactory.h"
#include "../Managers/imagecontextsmanager.h"
#include "../Managers/linearfiltersmanager.h"
#include "../Tools/gridrenderer.h"

#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QScrollBar>
#include <QMouseEvent>
#include <QTimer>

namespace Ui
{
	/**
 	 public:
	 */
	ImageViewWidget::ImageViewWidget( QWidget* parent /*= NULL*/ )
		: QGraphicsView(parent),
			mContextWidget(parent),
			mColorModel(Data::MODEL_SRGB),
			mColorChannel1Enabled(true),
			mColorChannel2Enabled(true),
			mColorChannel3Enabled(true),
			mIsGridVisible(false),
			mGridRenderer(new Tools::GridRenderer(Constants::DefaultGridWidthStep, Constants::DefaultGridHeightStep, Constants::DefaultGridColor))
	{
		setMinimumSize(parent->width(), parent->height());

		// Create a graphics scene and connect it
		mScene = new QGraphicsScene(0, 0, 0, 0, this);
		setScene(mScene);
		setDragMode(QGraphicsView::ScrollHandDrag);
		setViewportUpdateMode(QGraphicsView::MinimalViewportUpdate);
		setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		setInteractive(false);
		// Init scene pixmap
		mPixmapItem = mScene->addPixmap(QPixmap());
		mPixmapItem->setPos(0, 0);
		mGridPixmapItem = mScene->addPixmap(QPixmap());
		mGridPixmapItem->setVisible(false);
		// Set default zoom params
		mZoomMode	= ZoomFit;
		mZoomFactor = 1.0;
		// Assign wheel zoom factors
		mZoomFactors << Constants::MinZoom << 0.15 << 0.20 << 0.25 << 0.33 << 0.50 << 0.66 <<
			1.00 << 1.50 << 2.00 << 3.00 << 4.00 << 6.00 << Constants::MaxZoom;
		// Init stuff for dragging image with mouse
		mDragDelta = new QLine();
		mLeftMousePressed = false;

		// FIXME: hack
		QTimer::singleShot(0, this, SLOT(onZoomFit()));
	}

	ImageViewWidget::~ImageViewWidget()
	{
		delete mDragDelta;
		delete mScene;
	}

	void ImageViewWidget::mousePressEvent( QMouseEvent *e )
	{
		if (e->button() == Qt::LeftButton)
		{
			mLeftMousePressed = true;
			mDragDelta->setPoints(e->pos(), e->pos());
		}

	}

	void ImageViewWidget::mouseReleaseEvent( QMouseEvent *e )
	{
		if (e->button() == Qt::LeftButton && mLeftMousePressed)
			mLeftMousePressed = false;
	}

	void ImageViewWidget::mouseDoubleClickEvent( QMouseEvent *e )
	{
		
	}

	void ImageViewWidget::mouseMoveEvent( QMouseEvent *e )
	{
		// drag image with left mouse button to scroll
		if (mLeftMousePressed) {
			mDragDelta->setP2(e->pos());
			horizontalScrollBar()->setValue(horizontalScrollBar()->value() - mDragDelta->x2() + mDragDelta->x1());
			verticalScrollBar()->setValue(verticalScrollBar()->value() - mDragDelta->y2() + mDragDelta->y1());
			mDragDelta->setP1(e->pos());
		}
	}

	void ImageViewWidget::wheelEvent( QWheelEvent *e )
	{
		zoomStep(e->delta());
		onZoomTo(mZoomFactor);
	}

	void ImageViewWidget::onZoomFit()
	{
		Managers::ImageContextsManager	 manager;
		Interfaces::IImageContextPointer context = manager.getContext(mContextWidget);

		Q_ASSERT(context);

		const QImage&  currentImage     = context->getCurrentState()->mImage->getQImage();

		mScene->setSceneRect(0, 0, currentImage.width(), currentImage.height());

		fitInView(mScene->sceneRect(), Qt::KeepAspectRatio);
		mZoomFactor = transform().m11();

		// Clamp to (0, 1)
		if (mZoomFactor > 1.f)
			mZoomFactor = 1.f;

		imageToScene(mZoomFactor);
	}

	void ImageViewWidget::onZoomTo(float factor)
	{
		mZoomMode = ZoomNonFit;
		factor = qBound(Constants::MinZoom, factor, Constants::MaxZoom);

		imageToScene(factor);

		mZoomFactor = factor;//transform().m11();
	}

	void ImageViewWidget::imageToScene(const float factor)
	{
		resetTransform();

		Qt::TransformationMode Mode;

		if (((uint)(factor * 10000) % 10000) < 1) {
			// Nearest neighbour resize for 200%, 300%, 400% zoom
			Mode = Qt::FastTransformation;
		}
		else {
			// Bilinear resize for all others
			Mode = Qt::SmoothTransformation;
		}

		Managers::ImageContextsManager	 manager;
		Interfaces::IImageContextPointer context = manager.getContext(mContextWidget);

		const Data::SafeImagePointer image = context->getCurrentState()->mImage;
		const QImage &currentImage = image->inColorModel(mColorModel, mColorChannel1Enabled, mColorChannel2Enabled, mColorChannel3Enabled);

		float scaledW = currentImage.width() * factor, scaledH = currentImage.height() * factor;

		mScene->setSceneRect(0, 0, scaledW, scaledH);
		//mPixmapItem->setZValue(-1);
		mPixmapItem->setPixmap(QPixmap::fromImage(currentImage).scaled(scaledW, scaledH, Qt::IgnoreAspectRatio, Mode));
		mPixmapItem->setTransformationMode(Mode);

		if (mIsGridVisible)
		{
			if (scaledW < Constants::ShowGridMinImageSize || scaledH < Constants::ShowGridMinImageSize)
			{
				mGridPixmapItem->setVisible(false);
				return;
			}

			//mGridPixmapItem->setZValue(0);
			mGridPixmapItem->setPixmap(QPixmap::fromImage(mGridImage.scaled(scaledW, scaledH, Qt::IgnoreAspectRatio, Qt::SmoothTransformation)));
			mPixmapItem->setTransformationMode(Qt::SmoothTransformation);

			mGridPixmapItem->setVisible(true);
		}
		else
		{
			mGridPixmapItem->setVisible(false);
		}
	}

	void ImageViewWidget::redraw()
	{
		onZoomTo(mZoomFactor);
	}

	void ImageViewWidget::zoomStep(int direction)
	{
		int ZoomIdx = -1;

		// Zoom larger
		if (direction > 0) {
			for (int i = 0; i < mZoomFactors.size(); i++) {
				if (mZoomFactors[i] > mZoomFactor) {
					ZoomIdx = i;
					break;
				}
			}
		}
		// Zoom smaller
		else if (direction < 0) {
			for (int i = mZoomFactors.size() - 1; i >= 0; i--) {
				if (mZoomFactors[i] < mZoomFactor) {
					ZoomIdx = i;
					break;
				}
			}
		}

		if (ZoomIdx != -1) {
			mZoomFactor = mZoomFactors[ZoomIdx];
		}

		mZoomMode = ZoomNonFit;
	}

	void ImageViewWidget::onColorModelChanged(Data::ColorModel model)
	{
		if (model != mColorModel)
		{
			mColorModel = model;
			redraw();
		}
	}

	void ImageViewWidget::onColorChannelsChanged(bool ch1, bool ch2, bool ch3)
	{
		mColorChannel1Enabled = ch1;
		mColorChannel2Enabled = ch2;
		mColorChannel3Enabled = ch3;

		redraw();
	}

	void ImageViewWidget::paintEvent( QPaintEvent *e )
	{
		QGraphicsView::paintEvent(e);
	}

	void ImageViewWidget::drawForeground( QPainter *painter, const QRectF &rect )
	{
		//QGraphicsScene::drawForeground(painter, rect);
	}

	void ImageViewWidget::drawItems( QPainter *painter, int numItems, QGraphicsItem *items[], const QStyleOptionGraphicsItem options[] )
	{
		onZoomTo(mZoomFactor);
	}

	void ImageViewWidget::renderGridPixmap()
	{
		Managers::ImageContextsManager	 manager;
		Interfaces::IImageContextPointer context = manager.getContext(mContextWidget);

		const Data::SafeImagePointer image = context->getCurrentState()->mImage;
		const QImage &currentImage = image->prepareForProcessing();

		mGridImage = QImage(currentImage.width(), currentImage.height(), QImage::Format_ARGB32_Premultiplied);
		mGridImage.fill(Qt::transparent);

		QPainter painter;
		painter.begin(&mGridImage);
		mGridRenderer->draw(&painter, /*visibleRegion().boundingRect()*/ QRect(0, 0, currentImage.width(), currentImage.height()));
		painter.end();
	}

	void ImageViewWidget::onGridParamsChanged(const Data::NamedVariantMap& params)
	{
		mGridRenderer->setParameters(params);

		renderGridPixmap();

		redraw();
	}

	void ImageViewWidget::onShowGrid(bool show)
	{
		mIsGridVisible = show;

		renderGridPixmap();

		redraw();
	}

} // namespace Ui