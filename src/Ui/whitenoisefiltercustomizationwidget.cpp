/*!
 *\file whitenoisefiltercustomizationwidget.cpp
 *\brief This file contains implementation of white noise filter customization widget
 */

#include "whitenoisefiltercustomizationwidget.h"
#include "../Filters/whitenoisefilter.h"

namespace Ui
{

	/**
	 public:
	 */
	WhiteNoiseFilterCustomizationWidget::WhiteNoiseFilterCustomizationWidget(QWidget *parent /* = NULL */)
		: AbstractCustomizationWidget(parent),
		  mIgnoreChanges(false),
			mParams(Data::NamedVariantMap())
	{
		setupUi(this);
		setFixedSize(size());

		hsProbability->setRange(0, 100);
		hsMaxDisplacement->setRange(0, 255);

		connect(hsProbability,  SIGNAL( valueChanged(int) ),		 this, SLOT( onProbabilityChanged(int) ));
		connect(hsMaxDisplacement,  SIGNAL( valueChanged(int) ),		 this, SLOT( onMaxDisplacementChanged(int) ));
	}


	/**
	 public slots:
	 */
	void WhiteNoiseFilterCustomizationWidget::setParams(const Data::NamedVariantMap& params)
	{
		mIgnoreChanges = true;

		if (params.find("Probability") != params.end())
		{
			hsProbability->setValue(params["Probability"].asVariant().toFloat() * 100);
		}

		if (params.find("MaxDisplacement") != params.end())
		{
			hsMaxDisplacement->setValue(params["MaxDisplacement"].asVariant().toFloat() * 255);
		}

		mParams = params;
		mIgnoreChanges = false;
	}

	/**
	 private slots:
	 */

	void WhiteNoiseFilterCustomizationWidget::onProbabilityChanged(int value)
	{
		lProbability->setText(QVariant(value).toString());

		if (mIgnoreChanges)
			return;

		mParams["Probability"] = Data::ExtendedVariant::toExtendedVariant(QVariant(value / 100.f));
		emit paramsChanged(mParams);
	}

	void WhiteNoiseFilterCustomizationWidget::onMaxDisplacementChanged(int value)
	{
		lMaxDisplacement->setText(QVariant(value).toString());

		if (mIgnoreChanges)
			return;

		mParams["MaxDisplacement"] = Data::ExtendedVariant::toExtendedVariant(QVariant(value / 255.f));
		emit paramsChanged(mParams);
	}

} // namespace Ui