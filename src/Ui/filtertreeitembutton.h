/*!
 *\file filtertreeitembutton.h
 *\brief Filter's tree widget item button definition
 */

#ifndef UI_FILTERTREEITEMBUTTON_H
	#define UI_FILTERTREEITEMBUTTON_H

	#include <QPushButton>
	
	namespace Ui
	{
		/*!
		 *\class FilterTreeItemButton
		 *\brief This class represents button, that can be added for filter's tree widget item.
		 *\			 Extending QPushButton, it provides capabilities to retrieve, on which item button was pressed.
		 */
		class FilterTreeItemButton : public QPushButton
		{
			Q_OBJECT
		public:
			/*!
			 *\brief Constructor
			 *\param icon   Icon of the button
			 *\param text   Text of the button
			 *\param filter Name of the filter, which item button relies to
			 *\param parent	Additional button's parent widget
			 */
			explicit FilterTreeItemButton(const QIcon& icon, const QString& text, const QString& filter, QWidget *parent);

		signals:
			/*! 
			 *\brief This signal is emitted whenever button is pressed
			 *\param filter Name of the button's filter
			 */
			void pressed(const QString& filter);

		private slots:
			/*!
			 *\brief Handle superclass pressed signal
			 */
			void onSuperPressed();

		private:
			//! Filter name
			QString mFilterName;
		};
	} // namespace Ui

#endif // UI_FILTERTREEITEMBUTTON_H