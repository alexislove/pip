/*!
 *\file bilateralfiltercustomizationwidget.h
 *\brief This file contains definition of bilateral filter customization widget
 */

#ifndef UI_BILATERALFILTERCUSTOMIZATIONWIDGET_H
	#define UI_BILATERALFILTERCUSTOMIZATIONWIDGET_H

	#include "abstractcustomizationwidget.h"

	class QSlider;

	namespace Ui
	{
		/*!
		 *\class BilateralFilterCustomizationWidget
		 *\brief This class implements customization widget for bilateral filter, show over image
		 */
		class BilateralFilterCustomizationWidget : public AbstractCustomizationWidget
		{
			Q_OBJECT
		public:
			/*!
			 *\brief Constructor
			 *\param parent Parent widget
			 */
			explicit BilateralFilterCustomizationWidget(QWidget *parent = NULL);
			
		public slots:

			//! See AbstractCustomizationWidget.setParams(const Data::NamedVariantMap&)
			virtual void setParams(const Data::NamedVariantMap& params);

		private slots:
			void onRadiusChanged(int value);
			void onSigmaSpatialChanged(int value);
			void onSigmaRadiometricChanged(int value);

		private:
			void setupUi(QWidget *widget);
			QSlider *hsRadius;
			QSlider *hsSigmaSpatial;
			QSlider *hsSigmaRadiometric;

		private:
			bool mIgnoreChanges;
			Data::NamedVariantMap mParams;

			float mSigmaSpatialScale;
			float mSigmaRadimetricScale;
		};
	}
	

#endif // UI_BILATERALFILTERCUSTOMIZATIONWIDGET_H