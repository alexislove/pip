/*!
 *\file colorchannelwidget.cpp
 *\brief Color channel selector widget definition
 */

#include "colorchannelwidget.h"

namespace Ui
{
	/**
	 public:
	 */
	ColorChannelWidget::ColorChannelWidget(QWidget *parent /* = NULL */)
		: QWidget(parent)
	{
		setupUi(this);

		QStringList models;
		models << "RGB" << "YUV" << "YCbCr" << "HSV";
		lModels->insertItems(0, models);

		lModels->item(0)->setSelected(true); // RGB

		rbAll->setDown(true);
		rbCh1->setDown(false);
		rbCh2->setDown(false);
		rbCh3->setDown(false);

		connect(lModels, SIGNAL( currentRowChanged ( int ) ), this, SLOT( onModelChanged(int) ));
		connect(rbAll, SIGNAL( toggled(bool) ),		 this, SLOT( onChAllToggled(bool) ));
		connect(rbCh1, SIGNAL( toggled(bool) ),		 this, SLOT( onCh1Toggled(bool) ));
		connect(rbCh2, SIGNAL( toggled(bool) ),		 this, SLOT( onCh2Toggled(bool) ));
		connect(rbCh3, SIGNAL( toggled(bool) ),		 this, SLOT( onCh3Toggled(bool) ));
	}

	void ColorChannelWidget::onModelChanged( int row )
	{
		QString model = lModels->item(row)->text();
		Data::ColorModel colorModel = Data::MODEL_SRGB;

		if (model == "sRGB")
		{
			colorModel = Data::MODEL_SRGB;
			rbAll->setChecked(true);
		}
		else if (model == "YUV")
		{
			colorModel = Data::MODEL_YUV;
			rbCh1->setChecked(true);
		}
		else if (model == "YCbCr")
		{
			colorModel = Data::MODEL_YCBCR;
			rbCh1->setChecked(true);
		}
		else if (model == "HSV")
		{
			colorModel = Data::MODEL_HSV;
			rbCh1->setChecked(true);
		}

		emit modelChanged(colorModel);
	}

	void ColorChannelWidget::onChAllToggled( bool checked )
	{
		if (checked)
		{
			rbCh1->setChecked(false);
			rbCh2->setChecked(false);
			rbCh3->setChecked(false);
			emit channelsChanged(true, true, true);
		}
	}

	void ColorChannelWidget::onCh1Toggled( bool checked )
	{
		if (checked)
		{
			rbAll->setChecked(false);
			rbCh2->setChecked(false);
			rbCh3->setChecked(false);
			emit channelsChanged(true, false, false);
		}
	}

	void ColorChannelWidget::onCh2Toggled( bool checked )
	{
		if (checked)
		{
			rbAll->setChecked(false);
			rbCh1->setChecked(false);
			rbCh3->setChecked(false);
			emit channelsChanged(false, true, false);
		}
	}

	void ColorChannelWidget::onCh3Toggled( bool checked )
	{
		if (checked)
		{
			rbAll->setChecked(false);
			rbCh1->setChecked(false);
			rbCh2->setChecked(false);
			emit channelsChanged(false, false, true);
		}
	}

} // namespace Ui