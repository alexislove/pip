/*!
 *\file filtertreeitembuttongroup.h
 *\brief Filter's tree widget item button group definition
 */

#ifndef UI_FILTERTREEITEMBUTTONGROUP_H
	#define UI_FILTERTREEITEMBUTTONGROUP_H

	#include <QWidget>

	class QHBoxLayout;

	namespace Ui
	{
		class FilterTreeItemButton;

		/*!
		 *\class FilterTreeItemButtonGroup
		 *\brief Button group, that can be added for the particular item of filters' tree widget
		 */
		class FilterTreeItemButtonGroup : public QWidget
		{
			Q_OBJECT
		public:
			/*!
			 *\brief Get uniform width of the button group
			 *\return Screen resolution independent width
			 */
			static int getGroupUniformWidth();

			/*!
			 *\brief Get uniform height of the button group
			 *\return Screen resolution independent height
			 */
			static int getGroupUniformHeight();

		public:
			/*!
			 *\brief Constructor
			 *\param filter Name of the filter, that's associated with the item, where this group is desired to be added
			 *\param parent Parent widget
			 */
			explicit FilterTreeItemButtonGroup(const QString& filter, QWidget* parent = NULL);

			/*!
			 *\brief Hide show button
			 */
			void hideShowButton();

			/*!
			 *\brief Hide edit button
			 */
			void hideEditButton();

			/*!
			 *\brief Hide remove button
			 */
			void hideRemoveButton();

		signals:
			/*!
			 *\brief This signal is emitted, whenever show button is pressed
			 *\param filter Name of the filter
			 */
			void showButtonPressed(const QString& filter);

			/*!
			 *\brief This signal is emitted, whenever edit button is pressed
			 *\param filter Name of the filter
			 */
			void editButtonPressed(const QString& filter);

			/*!
			 *\brief This signal is emitted, whenever remove button is pressed
			 *\param filter Name of the filter
			 */
			void removeButtonPressed(const QString& filter);

		private:
			//! Show button
			FilterTreeItemButton *mShowButton;
			//! Edit button
			FilterTreeItemButton *mEditButton;
			//! Remove button
			FilterTreeItemButton *mRemoveButton;
			//! Horizontal layout for buttons
			QHBoxLayout					 *mLayout;
		};

	} // namespace Ui
	
#endif // UI_FILTERTREEITEMBUTTONGROUP_H