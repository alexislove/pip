/*!
 *\file dustnoisefiltercustomizationwidget.h
 *\brief This file contains definition of dust noise filter customization widget
 */

#ifndef UI_DUSTNOISEFILTERCUSTOMIZATIONWIDGET_H
	#define UI_DUSTNOISEFILTERCUSTOMIZATIONWIDGET_H

	#include "abstractcustomizationwidget.h"

	#include "ui_dustnoisefiltercustomizationwidgetform.h"

	namespace Ui
	{
		/*!
		 *\class DustNoiseFilterCustomizationWidget
		 *\brief This class implements customization widget for dust noise filter, show over image
		 */
		class DustNoiseFilterCustomizationWidget : public AbstractCustomizationWidget, 
																							 private DustNoiseFilterCustomizationWidgetForm
		{
			Q_OBJECT
		public:
			/*!
			 *\brief Constructor
			 *\param parent Parent widget
			 */
			explicit DustNoiseFilterCustomizationWidget(QWidget *parent = NULL);
			
		public slots:

			//! See AbstractCustomizationWidget.setParams(const Data::NamedVariantMap&)
			virtual void setParams(const Data::NamedVariantMap& params);

		private slots:

			void onProbabilityChanged(int value);
			void onMinAdditionChanged(int value);

		private:
			bool mIgnoreChanges;
			Data::NamedVariantMap mParams;
		};
	}
	

#endif // UI_DUSTNOISEFILTERCUSTOMIZATIONWIDGET_H