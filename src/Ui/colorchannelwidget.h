/*!
 *\file colorchannelwidget.h
 *\brief Color channel selector widget definition
 */

#ifndef UI_COLORCHANNELWIDGET_H
	#define UI_COLORCHANNELWIDGET_H

	#include "ui_colorchannelwidgetform.h"

	#include "..\Data\colormodelconversions.h"

	namespace Ui {

	class ColorChannelWidget : public QWidget,
														 public ColorChannelWidgetForm
	{
		Q_OBJECT
	public:
		/*!
			*\brief Constructor
			*\param parent Parent widget
			*/
		explicit ColorChannelWidget(QWidget *parent = NULL);

	private slots:
		void onModelChanged(int);
		void onChAllToggled(bool);
		void onCh1Toggled(bool);
		void onCh2Toggled(bool);
		void onCh3Toggled(bool);

	signals:
		void modelChanged(Data::ColorModel model);
		void channelsChanged(bool ch1, bool ch2, bool ch3);
	};
	} // namespace Ui

#endif // UI_COLORCHANNELWIDGET_H