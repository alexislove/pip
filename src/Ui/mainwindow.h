#ifndef UI_MAINWINDOW_H
	#define UI_MAINWINDOW_H

	#include <QMainWindow>
	#include <QScopedPointer>

	#include "../data/namedvariantmap.h"

	class QCloseEvent;

	namespace Ui
	{
		/*!
		 *\class MainWindow
		 *\brief This class represents application's main window that manages all corresponding controls and actions
		 */
		class MainWindow : public QMainWindow
		{
			Q_OBJECT

			//! This struct incapsulates controls and actions, that are present in menu
			struct Appearance;
		public:
			/*!
			 *\brief Constructor
			 *\param parent Parent widget
			 *\param flags  Desired window flags
			 */
			MainWindow(QWidget *parent = 0, Qt::WindowFlags flags = 0);

			/*!
			 *\brief Destructor
			 */
			~MainWindow();

			/*!
			 *\brief Setup main window appearance and behavior
			 */
			void buildAppearance();

    signals:
      /*!
       *\brief This signal is emmitted, when new filter has been added
       *\param meAdded Instance of the main window, which added new filter
       */
      void newLinearFilterAdded(QWidget* meAdded);

			/*!
			 *\brief This signal is emitted, when grid is desired to be shown
			 */
			void showGrid();

			/*!
			 *\brief This signal is emitted, when grid is desired to be hidden
			 */
			void hideGrid();

			/*!
			 *\brief Request visible state of current tab's grid
			 */
			void currentGridVisibleStateRequest();

			/*!
			 *\brief This signal is emitted, when comparison to golden image is desired
			 */
			void compareCurrentToGolden();

			/*!
			 *\brief This signal is emitted, when specific channel show is desired
			 */
			void showChannel();

		protected:
			/*!
			 *\brief Handle window close event
			 *\param e Event descriptor
			 */
			virtual void closeEvent(QCloseEvent* e);
			
		private:
			// Please, call next methods in the way, they appear here to avoid undefined behavior
			// It is clearly understandable, because they're obviously dependent

			//** Obviously dependent methods start **//
			/*!
			 *\brief Create all actions
			 */
			void createActions();

			/*!
			 *\brief Create all toolbars
			 */
			void createToolBars();

			/*!
			 *\brief Create menu bar and all menus
			 */
			void createMenus();

			/*!
			 *\brief Create status bar
			 */
			void createStatusBar();

			/*!
			 *\brief Create different components of the UI
			 */
			void createComponents();

			/*!
			 *\brief Setup all inter-objects connections here
			 */
			void setupConnections();

			/*!
			 *\brief Enable/disable actions
			 */
			void switchStartupConfiguration();
			//** Obviously dependent methods end **//

			/*!
			 *\brief Update undo/redo actions for current tab
			 */
			void updateUndoRedoActionsState();

			/*!
			 *\brief Update save actions for current tab
			 */
			void updateSaveActionsState();

			/*!
			 *\brief Update filters actions state
			 */
			void updateFiltersActionsState();

			/*!
			 *\brief Update tools actions state
			 */
			void updateToolsActionsState();

			/*!
			 *\brief Update history combo box
			 */
			void updateHistoryBox();

		private slots:
			/*!
			 *\brief Handle open file action
			 */
			void onOpenFileAction();

			/*!
			 *\brief Handle save file action
			 */
			void onSaveFileAction();

			/*!
			 *\brief Handle save file as action
			 */
			void onSaveFileAsAction();

			/*!
			 *\brief Handle quit action
			 */
			void onQuitAction();

			/*!
			 *\brief Handle undo action
			 */
			void onUndoAction();

			/*!
			 *\brief Handle redo action
			 */
			void onRedoAction();

			/*!
			 *\brief Handle show grid action
			 */
			void onShowGridAction();

			/*!
			 *\brief Handle golden image comparison action
			 */
			void onGoldenImageCompareAction();

			/*!
			 *\brief Handle show channel action
			 */
			void onShowChannelAction();

			/*!
			 *\brief Handle about action
			 */
			void onAboutAction();

			/*!
			 *\brief Handle help action
			 */
			void onHelpAction();

			/*!
			 *\brief Handle add linear filter action
			 */
			void onAddLinearFilterAction();

			/*!
			 *\brief Handle viewers tab change
			 *\param index New tab index
			 */
			void onCurrentViewerChanged(int index);

			/*!
			 *\brief Handle tab close
			 */
			void onViewerClosed();

			/*!
			 *\brief Handle applied commands notifications
			 */
			void onProcessorCommandApplied();

      /*!
       *\brief Handle notifications signals about linear filters addition
       *\param whoUpdated Widget instance, which added filter
       */
      void onLinearFiltersSetUpdated(QWidget* whoUpdated);

      /*!
       *\brief Refresh linear filters actions
       */
      void onRefreshLinearFilters();

			/*!
			 *\brief Handle current history index change
			 *\param index New index
			 */
			void onCurrentHistoryItemChanged(int index);

			/*!
			 *\brief Handle grid visible state response
			 *\param visible Actual state
			 */
			void onCurrentGridVisibleStateResponse(bool visible);

		private:
			//! Window UI pointer
			QScopedPointer< Appearance > mUi;
		}; // class MainWindow
	} // namespace Ui

#endif // UI_MAINWINDOW_H
