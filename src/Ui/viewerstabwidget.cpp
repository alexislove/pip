/*!
 *\file viewerstabwidget.h
 *\brief Tab widget for image viewers implementation
 */

#include <QDir>
#include <QFileInfo>
#include <QMessageBox>

#include "../dispatchers/filtertriggersdispatcher.h"
#include "../interfaces/iimagecontext.h"
#include "../managers/imagecontextsmanager.h"

#include "imageprocessorwidget.h"

#include "viewerstabwidget.h"

using namespace Dispatchers;
using namespace Interfaces;
using namespace Managers;

namespace Ui
{
	/*!
	 *\brief Get context of tab with given index
	 *\param tab   Tab widget instance
	 *\param index Index of the tab
	 *\return Tab context's pointer
	 */
	IImageContextPointer GGetTabContext(const QTabWidget *tab, int index)
	{
		QWidget *currentWidget = tab->widget(index);									
		ImageContextsManager manager;																			
		IImageContextPointer context = manager.getContext(currentWidget);
		return context;
	}

	/*!
	 *\brief Get context of current tab 
	 *\return Current tab context's pointer
	 */
	IImageContextPointer GGetCurrentTabContext(const QTabWidget* tab)
	{
		return GGetTabContext(tab, tab->currentIndex());
	}

	/**
	 public:
	 */
	ViewersTabWidget::ViewersTabWidget(QWidget *parent /* = NULL */)
		: QTabWidget(parent),
			mPreviousTab(-1)
	{
		buildAppearance();
		setupConnections();
	}

	ViewersTabWidget::~ViewersTabWidget()
	{

	}

	QStringList ViewersTabWidget::getCurrentImageHistory() const
	{
		if (count() == 0)
			return QStringList();

		IImageContextPointer context = GGetCurrentTabContext(this);
		Q_ASSERT(context);
		return context->getHistory();
	}

	int ViewersTabWidget::getCurrentOperationIndex() const
	{
		if (count() == 0)
			return -1;

		IImageContextPointer context = GGetCurrentTabContext(this);
		Q_ASSERT(context);
		return context->getCurrentCommandIndex();
	}

	void ViewersTabWidget::setCurrentOperationIndex(int index)
	{
		if (count() == 0)
			return;

		IImageContextPointer context = GGetCurrentTabContext(this);
		Q_ASSERT(context);
		return context->setCommandIndex(index);
	}

	bool ViewersTabWidget::isAnyDirty()
	{
		for (int idx = 0, tabsCount = count(); idx < tabsCount; ++idx)
		{
			IImageContextPointer context = GGetTabContext(this, idx);
			if (context->isDirty())
			{
				return true;
			}
		}

		return false;
	}

	void ViewersTabWidget::saveAll(bool *cancelled)
	{
		for (int idx = 0, tabsCount = count(); idx < tabsCount; ++idx)
		{
			bool cancel = false;
			IImageContextPointer context = GGetTabContext(this, idx);
			if (promptToSaveFile(QFileInfo(context->getCurrentImageFile()).fileName(), &cancel))
			{
				
				if (!context->save(context->getCurrentImageFile()))
				{
					QMessageBox::critical(this, 
															  tr("Error!"),
																tr("Failed to save file: ") + context->getCurrentImageFile() + tr(". Please try again!"));
					cancel = true;
				}
				if (cancel)
				{
					*cancelled = true;
				}
			}
		}
	}

	bool ViewersTabWidget::canCurrentRedo()
	{
		if (count() == 0)
		{
			return false;
		}
		IImageContextPointer context = GGetCurrentTabContext(this);
		Q_ASSERT(context);
		return context->canRedo();
	}

	bool ViewersTabWidget::canCurrentUndo()
	{
		if (count() == 0)
		{
			return false;
		}
		IImageContextPointer context = GGetCurrentTabContext(this);
		Q_ASSERT(context);
		return context->canUndo();
	}

	bool ViewersTabWidget::isCurrentDirty()
	{
		if (count() == 0)
		{
			return false;
		}
		IImageContextPointer context = GGetCurrentTabContext(this);
		Q_ASSERT(context);
		return context->isDirty();
	}

	/**
	 public slots:
	 */

	void ViewersTabWidget::openFile(const QString& fileName)
	{
		// Create new widget and open file there
		ImageProcessorWidget* widget = new ImageProcessorWidget(this);

		// Create context for widget
		ImageContextsManager manager;
		IImageContextPointer context = manager.newContext(widget);

		if (! context->load(fileName))
		{
			QMessageBox::critical(this, 
														tr("Error!"), 
														tr("Failed to load image file: ") + fileName + tr(". Please try again!"));
			manager.deleteContext(widget);
			widget->deleteLater();
		}

		connect(widget, SIGNAL( commandApplied() ),													this,   SIGNAL( processorCommandApplied() ));
    connect(widget, SIGNAL( newLinearFilterAdded() ),										this,   SLOT( onCurrentProcessorAddedLinearFilter() ));
		connect(widget, SIGNAL( notifyGridVisibleState(bool) ),							this,   SIGNAL( currentGridVisibleStateResponse(bool) ));
    connect(this,   SIGNAL( someoneAddedLinearFilter(QWidget*) ),				widget, SLOT( onLinearFiltersSetUpdated(QWidget*) ));
		connect(this,   SIGNAL( commandApplied() ),													widget, SLOT( onUndoRedoCommand() ));
		connect(this,   SIGNAL( showGrid(QWidget*) ),												widget, SLOT( onShowGrid(QWidget*) ));
		connect(this,   SIGNAL( hideGrid(QWidget*) ),												widget, SLOT( onHideGrid(QWidget*) ));
		connect(this,   SIGNAL( hideCustomizers(QWidget*) ),								widget, SLOT( onHideCustomizationWidgets(QWidget*) ));
		connect(this,   SIGNAL( restoreCustomizers(QWidget*) ),							widget, SLOT( onRestoreCustomizationWidgets(QWidget*) ));
		connect(this,   SIGNAL( viewerGridVisibleStateRequest(QWidget*) ),  widget, SLOT( onGridVisibleStateRequest(QWidget*) ));
		connect(this,   SIGNAL( compareToGolden(QWidget*) ),								widget, SLOT( onCompareCurrentToGolden(QWidget*) ));
		connect(this,   SIGNAL( showChannel(QWidget*) ),										widget, SLOT( onShowChannel(QWidget*) ));

		mFilterTriggerListeners.push_back(widget);
		
		// Add widget, caption - only file name
		addTab(widget, QFileInfo(fileName).fileName()); 

		// Set current tab as new one
		setCurrentIndex(count() - 1);

		
	}
		
	void ViewersTabWidget::saveCurrent()
	{
		// Get current widget's context and ask it to save the image
		IImageContextPointer context = GGetCurrentTabContext(this);

		Q_ASSERT(context);

		if (!context->save(context->getCurrentImageFile())) 
		{
			QMessageBox::critical(this, 
														tr("Error!"),
														tr("Failed to save file: ") + context->getCurrentImageFile() + tr(". Please try again!"));
			return;
		}
	}

	void ViewersTabWidget::saveAs(const QString& fileName)
	{
		// Get current widget's context and ask it to save the image
		IImageContextPointer context = GGetCurrentTabContext(this);

		Q_ASSERT(context);

		if (!context->save(fileName)) 
		{
			QMessageBox::critical(this, 
														tr("Error!"),
														tr("Failed to save file: ") + context->getCurrentImageFile() + tr(". Please try again!"));
			return;
		}
	}

	void ViewersTabWidget::undo()
	{
		IImageContextPointer context = GGetCurrentTabContext(this);
		Q_ASSERT(context);
		context->undo();
		// Update current widget
		currentWidget()->update();
		emit commandApplied();
	}

	void ViewersTabWidget::redo()
	{
		IImageContextPointer context = GGetCurrentTabContext(this);
		Q_ASSERT(context);
		context->redo();
		// Update current widget
		currentWidget()->update();
		emit commandApplied();
	}

	void ViewersTabWidget::updateCurrent()
	{
		if (count() == 0)
			return;
		currentWidget()->update(currentWidget()->rect());
	}

	void ViewersTabWidget::onShowGridOnCurrent()
	{
		if (count() == 0)
		{
			return;
		}
		emit showGrid(currentWidget());
	}

	void ViewersTabWidget::onHideGridOnCurrent()
	{
		if (count() == 0)
		{
			return;
		}
		emit hideGrid(currentWidget());
	}

	void ViewersTabWidget::onCurrentGridVisibleRequest()
	{
		if (count() == 0)
		{
			return;
		}
		emit viewerGridVisibleStateRequest(currentWidget());
	}

	void ViewersTabWidget::onCurrentCompareToGolden()
	{
		if (count() == 0)
		{
			return;
		}
		emit compareToGolden(currentWidget());
	}

	void ViewersTabWidget::onShowChannel()
	{
		if (count() == 0)
		{
			return;
		}
		emit showChannel(currentWidget());
	}

	/**
	 private:
	 */
	void ViewersTabWidget::buildAppearance()
	{
		setTabsClosable(true);
	}

	void ViewersTabWidget::setupConnections()
	{
		connect(this, SIGNAL( currentChanged(int) ),		this, SLOT( onTabChanged(int) ));
		connect(this, SIGNAL( tabCloseRequested(int) ), this, SLOT( onTabCloseRequested(int) ));
	}

	bool ViewersTabWidget::promptToSaveFile(const QString& fileName, bool *cancelled)
	{
		// Get current widget's context and ask it to save the image
		int button = QMessageBox::information(this, 
																					tr("Image ") + fileName + tr(" changed"),
																					tr("Would you like to save changes?"),
																					tr("Yes"),
																					tr("No"),
																					tr("Cancel"),
																					0,	  // Default button - Yes
																					2); // Escape button - Cancel
		*cancelled = (button == 2);
		return (button == 0); // User decided to save
	}

	/**
	 private slots:
	 */
	void ViewersTabWidget::onTabCloseRequested(int index)
	{
		IImageContextPointer context = GGetTabContext(this, index);
		
		if (context->isDirty())
		{
			bool cancel = false;
			const QString& contextFile = context->getCurrentImageFile();
			if (promptToSaveFile(QFileInfo(contextFile).fileName(), &cancel))
			{
				if (!context->save(contextFile))
				{
					QMessageBox::critical(this, 
														tr("Error!"),
														tr("Failed to save file: ") + context->getCurrentImageFile() + tr(". Please try again!"));
				}
			}
			if (cancel)
			{
				return;
			}
		}

		ImageContextsManager manager;
		manager.deleteContext(widget(index));

		emit hideCustomizers(widget(index));

		// Remove tab
		removeTab(index);
		mFilterTriggerListeners.remove(index);

		if (count() == 0)
		{
			FilterTriggersDispatcher::getInstance().setCurrentListener(NULL); // Remove listeners at all
		}
		else
		{
			FilterTriggersDispatcher::getInstance().setCurrentListener(mFilterTriggerListeners[currentIndex()]);
		}

		emit tabClosed();
	}

	void ViewersTabWidget::onTabChanged(int index)
	{
		if (mPreviousTab != -1)
		{
			emit hideCustomizers(widget(mPreviousTab));
		}
		mPreviousTab = index;
		// Update listener in dispatcher
		Q_ASSERT(index < mFilterTriggerListeners.size());
		if (index >= 0) // If all tabs closed can be negative
		{
			FilterTriggersDispatcher::getInstance().setCurrentListener(mFilterTriggerListeners[index]);
			emit restoreCustomizers(widget(index));
		}
	}

  void ViewersTabWidget::onCurrentProcessorAddedLinearFilter()
  {
    emit someoneAddedLinearFilter(widget(currentIndex()));
  }
} // namespace Ui
