/*!
 *\file imageprocessorwidget.h
 *\brief Image processor widget implementation
 */

#ifndef UI_IMAGEPROCESSORWIDGET_H
	#define UI_IMAGEPROCESSORWIDGET_H

	#include <QSharedPointer>
	#include <QWidget>

	#include "../Data/namedvariantmap.h"
	#include "../Interfaces/ifiltertriggerlistener.h"
	#include "../Interfaces/ifilter.h"

	class QPaintEvent;
	class QHBoxLayout;
	class QVBoxLayout;
	class QTreeWidget;
	class QFrame;
	class QDockWidget;

	namespace Tools
	{
		class GridRenderer;
	} // namespace Tools


	namespace Ui
	{
		class ImageViewWidget;
		class ColorChannelWidget;
		class AbstractCustomizationWidget;
		class GridCustomizationWidget;
		class MedianFilterCustomizationWidget;
		class WhiteNoiseFilterCustomizationWidget;
		class DustNoiseFilterCustomizationWidget;
		class BilateralFilterCustomizationWidget;
		class WienerFilterCustomizationWidget;
		//! Warning: do not edit this line!!! $WidgetForwardDecl$

		namespace Constants
		{
			static const int MaximumCustomizersIntersectionArea = 100 * 100;
		} // namespace Constants

		/*!
		 *\class ImageProcessorWidget
		 *\brief This class represents an image processor widget, that contains image view and list of filters to be applied
		 */
		class ImageProcessorWidget : public QWidget, 
																 public Interfaces::IFilterTriggerListener
		{
			Q_OBJECT

			//! Tree widget items for morphological filters
			struct MorphFilterItemsSet
			{
				//! Inversion filter item
				QTreeWidgetItem* mInversion;

				//! Dilation filter item
				QTreeWidgetItem* mDilation;

				//! Erosion filter item
				QTreeWidgetItem* mErosion;

				//! Top-hat transform filter item
				QTreeWidgetItem* mTopHatTransform;

				//! Bottom-hat transform filter item
				QTreeWidgetItem* mBottomHatTransform;

        //! Median filter item
				QTreeWidgetItem* mMedian;

				//! White noise filter item
				QTreeWidgetItem* mWhiteNoise;

				//! Dust noise filter item
				QTreeWidgetItem* mDustNoise;

				//! Bilateral filter item
				QTreeWidgetItem* mBilateral;

				//! Wiener filter item
				QTreeWidgetItem* mWiener;

				//! Warning: do not edit this line!!! $TreeWidgetItem$
			};

		public:
			/*!
			 *\brief Constructor
			 *\param parent Parent widget instance
			 */
			explicit ImageProcessorWidget(QWidget* parent = NULL);

			/*!
			 *\brief Destructor
			 */
			virtual ~ImageProcessorWidget();

    public slots:
      /*!
       *\brief Handle linear filter set updates
       *\param whoUpdated Instance of widget, that updated filters
       */
      void onLinearFiltersSetUpdated(QWidget *whoUpdated);

			/*!
			 *\brief Handle undo/redo applications
			 */
			void onUndoRedoCommand();

			// Next group of methods accepts QWidget *target arg to filter signals, received
			// This gives ability to have unique grids per each viewer

			/*!
			 *\brief Show grid over the image with given width and height step
			 *\param target Target widget, that signal was sent to
			 *\param dw			Width step
			 *\param dh			Height step
			 *\param color  Grid color
			 */
			void onShowGrid(QWidget *target);

			/*!
			 *\brief Hide visible grid
			 *\param target Target widget, that signal was sent to
			 */
			void onHideGrid(QWidget *target);

			/*!
			 *\brief Hide grid customization widget
			 *\param target Target widget, that signal was sent to
			 */
			void onHideCustomizationWidgets(QWidget *target);

			/*!
			 *\brief Restore visible state of the customization widget
			 *\param target Target widget, that signal was sent to
			 */
			void onRestoreCustomizationWidgets(QWidget *target);

			/*!
			 *\brief Handle grid params change
			 *\param params New params
			 */
			//void onGridParamsChanged(const Data::NamedVariantMap& params);

			/*!
			 *\brief Handle median filter params change
			 *\param params New params
			 */
			void onMedianFilterParamsChanged(const Data::NamedVariantMap& params);

			/*!
			 *\brief Handle white noise filter params change
			 *\param params New params
			 */
			void onWhiteNoiseFilterParamsChanged(const Data::NamedVariantMap& params);

			/*!
			 *\brief Handle dust noise filter params change
			 *\param params New params
			 */
			void onDustNoiseFilterParamsChanged(const Data::NamedVariantMap& params);

			/*!
			 *\brief Handle Bilateral filter params change
			 *\param params New params
			 */
			void onBilateralFilterParamsChanged(const Data::NamedVariantMap& params);

			/*!
			 *\brief Handle Wiener filter params change
			 *\param params New params
			 */
			void onWienerFilterParamsChanged(const Data::NamedVariantMap& params);

			//! Warning: do not edit this line!!! $OnParamsChanged$

			/*!
			 *\brief Handle application request about current grid visibility state
			 *\param target Target widget, that signal was sent to
			 */
			void onGridVisibleStateRequest(QWidget *target);

			/*!
			 *\brief Handle request to compare image with golden one
			 *\param target Target widget, that is desired to find difference
			 */
			void onCompareCurrentToGolden(QWidget *target);

			/*!
			 *\brief Handle request to show specific color channel
			 *\param target Target widget
			 */
			void onShowChannel(QWidget *target);

		signals:
			/*!
			 *\brief Emitted, whenever command is applied to context
			 */
			void commandApplied();

      /*!
       *\brief Emitted, whenever new linear filter was added
       */
      void newLinearFilterAdded();

			/*!
			 *\brief Emitted, whenever application needs current state of grid visibility
			 *\param visible Actual visible state of the grid
			 */
			void notifyGridVisibleState(bool visible);

		protected:
			/*!
			 *\brief Filter events on child objects
			 *\param obj Object, received event
			 *\param e	 Event descriptor
			 *\return True, if event shouldn't be passed to the object
			 */
			virtual bool eventFilter(QObject* obj, QEvent* e);
			
			virtual void paintEvent(QPaintEvent *);

		private:
			/*!
			 *\brief Setup widget behavior
			 */
			void setupUi();

			void retranslateUi();

			/*!
			 *\brief Load filters
			 */
			void loadFilters();

      /*!
       *\brief Load morphological filters
       */
      void loadMorphologicalFilters();

      /*!
       *\brief Load linear filters
       */
      void loadLinearFilters();

			/*!
       *\brief Load other filters
       */
      void loadOtherFilters();

			/*!
       *\brief Hide old customizer, apply filter, show new if exists
       */
			void applyFilterAndShowCustomizer(const QString& name);

			/*!
			 *\brief See IFilterTriggerListener.onFilterItemClicked(QTreeWidgetItem*)
			 */
			virtual void onFilterItemClicked(QTreeWidgetItem* item);

			/*!
			 *\brief See IFilterTriggerListener.onFilterActionTriggered(QAction*)
			 */
			virtual void onFilterActionTriggered(QAction* action);

			/*!
			 *\brief Get filter pointer with given name, if such will be found
			 *\param params Filter name
			 *\return pointer to filter
			 */
			Interfaces::IFilterPointer getFilterPointer(const QString& name, const Data::NamedVariantMap& params = Data::NamedVariantMap());

			/*!
			 *\brief Apply specified filter
			 *\param filter pointer to filter
			 */
			void ImageProcessorWidget::applyFilter(Interfaces::IFilterPointer filter);

			/*!
			 *\brief Apply filter with given name, if such will be found
			 *\param name Filter name
			 *\param params parameters for filter
			 */
			void applyFilter(const QString& name, const Data::NamedVariantMap& params = Data::NamedVariantMap());

			/*!
			 *\brief Show custom filter editor dialog
			 */
			void createCustomFilter();

			void createImageView(QWidget *parent);
			void createFiltersTreeView(QWidget *parent);

		private slots:
			/*!
			 *\brief Handle filter item click
			 *\param item   Clicked item
			 *\param column Column, where the item was clicked
			 */
			void onFilterWidgetItemClicked(QTreeWidgetItem* item, int column);

			/*!
			 *\brief Handle filter item show button press
			 *\param filter Name of the filter associated with the item
			 */
			void onFilterItemShowButtonPressed(const QString& filter);

			/*!
			 *\brief Handle filter item edit button press
			 *\param filter Name of the filter associated with the item
			 */
			void onFilterItemEditButtonPressed(const QString& filter);

			/*!
			 *\brief Handle filter item remove button press
			 *\param filter Name of the filter associated with the item
			 */
			void onFilterItemRemoveButtonPressed(const QString& filter);

		private:
			//! Morphological filters tree widget items
			MorphFilterItemsSet								  mBuiltinItems;
      //! Morph. filters top level item
      QTreeWidgetItem										 *mMorphologicalTop;
      //! Linear filters top level item
      QTreeWidgetItem										 *mLinearTop;
			//! Other filters top level item
			QTreeWidgetItem										 *mOtherTop;
			//! Add custom filter tree widget item
			QTreeWidgetItem										 *mAddCustomFilterItem;

			//! Grid customization widget instance
			GridCustomizationWidget						 *mGridCustomizer;
			//! Visibility state of grid customizer to restore
			bool																mGridCustomizerVisible;

			//! Median filter customization widget instance
			MedianFilterCustomizationWidget		 *mMedianFilterCustomizer;
			//! White noise filter customization widget instance
			WhiteNoiseFilterCustomizationWidget *mWhiteNoiseFilterCustomizer;
			//! Dust noise filter customization widget instance
			DustNoiseFilterCustomizationWidget *mDustNoiseFilterCustomizer;
			//! Bilateral filter customization widget instance
			BilateralFilterCustomizationWidget *mBilateralFilterCustomizer;
			//! Wiener filter customization widget instance
			WienerFilterCustomizationWidget *mWienerFilterCustomizer;
			//! Warning: do not edit this line!!! $CustomizationWidgetPtr$

			//! Current filter customization widget instance
			QWidget														 *mCurrentFilterCustomizer;
			//! Visibility state of current filter customizer to restore
			bool																mCurrentFilterCustomizerVisible;

		private:
			// Ui elements
			QHBoxLayout												 *horizontalLayout;
			QVBoxLayout												 *verticalLayout;
			
			QTreeWidget												 *twFilters;
			ImageViewWidget										 *wImageView;
	
			QFrame *frame;
			QDockWidget *dwFilters;
			QDockWidget *dwCustomizer;
			QDockWidget *dwGridCustomizer;
			QDockWidget *dwColorChannel;

			ColorChannelWidget *mColorChannelWidget;
		};

	} // namespace Ui

#endif // UI_IMAGEPROCESSORWIDGET_H