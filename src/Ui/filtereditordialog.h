/*!
 *\file filtereditordialog.h
 *\brief Filter editor dialog definition
 */

#ifndef UI_FILTEREDITORDIALOG_H
	#define UI_FILTEREDITORDIALOG_H

	#include <QDialog>
	#include <QFlags>

	#include "../Filters/linearfilterdescriptor.h"

	#include "ui_filtereditorform.h"

	namespace Ui
	{
		class FilterEditorDialog : public QDialog, private FilterEditorForm
		{
			Q_OBJECT
		public:
			//! Execution mode flags enumeration
			enum Mode
			{
				//! Create new filter mode
				MODE_CREATE_NEW		 = 0,
				//! Edit existing filter mode
				MODE_EDIT_EXISTING = 1,
				//! Read only (show mode)
				MODE_READ_ONLY		 = 2,
			};

			// Enable operations on mode
			typedef QFlags<Mode> ModeFlags;

		public:
			/*!
			 *\brief Constructor
			 *\param parent   Parent widget
			 *\param readOnly Can user edit values or not
			 */
			explicit FilterEditorDialog(QWidget *parent = NULL);

			/*!
			 *\brief Constructor from existing descriptor
			 *\param desc   Linear filter descriptor instance
			 *\param mode   Dialog execution mode
			 *\param parent Parent widget
			 */
			explicit FilterEditorDialog(const Filters::LinearFilterDescriptor& desc, ModeFlags mode, QWidget *parent = NULL);

			/*!
			 *\brief Execute dialog
			 *\param out Created descriptor pointer
			 *\return Was dialog accepted or rejected
			 */
			bool execute(Filters::LinearFilterDescriptor *out = NULL);

			/*!
			 *\brief Get state of the saving filter on disc
			 *\return Current state
			 */
			bool shouldSaveOnDisk() const;

		private:
			/*!
			 *\brief Create descriptor from the filled line edits
			 *\param ok Success state
			 *\return Created descriptor
			 */
			Filters::LinearFilterDescriptor createDescriptor();

			/*!
			 *\brief Create 3x3 descriptor from the filled line edits
			 *\param ok Success state
			 *\return Created descriptor
			 */
			Filters::LinearFilterDescriptor create3x3Descriptor();

			/*!
			 *\brief Create 3x3 descriptor from the filled line edits
			 *\param ok Success state
			 *\return Created descriptor
			 */
			Filters::LinearFilterDescriptor create5x5Descriptor();

			/*!
			 *\brief Set all edit items read-only
			 */
			void setReadOnly();
			 
			/*!
			 *\brief Fill widget from descriptor
			 *\param desc Descriptor instance
			 */
			void fillFromDescriptor(const Filters::LinearFilterDescriptor& desc);

			/*!
			 *\brief Fill widget from 3x3 matrix descriptor
			 *\param desc Descriptor instance
			 */
			void fillMatrixFrom3x3Descriptor(const Filters::LinearFilterDescriptor& desc);

			/*!
			 *\brief Fill widget from 5x5 matrix descriptor
			 *\param desc Descriptor instance
			 */
			void fillMatrixFrom5x5Descriptor(const Filters::LinearFilterDescriptor& desc);

		protected:
			/*! 
			 *\brief Close standard exec and use custom one
			 */
			virtual int exec()
			{
				return QDialog::exec();
			}

		private slots:

			/*!
			 *\brief Handle add button press
			 */
			void onAddButtonPressed();

			/*!
			 *\brief Handle cancel button press
			 */
			void onCancelButtonPressed();

		private:
			/*!
			 *\brief Check, that all fields are well-formed
			 *\return Check state
			 */
			bool isWellFormed() const;

		private:
			//! Current mode
			ModeFlags mCurrentMode;
		};
	} // namespace Ui

#endif // UI_FILTEREDITORDIALOG_H