/*!
 *\file imageprocessorwidget.cpp
 *\brief Image processor widget implementation
 */

#include <QAction>
#include <QFileInfo>
#include <QItemDelegate>
#include <QMessageBox>
#include <QPaintEvent>
#include <QPainter>
#include <QPushButton>
#include <QResizeEvent>
#include <QScrollBar>
#include <QTreeWidgetItem>
#include <QDockWidget>
#include <QSplitter>

#include "imageprocessorwidget.h"

#include "../Commands/filtercommand.h"
#include "../Dispatchers/filtertriggersdispatcher.h"
#include "../Interfaces/ifilter.h"
#include "../Filters/builtinfiltersfactory.h"
#include "../Managers/imagecontextsmanager.h"
#include "../Managers/linearfiltersmanager.h"

#include "imageviewwidget.h"
#include "colorchannelwidget.h"
#include "filtereditordialog.h"
#include "filtertreeitembuttongroup.h"
#include "goldenimagecomparatorwidget.h"
#include "gridcustomizationwidget.h"
#include "medianfiltercustomizationwidget.h"
#include "whitenoisefiltercustomizationwidget.h"
#include "dustnoisefiltercustomizationwidget.h"
#include "Bilateralfiltercustomizationwidget.h"
#include "wienerfiltercustomizationwidget.h"
//! Warning: do not edit this line!!! $CustomizationWidgetInclude$


#include "../Filters/medianfilter.h"
#include "../Filters/whitenoisefilter.h"
#include "../Filters/dustnoisefilter.h"
#include "../Filters/Bilateralfilter.h"
#include "../Filters/wienerfilter.h"
//! Warning: do not edit this line!!! $CustomizableFilterInclude$


using namespace Commands;
using namespace Dispatchers;
using namespace Filters;
using namespace Interfaces;
using namespace Managers;
using namespace Tools;

namespace Ui
{
	class FilterItemDelegate : public QItemDelegate
	{
	public:
		virtual QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
		{
			return QSize(50, FilterTreeItemButtonGroup::getGroupUniformHeight());
		}
	};

	/**
	 public:
	 */
	ImageProcessorWidget::ImageProcessorWidget(QWidget* parent /* = NULL */)
		: QWidget(parent),
			mGridCustomizerVisible(false),
			mMedianFilterCustomizer(new MedianFilterCustomizationWidget(this)),
			mWhiteNoiseFilterCustomizer(new WhiteNoiseFilterCustomizationWidget(this)),
			mDustNoiseFilterCustomizer(new DustNoiseFilterCustomizationWidget(this)),
			mBilateralFilterCustomizer(new BilateralFilterCustomizationWidget(this)),
			mWienerFilterCustomizer(new WienerFilterCustomizationWidget(this)),
			//! Warning: do not edit this line!!! $CustomizerCreateInstance$
			mCurrentFilterCustomizer(nullptr),
			mCurrentFilterCustomizerVisible(false)
	{
		setupUi();

		// Get default filter parameters for customization widgets
		MedianFilter medianFilter;
		mMedianFilterCustomizer->setParams(medianFilter.getParameters());

		WhiteNoiseFilter whiteNoiseFilter;
		mWhiteNoiseFilterCustomizer->setParams(whiteNoiseFilter.getParameters());

		DustNoiseFilter dustNoiseFilter;
		mDustNoiseFilterCustomizer->setParams(dustNoiseFilter.getParameters());

		BilateralFilter BilateralFilter;
		mBilateralFilterCustomizer->setParams(BilateralFilter.getParameters());

		WienerFilter wienerFilter;
		mWienerFilterCustomizer->setParams(wienerFilter.getParameters());

		//! Warning: do not edit this line!!! $CustomizerSetDefaultParams$
	}

	ImageProcessorWidget::~ImageProcessorWidget()
	{
	}

	/**
	 public slots:
	 */
	void ImageProcessorWidget::onLinearFiltersSetUpdated(QWidget *whoUpdated)
	{
		// This has already updated filters
		if (whoUpdated == this)
		{
			return;
		}

		loadLinearFilters();
	}

	void ImageProcessorWidget::onUndoRedoCommand()
	{
		wImageView->redraw();

		emit commandApplied();
	}

	void ImageProcessorWidget::onShowGrid(QWidget *target)
	{
		if (this != target) 
		{
			return;
		}

		wImageView->onShowGrid(true);

		// FIXME: see setupUi()
		dwGridCustomizer->setWidget(mGridCustomizer);
		dwGridCustomizer->setVisible(true);

		update();
	}

	void ImageProcessorWidget::onHideGrid(QWidget *target)
	{
		if (this != target)
		{
			return;
		}

		wImageView->onShowGrid(false);
		dwGridCustomizer->setVisible(false);

		update();
	}

	void ImageProcessorWidget::onMedianFilterParamsChanged(const Data::NamedVariantMap& params)
	{
		// Make undo action
		Managers::ImageContextsManager manager;
		Interfaces::IImageContextPointer context = manager.getContext(this);
		Q_ASSERT(context);
		context->undo();

		// Apply filter with new params
		applyFilter("Median", params);
	}

	void ImageProcessorWidget::onWhiteNoiseFilterParamsChanged(const Data::NamedVariantMap& params)
	{
		// Make undo action
		Managers::ImageContextsManager manager;
		Interfaces::IImageContextPointer context = manager.getContext(this);
		Q_ASSERT(context);
		context->undo();

		// Apply filter with new params
		applyFilter("White noise", params);
	}

	void ImageProcessorWidget::onDustNoiseFilterParamsChanged(const Data::NamedVariantMap& params)
	{
		// Make undo action
		Managers::ImageContextsManager manager;
		Interfaces::IImageContextPointer context = manager.getContext(this);
		Q_ASSERT(context);
		context->undo();

		// Apply filter with new params
		applyFilter("Dust noise", params);
	}

	void ImageProcessorWidget::onBilateralFilterParamsChanged(const Data::NamedVariantMap& params)
	{
		// Make undo action
		Managers::ImageContextsManager manager;
		Interfaces::IImageContextPointer context = manager.getContext(this);
		Q_ASSERT(context);
		context->undo();

		// Apply filter with new params
		applyFilter("Bilateral", params);
	}

	void ImageProcessorWidget::onWienerFilterParamsChanged(const Data::NamedVariantMap& params)
	{
		// Make undo action
		Managers::ImageContextsManager manager;
		Interfaces::IImageContextPointer context = manager.getContext(this);
		Q_ASSERT(context);
		context->undo();

		// Apply filter with new params
		applyFilter("Wiener", params);
	}

	//! Warning: do not edit this line!!! $OnParamsChangedImpl$

	void ImageProcessorWidget::onHideCustomizationWidgets(QWidget *target)
	{
		if (this != target)
		{
			return;
		}

		// Save state of customization widgets to restore it
		mGridCustomizerVisible = dwGridCustomizer->isVisible();
		dwGridCustomizer->setVisible(false);

		mCurrentFilterCustomizerVisible = dwCustomizer->isVisible();
		dwCustomizer->setVisible(false);

		// FIXME: save state for those too?
		dwColorChannel->setVisible(false);
		dwFilters->setVisible(false);

		update();
	}

	void ImageProcessorWidget::onRestoreCustomizationWidgets(QWidget *target)
	{
		if (this != target)
		{
			return;
		}

		// Restore customization widgets state
		dwGridCustomizer->setVisible(mGridCustomizerVisible);
		dwCustomizer->setVisible(mCurrentFilterCustomizerVisible);

		dwColorChannel->setVisible(true);
		dwFilters->setVisible(true);

		update();
	}

	void ImageProcessorWidget::onGridVisibleStateRequest(QWidget *target)
	{
		if (this == target)
		{
			emit notifyGridVisibleState(wImageView->isGridVisible());
		}
	}

	void ImageProcessorWidget::onCompareCurrentToGolden(QWidget *target)
	{
		if (this != target)
		{
			return;
		}

		Managers::ImageContextsManager	 manager;
		Interfaces::IImageContextPointer context = manager.getContext(this);

		const QImage&  currentImage     = context->getCurrentState()->mImage->getQImage();
		const QString& currentImageName = QFileInfo(context->getCurrentImageFile()).fileName();
		GoldenImageComparatorWidget *widget = new GoldenImageComparatorWidget(currentImage, currentImageName, this);
		widget->exec();
		widget->deleteLater();
	}

	void ImageProcessorWidget::onShowChannel(QWidget *target)
	{
		if (this != target)
		{
			return;
		}

		dwColorChannel->setVisible(! dwColorChannel->isVisible());
	}

	/**
	 protected:
	 */
	bool ImageProcessorWidget::eventFilter(QObject* obj, QEvent *e)
	{
		//if (obj == wImageView && e->type() == QEvent::Paint)
		//{
		//	QPaintEvent *ev = static_cast<QPaintEvent *>(e);
		//	
		//	// Obtain context and draw image
		//	Managers::ImageContextsManager	 manager;
		//	Interfaces::IImageContextPointer context = manager.getContext(this);

		//	Q_ASSERT(context);
		//  return true;
		//}

		return false;
	}

	void ImageProcessorWidget::paintEvent(QPaintEvent *e)
	{
		// FIXME: HACK (reduce cpu usage)
		if (e->rect() == rect())
			wImageView->redraw();
	}

	/**
	 private:
	 */
	void ImageProcessorWidget::createImageView(QWidget *parent)
	{
		wImageView = new ImageViewWidget(parent);
		wImageView->setObjectName("wImageView");
		QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
		//sizePolicy.setHorizontalStretch(0);
		//sizePolicy.setVerticalStretch(0);
		sizePolicy.setHeightForWidth(wImageView->sizePolicy().hasHeightForWidth());
		wImageView->setSizePolicy(sizePolicy);
		wImageView->setStyleSheet("background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #646262),  color-stop(0.5, #484848),  color-stop(0.5, #3D3D3D), color-stop(1, #4C4C4C));");

		//wImageView->installEventFilter(this);
	}

	void ImageProcessorWidget::createFiltersTreeView(QWidget *parent)
	{
		twFilters = new QTreeWidget(parent);
		twFilters->headerItem()->setText(1, QString());
		twFilters->setObjectName("twFilters");
		QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
		sizePolicy.setHorizontalStretch(0);
		sizePolicy.setVerticalStretch(0);
		sizePolicy.setHeightForWidth(twFilters->sizePolicy().hasHeightForWidth());
		twFilters->setSizePolicy(sizePolicy);
		twFilters->setMaximumSize(QSize(300, 16777215));
		QFont font;
		font.setFamily("Calibri");
		font.setPointSize(10);
		twFilters->setFont(font);
		twFilters->setStyleSheet("background-color: black; color: white;");

		// Setup filters tree view
		twFilters->setColumnCount(2);
		twFilters->setHeaderLabels(QStringList() << tr("Filters") << tr(""));
		twFilters->setIndentation(10); // Make indentation between viewport edge and item less

		loadFilters();

		twFilters->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);								 // Disable scroll bar
		twFilters->setColumnWidth(1, FilterTreeItemButtonGroup::getGroupUniformWidth()); // Make all buttons visible
		twFilters->resizeColumnToContents(0);

		// Create and setup add custom widget item
		mAddCustomFilterItem = new QTreeWidgetItem(twFilters, QStringList(tr("Add linear filter")));
		mAddCustomFilterItem->setIcon(0, QIcon(":/icons/mAddLinearFilterAction.png"));

		FilterTriggersDispatcher::getInstance().addTreeWidget(twFilters);
	}

	void ImageProcessorWidget::setupUi()
	{
		if (this->objectName().isEmpty())
			this->setObjectName("ImageProcessorWidgetForm");

		this->resize(726, 438);
		this->setStyleSheet("background-color: black;");
		
		createImageView(this);

		frame = new QFrame(this);
		frame->setObjectName("frame");
		frame->setFrameShape(QFrame::StyledPanel);
		frame->setFrameShadow(QFrame::Raised);

		dwFilters = new QDockWidget(frame);
		dwFilters->setObjectName("dwFilters");
		createFiltersTreeView(dwFilters);
		dwFilters->setWidget(twFilters);

		dwCustomizer = new QDockWidget(frame);
		dwCustomizer->setObjectName("dwCustomizer");
		//dwCustomizer->setWidget(dwCustomizerContents);
		dwCustomizer->hide();

		dwGridCustomizer = new QDockWidget(frame);
		dwGridCustomizer->setObjectName("dwGridCustomizer");
		mGridCustomizer = new GridCustomizationWidget(dwGridCustomizer);
		// FIXME: figure out why calling setWidget() only one time not working
		//dwGridCustomizer->setWidget(mGridCustomizer);
		dwGridCustomizer->hide();

		dwColorChannel = new QDockWidget(frame);
		dwColorChannel->setObjectName("dwColorChannel");
		mColorChannelWidget = new ColorChannelWidget(dwColorChannel);
		dwColorChannel->setWidget(mColorChannelWidget);

		verticalLayout = new QVBoxLayout(frame);
		verticalLayout->setObjectName("verticalLayout");
		verticalLayout->addWidget(dwFilters);
		verticalLayout->addWidget(dwCustomizer);
		verticalLayout->addWidget(dwGridCustomizer);
		verticalLayout->addWidget(dwColorChannel);
		verticalLayout->setStretch(0, 6);
		verticalLayout->setStretch(1, 1);
		verticalLayout->setStretch(2, 1);
		verticalLayout->setStretch(3, 1);

		QSplitter *splitter = new QSplitter(parentWidget());
		splitter->addWidget(wImageView);
		splitter->addWidget(frame);
		splitter->setOrientation(Qt::Horizontal);

		QList<int> sizes;
		sizes << 10 << 1;
		splitter->setSizes(sizes);

		horizontalLayout = new QHBoxLayout(this);
		horizontalLayout->setObjectName("horizontalLayout");
		horizontalLayout->addWidget(splitter);


		retranslateUi();

		QMetaObject::connectSlotsByName(this);

		connect(twFilters,			 SIGNAL( itemClicked(QTreeWidgetItem*, int) ),	 this, SLOT( onFilterWidgetItemClicked(QTreeWidgetItem*, int) ));
		connect(mColorChannelWidget, SIGNAL( modelChanged(Data::ColorModel) ), wImageView, SLOT( onColorModelChanged(Data::ColorModel) ));
		connect(mColorChannelWidget, SIGNAL( channelsChanged(bool, bool, bool) ), wImageView, SLOT( onColorChannelsChanged(bool, bool, bool) ));
		connect(mGridCustomizer, SIGNAL( paramsChanged(Data::NamedVariantMap) ), wImageView, SLOT( onGridParamsChanged(Data::NamedVariantMap) ));
		connect(mMedianFilterCustomizer, SIGNAL( paramsChanged(Data::NamedVariantMap) ), this, SLOT( onMedianFilterParamsChanged(Data::NamedVariantMap) ));
		connect(mWhiteNoiseFilterCustomizer, SIGNAL( paramsChanged(Data::NamedVariantMap) ), this, SLOT( onWhiteNoiseFilterParamsChanged(Data::NamedVariantMap) ));
		connect(mDustNoiseFilterCustomizer, SIGNAL( paramsChanged(Data::NamedVariantMap) ), this, SLOT( onDustNoiseFilterParamsChanged(Data::NamedVariantMap) ));
		connect(mBilateralFilterCustomizer, SIGNAL( paramsChanged(Data::NamedVariantMap) ), this, SLOT( onBilateralFilterParamsChanged(Data::NamedVariantMap) ));
		connect(mWienerFilterCustomizer, SIGNAL( paramsChanged(Data::NamedVariantMap) ), this, SLOT( onWienerFilterParamsChanged(Data::NamedVariantMap) ));
		//! Warning: do not edit this line!!! $ConnectCustomizerSignalToSlot$

		mGridCustomizer->setVisible(false);
	}

	void ImageProcessorWidget::retranslateUi()
	{
		this->setWindowTitle(QApplication::translate("ImageProcessorWidgetForm", "Form", 0));
		QTreeWidgetItem *___qtreewidgetitem = twFilters->headerItem();
		___qtreewidgetitem->setText(0, QApplication::translate("ImageProcessorWidgetForm", "Filters", 0));
	}

	void ImageProcessorWidget::loadFilters()
	{
		twFilters->header()->setStyleSheet("color: black;");
		
		mMorphologicalTop = new QTreeWidgetItem(QStringList(tr("Morphological")));
		twFilters->addTopLevelItem(mMorphologicalTop);
		mLinearTop  = new QTreeWidgetItem(QStringList(tr("Linear")));
		twFilters->addTopLevelItem(mLinearTop);
		mOtherTop  = new QTreeWidgetItem(QStringList(tr("Other")));
		twFilters->addTopLevelItem(mOtherTop);

		
		loadMorphologicalFilters();
		loadLinearFilters();
		loadOtherFilters();
		
		twFilters->expandAll();
		mLinearTop->setExpanded(false);
	}

	void ImageProcessorWidget::loadOtherFilters()
	{
		mBuiltinItems.mMedian = new QTreeWidgetItem(mOtherTop, QStringList(tr("Median")));
		mBuiltinItems.mWhiteNoise = new QTreeWidgetItem(mOtherTop, QStringList(tr("White noise")));
		mBuiltinItems.mDustNoise = new QTreeWidgetItem(mOtherTop, QStringList(tr("Dust noise")));
		mBuiltinItems.mBilateral = new QTreeWidgetItem(mOtherTop, QStringList(tr("Bilateral")));
		mBuiltinItems.mWiener = new QTreeWidgetItem(mOtherTop, QStringList(tr("Wiener")));
		//! Warning: do not edit this line!!! $CreateFilterTreeNode$
	}

	void ImageProcessorWidget::loadMorphologicalFilters()
	{
		mBuiltinItems.mInversion = new QTreeWidgetItem(mMorphologicalTop, QStringList(tr("Inversion")));
		mBuiltinItems.mDilation = new QTreeWidgetItem(mMorphologicalTop, QStringList(tr("Dilation")));
		mBuiltinItems.mErosion = new QTreeWidgetItem(mMorphologicalTop, QStringList(tr("Erosion")));
		mBuiltinItems.mTopHatTransform = new QTreeWidgetItem(mMorphologicalTop, QStringList(tr("Top-hat transform")));
		mBuiltinItems.mBottomHatTransform = new QTreeWidgetItem(mMorphologicalTop, QStringList(tr("Bottom-hat transform")));
	}

	void ImageProcessorWidget::loadLinearFilters()
	{
		while(mLinearTop->childCount() > 0)
		{
			mLinearTop->removeChild(mLinearTop->child(mLinearTop->childCount() - 1));
		}

		// Get custom filter names 
		LinearFiltersManager manager;
		QStringList customFilters = manager.getRegisteredFiltersNames();

		foreach (const QString& filter, customFilters)
		{
			QTreeWidgetItem* filterItem = new QTreeWidgetItem(mLinearTop, QStringList(filter));
			mLinearTop->addChild(filterItem);

			FilterTreeItemButtonGroup *group = new FilterTreeItemButtonGroup(filter, this);

			connect(group, SIGNAL( showButtonPressed(QString) ),	 this, SLOT( onFilterItemShowButtonPressed(QString) ));
			connect(group, SIGNAL( editButtonPressed(QString) ),	 this, SLOT( onFilterItemEditButtonPressed(QString) ));
			connect(group, SIGNAL( removeButtonPressed(QString) ), this, SLOT( onFilterItemRemoveButtonPressed(QString) ));

			bool ok = true;
			LinearFilterDescriptor descriptor = manager.getDescriptor(filter, &ok);
			Q_ASSERT(ok);
			if (!manager.canBeShowOrEdited(descriptor))
			{
				group->hideShowButton();
				group->hideEditButton();
			}

			twFilters->setItemWidget(filterItem, 1, group);
		}
	}

	void ImageProcessorWidget::applyFilterAndShowCustomizer(const QString& name)
	{
		// Apply filter, that would assign pointer to new customization widget if exists
		applyFilter(name);

		// Show applied filter customization widget if exists
		if (mCurrentFilterCustomizer)
		{
			dwCustomizer->setWidget(mCurrentFilterCustomizer);
			dwCustomizer->setVisible(true);
		}
		else
		{
			dwCustomizer->setVisible(false);
		}
	}

	void ImageProcessorWidget::onFilterItemClicked(QTreeWidgetItem* item)
	{
		if (twFilters->indexOfTopLevelItem(item) != -1) 
		{
			return;
		}
		
		applyFilterAndShowCustomizer(item->text(0));
	}

	void ImageProcessorWidget::onFilterActionTriggered(QAction* action)
	{
		Q_ASSERT(action);
		applyFilterAndShowCustomizer(action->text());
	}

	IFilterPointer ImageProcessorWidget::getFilterPointer(const QString& name, const Data::NamedVariantMap& params)
	{
		BuiltinFiltersFactory factory;
		IFilterPointer				filter;

		if (name == tr("Inversion"))
		{
			filter = factory.createInversionFilter();
			mCurrentFilterCustomizer = 0;
		}
		else if (name == tr("Dilation"))
		{
			filter = factory.createDilationFilter(params);
			mCurrentFilterCustomizer = 0;
		}
		else if (name == tr("Erosion"))
		{
			filter = factory.createErosionFilter(params);
			mCurrentFilterCustomizer = 0;
		}
		else if (name == tr("Top-hat transform"))
		{
			filter = factory.createTopHatTransformFilter(params);
			mCurrentFilterCustomizer = 0;
		}
		else if (name == tr("Bottom-hat transform"))
		{
			filter = factory.createBottomHatTransformFilter(params);
			mCurrentFilterCustomizer = 0;
		}
		else if (name == tr("Median"))
		{
			filter = factory.createMedianFilter(params);
			mCurrentFilterCustomizer = mMedianFilterCustomizer;
		}
		else if (name == tr("White noise"))
		{
			filter = factory.createWhiteNoiseFilter(params);
			mCurrentFilterCustomizer = mWhiteNoiseFilterCustomizer;
		}
		else if (name == tr("Dust noise"))
		{
			filter = factory.createDustNoiseFilter(params);
			mCurrentFilterCustomizer = mDustNoiseFilterCustomizer;
		}
		else if (name == tr("Bilateral"))
		{
			filter = factory.createBilateralFilter(params);
			mCurrentFilterCustomizer = mBilateralFilterCustomizer;
		}
		else if (name == tr("Wiener"))
		{
			filter = factory.createWienerFilter(params);
			mCurrentFilterCustomizer = mWienerFilterCustomizer;
		}
		//! Warning: do not edit this line!!! $CreateFilterInstanceUsingFactory$
		else 
		{
			LinearFiltersManager manager;
			bool ok;
			LinearFilterDescriptor desc;
			desc = manager.getDescriptor(name, &ok);
			if (ok)
			{
				filter = factory.createLinearFilter(desc);
				mCurrentFilterCustomizer = 0;
			}
			else
			{
				Q_ASSERT(! "Invalid filter name");
			}
		}

		return filter;
	}

	void ImageProcessorWidget::applyFilter(IFilterPointer filter)
	{
		// Obtain context and draw image
		Managers::ImageContextsManager	 manager;
		Interfaces::IImageContextPointer context = manager.getContext(this);

		Q_ASSERT(context);

		context->applyCommand(new FilterCommand(filter, context));
		emit commandApplied();

		update();
	}

	void ImageProcessorWidget::applyFilter(const QString& name, const Data::NamedVariantMap& params)
	{
		IFilterPointer filter = getFilterPointer(name, params);
		applyFilter(filter);
	}

	/** 
	 private slots:
	 */
	void ImageProcessorWidget::onFilterWidgetItemClicked(QTreeWidgetItem* item, int column)
	{
		if (item == mAddCustomFilterItem)
		{
			FilterEditorDialog* dialog = new FilterEditorDialog(this);

			LinearFilterDescriptor descriptor;
			if (!dialog->execute(&descriptor))
			{
				return;
			}

			// One of the validness checks
			if (descriptor.Name.isEmpty())
			{
				return;
			}

			LinearFiltersManager manager;
			manager.registerDescriptor(descriptor, dialog->shouldSaveOnDisk());

			dialog->deleteLater();

			loadLinearFilters();
			emit newLinearFilterAdded();
		}
	}

	void ImageProcessorWidget::onFilterItemShowButtonPressed(const QString& filter)
	{
		LinearFiltersManager manager;

		bool ok = true;
		LinearFilterDescriptor descriptor = manager.getDescriptor(filter, &ok);
		if (!ok)
		{
			QMessageBox::critical(twFilters, tr("Internal error occured!"), tr("Please contact developers!"));
			return;
		}

		FilterEditorDialog* dialog = new FilterEditorDialog(descriptor, FilterEditorDialog::MODE_READ_ONLY, this);
		dialog->execute();
		dialog->deleteLater();
	}

	void ImageProcessorWidget::onFilterItemEditButtonPressed(const QString& filter)
	{
		LinearFiltersManager manager;

		bool ok = true;
		LinearFilterDescriptor descriptor = manager.getDescriptor(filter, &ok);
		FilterEditorDialog* dialog = new FilterEditorDialog(descriptor, FilterEditorDialog::MODE_EDIT_EXISTING, this);

		LinearFilterDescriptor editedDescriptor;
		if (!dialog->execute(&editedDescriptor))
		{
			return;
		}

		// One of the validness checks
		if (editedDescriptor.Name.isEmpty())
		{
			return;
		}

		// Remove existing descriptor
		manager.removeDescriptor(filter);
		
		// Save new one
		manager.registerDescriptor(editedDescriptor, true);

		dialog->deleteLater();

		loadLinearFilters();
		emit newLinearFilterAdded();
	}

	void ImageProcessorWidget::onFilterItemRemoveButtonPressed(const QString& filter)
	{
		const int button = QMessageBox::information(twFilters, 
																								filter + tr(" will be removed."), 
																								tr("Are you sure you want to continue?"), 
																								tr("Yes"),
																								tr("No"),
																								tr(""),
																								1, // Default button No
																								1  // Escape button No
																								);
		if (button == 1) // No pressed
		{
			return;
		}

		// Remove filter
		LinearFiltersManager manager;
		manager.removeDescriptor(filter);

		loadLinearFilters();
		emit newLinearFilterAdded(); // TODO: Change signal
	}

} // namespace Ui