/*!
 *\file medianfiltercustomizationwidget.h
 *\brief This file contains definition of median filter customization widget
 */

#ifndef UI_MEDIANFILTERCUSTOMIZATIONWIDGET_H
	#define UI_MEDIANFILTERCUSTOMIZATIONWIDGET_H

	#include "abstractcustomizationwidget.h"

	#include "ui_medianfiltercustomizationwidgetform.h"

	namespace Ui
	{
		/*!
		 *\class MedianFilterCustomizationWidget
		 *\brief This class implements customization widget for median filter, show over image
		 */
		class MedianFilterCustomizationWidget : public AbstractCustomizationWidget, 
																						private MedianFilterCustomizationWidgetForm
		{
			Q_OBJECT
		public:
			/*!
			 *\brief Constructor
			 *\param parent Parent widget
			 */
			explicit MedianFilterCustomizationWidget(QWidget *parent = NULL);
			
		public slots:

			//! See AbstractCustomizationWidget.setParams(const Data::NamedVariantMap&)
			virtual void setParams(const Data::NamedVariantMap& params);

		private slots:

			/*!
			 *\brief Handle median filter radius value changed
			 *\param value New value
			 */
			void onSliderValueChanged(int value);
			void onSliderPressed();
			void onSliderReleased();

			/*!
			 *\brief Handle median filter radius value changed
			 *\param value New value
			 */
			void onLineEditTextChanged(const QString &value);

			/*!
			 *\brief Handle median filter type change
			 *\param index Index of new color
			 */
			void onMedianFilterTypeChanged(bool isHybridChecked);

		private:
			int mMinRadius, mMaxRadius;
			bool mIgnoreRadiusChanges;
			Data::NamedVariantMap mParams;
		};
	}
	

#endif // UI_MEDIANFILTERCUSTOMIZATIONWIDGET_H