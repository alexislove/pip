/*!
 *\file filtertreeitembutton.cpp
 *\brief Filter's tree widget item button definition
 */

#include "filtertreeitembutton.h"

namespace Ui
{
	/**
	 public:
	 */
	FilterTreeItemButton::FilterTreeItemButton(const QIcon& icon, const QString& text, const QString& filter, QWidget *parent/* = NULL */)
		: QPushButton(icon, text, parent),
			mFilterName(filter)
	{
		connect(this, SIGNAL( pressed() ), this, SLOT( onSuperPressed() ));
	}

	/**
	 private slots:
	 */
	 void FilterTreeItemButton::onSuperPressed()
	 {
		 emit pressed(mFilterName);
	 }

} // namespace Ui
