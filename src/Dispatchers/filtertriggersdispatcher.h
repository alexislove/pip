/*!
 *\file builtinfiltersdispatcher.h
 *\brief Dispatcher for builtin filter's UI events
 */

#ifndef DISPATCHERS_FILTERTRIGGERSDISPATCHER_H
	#define DISPATCHERS_FILTERTRIGGERSDISPATCHER_H

	#include <QObject>
	#include <QScopedPointer>

	#include "../Interfaces/ifiltertriggerlistener.h"

	class QAction;
	class QTreeWidget;
	class QTreeWidgetItem;
	class QWidget;

	namespace Dispatchers
	{
		/*!
		 *\class BuiltinFilterDispatcher
		 *\brief This class provides dispatch mechanisms for transferring events from actions and items to listeners
		 *\			 Main difference is that it transfers events only to the current listener, not for all
		 */
		class FilterTriggersDispatcher : public QObject
		{
			Q_OBJECT
		public:
			/*!
			 *\brief Get canonical instance reference
			 *\return Reference
			 */
			static FilterTriggersDispatcher& getInstance() 
			{
				if (mInstance)
					return *mInstance;
				mInstance.reset(new FilterTriggersDispatcher);
				return *mInstance;
			}

		public:
			/*!
			 *\brief Add action to control it's activity. If this action doesn't belong to the filter, undefined behavior is guaranteed
			 *\			 Dispatcher doesn't store any pointer internally
			 *\param action Action instance
			 */
			void addFilterAction(QAction* action);

			/*!
			 *\brief Add tree widget item to control it's activity.
			 *\			 Dispatcher doesn't store any pointer internally
			 *\param tree			Tree widget instance
			 */
			void addTreeWidget(QTreeWidget* tree);

			/*!
			 *\brief Set current listener for dispatcher events. 
			 *\			 U must take care of the current listener, because if wrong one is set, events will be transferred to the wrong target.
			 *\param listener Listener instance
			 */
			void setCurrentListener(Interfaces::IFilterTriggerListener* listeners);

		private:
			//! Canonical dispatcher instance
			static QScopedPointer< FilterTriggersDispatcher > mInstance;

		private:
			/*!
			 *\brief Constructor
			 */
			FilterTriggersDispatcher();

		private slots:
			/*!
			 *\brief Handle action trigger. If current listener is set, corresponding method will be called
			 */
			void onActionTriggered();

			/*!
			 *\brief Handle tree widget item click. If current listener is set, corresponding method will be called
			 *\param item Clicked item instance
			 *\param column Column, where item was clicked
			 */
			void onTreeWidgetItemClicked(QTreeWidgetItem* item, int column);

		private:
			//! Current listener instance
			Interfaces::IFilterTriggerListener *mCurrentListener;

		};
	} // namespace Dispatchers

#endif // DISPATCHERS_FILTERTRIGGERSDISPATCHER_H