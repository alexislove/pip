/*!
 *\file builtinfiltersdispatcher.cpp
 *\brief Dispatcher for builtin filter's UI events
 */

#include <QAction>
#include <QTreeWidget>

#include "filtertriggersdispatcher.h"

using namespace Interfaces;

namespace Dispatchers
{
	QScopedPointer< FilterTriggersDispatcher > FilterTriggersDispatcher::mInstance;

	/**
	 public:
	 */
	void FilterTriggersDispatcher::addFilterAction(QAction* action)
	{
		connect(action, SIGNAL( triggered() ), this, SLOT( onActionTriggered() ));
	}

	void FilterTriggersDispatcher::addTreeWidget(QTreeWidget* tree)
	{
		connect(tree, SIGNAL( itemClicked(QTreeWidgetItem*, int) ), this, SLOT( onTreeWidgetItemClicked(QTreeWidgetItem*, int) ));
	}

	void FilterTriggersDispatcher::setCurrentListener(IFilterTriggerListener* listener)
	{
		mCurrentListener = listener;
	}

	/**
	 private:
	 */
	FilterTriggersDispatcher::FilterTriggersDispatcher() 
		: mCurrentListener(NULL)
	{
	}

	/**
	 private slots:
	 */
	void FilterTriggersDispatcher::onActionTriggered()
	{
		// No checks, except current listener validity
		if (mCurrentListener)
		{
			// Retrieve action, that send the signal
			// See Qt documentation examples, why such kind of cast is valid
			QAction* action = static_cast< QAction* >(sender());
			mCurrentListener->onFilterActionTriggered(action);
		}
	}

	void FilterTriggersDispatcher::onTreeWidgetItemClicked(QTreeWidgetItem* item, int)
	{
		// No checks, except current listener validity
		if (mCurrentListener)
			mCurrentListener->onFilterItemClicked(item);
	}

} // namespace Dispatchers