/*!
 *\file commonimagecontext.h
 *\brief Common image context implementation
 */

#ifndef CONTEXTS_COMMONIMAGECONTEXT_H
	#define CONTEXTS_COMMONIMAGECONTEXT_H

	#include <QScopedPointer>
	#include <QUndoStack>

	#include "../Data/imagecontextstate.h"
	#include "../Data/safeimage.h"
	#include "../Interfaces/ifilter.h"
	#include "../Interfaces/iimagecontext.h"

	namespace Contexts
	{
		/*!
		 *\class CommonImageContext
		 *\brief This class implements IImageContext interface and is used for common operations on images
		 */
		class CommonImageContext : public Interfaces::IImageContext
		{
		public:
			/*!
			 *\brief Constructor
			 */
			explicit CommonImageContext();

			/*!
			 *\brief Destructor
			 */
			virtual ~CommonImageContext();

			/*!
			 *\brief See IImageContext.load(QString) for details
			 */
			virtual bool load(const QString& fileName);			

			/*!
			 *\brief See IImageContext.save(QString) for details
			 */
			virtual bool save(const QString& fileName);
			
			/*!
			 *\brief See IImageContext.applyCommand(QUndoCommand*) for details
			 */
			virtual void applyCommand(QUndoCommand *command);
			
			/*!
			 *\brief See IImageContext.applyFilter(IFilter*) for details
			 */
			virtual void applyFilter(Interfaces::IFilterPointer filter);

			/*!
			 *\brief See IImageContext.draw(QPainter*) for details
			 */
			virtual void draw(QPainter *painter) const;

			/*!
			 *\brief See IImageContext.redo() for details
			 */
			virtual void redo();

			/*!
			 *\brief See IImageContext.undo() for details
			 */
			virtual void undo();

			/*!
			 *\brief See IImageContext.releaseImage() for details
			 */
			void releaseImage();

			/*!
			 *\brief See IImageContext.clean() for details
			 */
			virtual void clean(); 

			/*!
			 *\brief See IImageContext.isDirty() for details
			 */
			virtual bool isDirty() const;

			/*!
			 *\brief See IImageContext.getCurrentImageFile() for details
			 */
			virtual const QString& getCurrentImageFile() const;

			/*!
			 *\brief See IImageContext.canRedo() for details
			 */
			virtual bool canRedo() const;

			/*!
			 *\brief See IImageContext.canUndo() for details
			 */
			virtual bool canUndo() const;

			/*!
			 *\brief See IImageContext.getHistory() for details
			 */
			virtual QStringList getHistory() const;
			
			
			/*!
			 *\brief See IImageContext.setCommandIndex(int) for details
			 */
			virtual void setCommandIndex(int index);

			/*!
			 *\brief See IImageContext.getCurrentCommandIndex() for details
			 */
			virtual int getCurrentCommandIndex() const;

			/*!
			 *\brief See IImageContext.getCurrentState() for details
			 */
			virtual Data::ImageContextStatePointer getCurrentState() const;

			/*!
			 *\brief See IImageContext.updateState() for details
			 */
			virtual void updateState(Data::ImageContextStatePointer state);

		private:
			//! Current image context state
			Data::ImageContextStatePointer mState;
			//! Undo/Redo stack of the context
			QScopedPointer< QUndoStack >	 mCommandStack;
		};
	} // namespace Contexts

#endif // CONTEXTS_COMMONIMAGECONTEXT_H