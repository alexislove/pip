/*!
 *\file commonimagecontext.h
 *\brief Common image context implementation
 */

#include <QApplication>
#include <QPainter>

#include "commonimagecontext.h"

using namespace Data;

namespace Contexts
{
	/**
	 public:
	 */
	CommonImageContext::CommonImageContext()
		: mState(new ImageContextState)
	{
		// Call clean, which will drop everything to it's defaults
		clean();
	}

	CommonImageContext::~CommonImageContext()
	{

	}

	bool CommonImageContext::load(const QString& fileName)
	{
		Q_ASSERT(mState);
		// Remove everything, that is stored
		clean();
		mState->mImage					  = SafeImagePointer(new SafeImage(fileName));
		mState->mCurrentImageFile = fileName;
		return !mState->mImage->getQImage().isNull();
	}

	bool CommonImageContext::save(const QString& fileName)
	{
		if (!mState->mImage || mState->mImage->isNull())
			return false;
		bool saved = mState->mImage->getQImage().save(fileName, 0 /* Leave default */, 100 /* 100% quality */);
		mState->mIsDirty = !saved; // Will be false, if save operation succeeded
		return saved;
	}

	void CommonImageContext::applyCommand(QUndoCommand *command)
	{
		mCommandStack->push(command);
	}

	void CommonImageContext::applyFilter(Interfaces::IFilterPointer filter)
	{
		// Set wait cursor
		QApplication::setOverrideCursor(Qt::WaitCursor);

		// FIXME: destroy previous image?

		// Update context state with the new one
		ImageContextStatePointer state(new ImageContextState);
		state->mCurrentImageFile = mState->mCurrentImageFile;
		state->mIsDirty					 = true;
		state->mImage						 = filter->apply(*mState->mImage);

		mState = state;

		// Fallback to the normal one
		QApplication::restoreOverrideCursor();
	}

	void CommonImageContext::draw(QPainter *painter) const
	{
		//if (!mState->mImage || mState->mImage->isNull())
		//{
		//	return;
		//}

		//const QImage& image = mState->mImage->getQImage();

		//// Calculate position for drawing, center of image will be placed in the center of widget
		//QRect windowRect  = painter->viewport();
		//int		width			  = windowRect.width();
		//int   height		  = windowRect.height();
		//int   imageWidth  = image.width();
		//int   imageHeight = image.height();

		//int x = width / 2  - imageWidth  / 2;
		//int y = height / 2 - imageHeight / 2;
		//
		//bool notScaled = true;
		//QImage clipped;
		//if (imageWidth > width || imageHeight > height)
		//{
		//	clipped   = image.scaled(qMin(imageWidth, width), qMin(imageHeight, height), Qt::KeepAspectRatio);
		//	// Recalculate coordinates once more
		//	x = width  / 2 - clipped.width()  / 2;
		//	y = height / 2 - clipped.height() / 2;
		//	notScaled = false;
		//}

		//painter->drawImage(x, y, image.transformed(mState->mTransform));
	}

	void CommonImageContext::undo()
	{
		mCommandStack->undo();
		mState->mIsDirty = canUndo(); // If no more undos can be done, it's initial state
	}

	void CommonImageContext::redo()
	{
		mCommandStack->redo();
		mState->mIsDirty = true;
	}

	void CommonImageContext::releaseImage()
	{
		if (mState->mImage)
		{
			mState->mImage->reset();
		}
	}

	void CommonImageContext::clean()
	{
		releaseImage();
		mCommandStack.reset(new QUndoStack);
		mState->mIsDirty				  = false;
		mState->mCurrentImageFile = QString();
//		mState->mTransform				= QTransform();
	}

	bool CommonImageContext::isDirty() const
	{
		return mState->mIsDirty;
	}

	const QString& CommonImageContext::getCurrentImageFile() const
	{
		return mState->mCurrentImageFile;
	}

	bool CommonImageContext::canRedo() const
	{
		return mCommandStack->canRedo();
	}

	bool CommonImageContext::canUndo() const
	{
		return mCommandStack->canUndo();
	}

	QStringList CommonImageContext::getHistory() const
	{
		QStringList history;
		// Since it's stack - iterate backward
		for (int idx = 0, count = mCommandStack->count(); idx < count; ++idx)
		{
			history << mCommandStack->text(idx);
		}

		return history;
	}

	void CommonImageContext::setCommandIndex(int index)
	{
		mCommandStack->setIndex(index);
	}

	int CommonImageContext::getCurrentCommandIndex() const
	{
		return mCommandStack->index();
	}

	ImageContextStatePointer CommonImageContext::getCurrentState() const
	{
		return mState;
	}

	void CommonImageContext::updateState(Data::ImageContextStatePointer state)
	{
		mState = state;
	}
} // namespace Contexts

