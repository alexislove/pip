/*!
 *\file bottomhattransformfilter.cpp
 *\brief Bottom-hat transform filter definition
 */

#include <QApplication>

#include "bottomhattransformfilter.h"
#include "erosionfilter.h"
#include "dilationfilter.h"
#include "../Tools/goldenimagecomparator.h"

using namespace Data;

namespace Filters
{
	namespace Constants
	{
		static const QString FilterName(QApplication::translate("BottomHatTransformFilter", "Bottom-hat transform"));
		static const char   *FilterIdName = "BottomHatTransformFilter";
	} // namespace Constants

	SafeImagePointer BottomHatTransformFilter::apply(const SafeImage& image)
	{
		DilationFilter dilationFilter;
		SafeImagePointer dilatedImage = dilationFilter.apply(image);

		ErosionFilter erosionFilter;
		SafeImagePointer closedImage = erosionFilter.apply(*dilatedImage);

		QImage::Format initialFormat = image.getQImage().format();
		QImage srcImage              = image.prepareForProcessing();
		QImage minuendImage          = closedImage->prepareForProcessing();
		QImage resImage;

		Tools::GoldenImageComparator comparator;
		bool ok = true;
		comparator.compare(minuendImage, srcImage, &resImage, &ok);

		return SafeImagePointer(new SafeImage(resImage.convertToFormat(initialFormat)));
	}

	const QString& BottomHatTransformFilter::getName() const
	{
		return Constants::FilterName;
	}

	const char* BottomHatTransformFilter::getIdName() const
	{
		return Constants::FilterIdName;
	}

	void BottomHatTransformFilter::setParameters(const Data::NamedVariantMap& params)
	{
		
	}

	void BottomHatTransformFilter::setDefaultParameters()
	{
		
	}

	const Data::NamedVariantMap& BottomHatTransformFilter::getParameters() const
	{
		Q_ASSERT(! "This method is not implemented!");

		static Data::NamedVariantMap params;
		return params;
	}
} // namespace Filters