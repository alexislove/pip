/*!
*\file erosionfilter.cpp
*\brief Erosion filter implementation
*/

#include <QApplication>
#include <QColor>

#include "erosionfilter.h"

using namespace Data;

namespace Filters
{
   namespace Constants
   {
	  static const QString FilterName(QApplication::translate("ErosionFilter", "Erosion"));
	  static const char   *FilterIdName = "Erosion";
	  static const int	   ErosionRadius = 1;
   } // namespace Constants

   SafeImagePointer ErosionFilter::apply(const SafeImage& image) 
   {
	  QImage::Format initialFormat = image.getQImage().format();
	  QImage srcImage              = image.prepareForProcessing();
	  QImage resImage              = srcImage.copy();

	  int colsCount = srcImage.bytesPerLine() / 4;
	  int rowsCount = srcImage.byteCount() / srcImage.bytesPerLine();

	  // Reinterpret cast is valid here, bits return uchar*, QRgb is actually unsigned int
	  QRgb* srcImageData = reinterpret_cast< QRgb* >(srcImage.bits());
	  QRgb* resImageData = reinterpret_cast< QRgb* >(resImage.bits());

	  for (int y = 0; y < rowsCount; ++y)
	  {
		 for (int x = 0; x < colsCount; ++x)
		 { 
			int minRedComponent   = 255;
			int minGreenComponent = 255;
			int minBlueComponent  = 255;

			// Calculate min R, G, and B components of adjacent pixels
			for (int i = -Constants::ErosionRadius; i <= Constants::ErosionRadius; ++i) 
			{
			   for (int j = -Constants::ErosionRadius; j <= Constants::ErosionRadius; ++j) 
			   {
				  int neighbourX = x + j;
				  int neighbourY = y + i;

				  // Skip pixels that are out of image bounds
				  if (neighbourX < 0 || neighbourX >= colsCount || neighbourY < 0 || neighbourY >= rowsCount) 
				  {
					 continue;
				  }

				  int neighbourIndex = neighbourY * colsCount + neighbourX;
				  QColor neighbourColor(srcImageData[neighbourIndex]);

				  if (neighbourColor.red() < minRedComponent) 
				  {
					 minRedComponent = neighbourColor.red();
				  }

				  if (neighbourColor.green() < minGreenComponent) 
				  {
					 minGreenComponent = neighbourColor.green();
				  }

				  if (neighbourColor.blue() < minBlueComponent) 
				  {
					 minBlueComponent = neighbourColor.blue();
				  }
			   }
			}

			// Current pixel index
			int index = y * colsCount + x;
			QColor src(srcImageData[index]);

			//Set result color of current pixel
			resImageData[index] = qRgba(minRedComponent, minGreenComponent, minBlueComponent, src.alpha());       
		 }
	  }

	  return SafeImagePointer(new SafeImage(resImage.convertToFormat(initialFormat)));
   }

   const QString& ErosionFilter::getName() const
   {
	  return Constants::FilterName;
   }

   const char* ErosionFilter::getIdName() const
   {
	  return Constants::FilterIdName;
   }

	 void ErosionFilter::setParameters(const Data::NamedVariantMap& params)
	 {
		 
	 }

	 void ErosionFilter::setDefaultParameters()
	 {
		 
	 }

	 const Data::NamedVariantMap& ErosionFilter::getParameters() const
	 {
		 Q_ASSERT(! "This method is not implemented!");

		 static Data::NamedVariantMap params;
		 return params;
	 }

} // namespace Filters