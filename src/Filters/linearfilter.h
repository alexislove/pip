/*!
 *\file linearfilter.h
 *\brief Linear filter definition
 */

#ifndef FILTER_LINEARFILTER_H
#define FILTER_LINEARFILTER_H

#include "../Interfaces/icustomizable.h"
#include "../Interfaces/ifilter.h"
#include "linearfilterdescriptor.h"

#include <QString>
#include <QVector>

namespace Filters
{

	 class LinearFilter : public Interfaces::IFilter,
												public Interfaces::ICustomizable
	 {
	 public:
			/*!
			*\brief Constructor
			*/
			LinearFilter(const LinearFilterDescriptor &desc);

			/*!
			*\brief Destructor
			*/
			virtual ~LinearFilter() 
			{
			}

			//! See IFilter.apply(const SafeImage&)
			virtual Data::SafeImagePointer apply(const Data::SafeImage& image);

			//! See IFilter.getName()
			virtual const QString& getName() const;

			//! See IFilter.getIdName()
			virtual const char* getIdName() const;

			//! See ICustomizable.setParameters(const Data::NamedVariantMap&)
			virtual void setParameters(const Data::NamedVariantMap& params);

			//! See ICustomizable.setDefaultParameters()
			virtual void setDefaultParameters();

	 private:
			LinearFilterDescriptor mDesc;
			int mDimension;
			int mWindowHalfSize;

	 private:
			inline float getWeight(int i, int j) const { return mDesc.Matrix[i + mWindowHalfSize][j + mWindowHalfSize]; }

			virtual const Data::NamedVariantMap& getParameters() const;

	 };
}

#endif // FILTER_LINEARFILTER_H