/*!
 *\file inversionfilter.h
 *\brief Inversion filter definition
 */

#ifndef FILTER_INVERSIONFILTER_H
	#define FILTER_INVERSIONFILTER_H

	#include "../Interfaces/ifilter.h"

	namespace Filters
	{
		class InversionFilter : public Interfaces::IFilter
		{
		public:
			virtual ~InversionFilter()
			{
			}

			//! See IFilter.apply(const SafeImage&)
			virtual Data::SafeImagePointer apply(const Data::SafeImage& image);

			//! See IFilter.getName()
			virtual const QString& getName() const;

			//! See IFilter.getIdName()
			virtual const char* getIdName() const;
		};
	} // namespace Filters

#endif // FILTER_INVERSIONFILTER_H