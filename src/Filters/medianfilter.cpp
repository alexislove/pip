#include "medianfilter.h"
#include "../Auxiliary/utils.h"

//#include <QColor>

using namespace Filters;
using namespace Data;

MedianFilter::MedianFilter()
{
	setDefaultParameters();
}

/* private */

void MedianFilter::setDimension(int dim)
{
	// Minimal radius allowed is 3
	Q_ASSERT(dim >= 3);
	// Only odd radius is allowed, i.e. 3, 5, 7, ...
	Q_ASSERT(dim & 1);

	mDimension = dim;
	mWindowHalfSize = mDimension / 2;
}

// FIXME: move to Auxiliary (if needed somewhere else)
template <typename T>
T median(T *sample, int N)
{
	// Order elements (only half of them)
	for (int i = 0; i < (N >> 1) + 1; ++i)
	{
		// Find position of minimum element
		int min = i;
		for (int j = i + 1; j < N; ++j)
			if (sample[j] < sample[min])
				min = j;
		// Put found minimum element in its place
		const T temp = sample[i];
		sample[i] = sample[min];
		sample[min] = temp;
	}
	// Get result - the middle element
	return sample[N >> 1];
}

/* public */

Data::SafeImagePointer MedianFilter::apply(const Data::SafeImage& image)
{
	switch (mType)
	{
	case Filters::MedianFilter::Normal:
		return applyNormal(image);

	case Filters::MedianFilter::Hybrid:
		return applyHybrid(image);

	default:
		throw std::exception("Undefined median filter type.");
		break;
	}
}

/* private */

Data::SafeImagePointer MedianFilter::applyNormal(const Data::SafeImage& image)
{
	QImage::Format initialFormat = image.getQImage().format();
	QImage srcImage              = image.prepareForProcessing();
	QImage resImage              = srcImage.copy();

	int colsCount = srcImage.bytesPerLine() / 4;
	int rowsCount = srcImage.byteCount() / srcImage.bytesPerLine();

	// Reinterpret cast is valid here, bits return uchar*, QRgb is actually unsigned int
	QRgb* srcImageData = reinterpret_cast< QRgb* >(srcImage.bits());
	QRgb* resImageData = reinterpret_cast< QRgb* >(resImage.bits());

	const int numChannels = 3;
	// Pixel window for each of color channels
	float *window[numChannels];

	for (int i = 0; i < numChannels; ++i)
	{
		window[i] = new float [mDimension * mDimension];
	}


	// Main filter processing loop
	// http://en.wikipedia.org/wiki/Median_filter
	for (int y = 0; y < rowsCount; ++y)
	{
		for (int x = 0; x < colsCount; ++x)
		{ 
			int fill = 0;

			for (int i = -mWindowHalfSize; i <= mWindowHalfSize; ++i)
			{
				for (int j = -mWindowHalfSize; j <= mWindowHalfSize; ++j)
				{
					int xpos = x + j, ypos = y + i;

					// Correct neighbour position in case if it is out of image borders according to border policy
					switch (mBorderPolicy)
					{
					case Filters::MedianFilter::ExtendEdge:
						// Extending edge policy
						if (xpos < 0)
							xpos = 0;
						else if (xpos >= colsCount)
							xpos = colsCount - 1;
						if (ypos < 0)
							ypos = 0;
						else if (ypos >= rowsCount)
							ypos = rowsCount - 1;
						break;
					case Filters::MedianFilter::Toroidal:
						// Toroidal edge policy
						xpos = abs(xpos % colsCount);
						ypos = abs(ypos % rowsCount);
						break;
					default:
						break;
					}

					int neighbourIndex = ypos * colsCount + xpos;
					QColor neighbourColor(srcImageData[neighbourIndex]);

					// Pixel window filling
					window[0][fill] = neighbourColor.redF();
					window[1][fill] = neighbourColor.greenF();
					window[2][fill] = neighbourColor.blueF();
					fill++;
				}
			}

			float r = median(window[0], mDimension * mDimension);
			float g = median(window[1], mDimension * mDimension);
			float b = median(window[2], mDimension * mDimension);

			// Current pixel index
			int index = y * colsCount + x;
			QColor src(srcImageData[index]);

			// Set result color of current pixel
			resImageData[index] = qRgba(Auxiliary::floatToIntColor(r),
				Auxiliary::floatToIntColor(g),
				Auxiliary::floatToIntColor(b), src.alpha());
		}
	}

	return SafeImagePointer(new SafeImage(resImage.convertToFormat(initialFormat)));
}

Data::SafeImagePointer MedianFilter::applyHybrid(const Data::SafeImage& image)
{
	QImage::Format initialFormat = image.getQImage().format();
	QImage srcImage              = image.prepareForProcessing();
	QImage resImage              = srcImage.copy();

	int colsCount = srcImage.bytesPerLine() / 4;
	int rowsCount = srcImage.byteCount() / srcImage.bytesPerLine();

	// Reinterpret cast is valid here, bits return uchar*, QRgb is actually unsigned int
	QRgb* srcImageData = reinterpret_cast< QRgb* >(srcImage.bits());
	QRgb* resImageData = reinterpret_cast< QRgb* >(resImage.bits());

	const int numChannels = 3;
	// X window for each of color channels
	float *windowX[numChannels];
	// Cross window for each of color channels
	float *windowC[numChannels];
	// Results window (always contains 3 elements)
	float windowR[3][numChannels];

	for (int i = 0; i < numChannels; ++i)
	{
		windowX[i] = new float [(mDimension - 1) * 2 + 1];
		windowC[i] = new float [(mDimension - 1) * 2 + 1];
	}

	for (int y = 0; y < rowsCount; ++y)
	{
		for (int x = 0; x < colsCount; ++x)
		{ 
			int fillX = 0, fillC = 0;

			for (int i = -mWindowHalfSize; i <= mWindowHalfSize; ++i)
			{
				for (int j = -mWindowHalfSize; j <= mWindowHalfSize; ++j)
				{
					int xpos = x + j, ypos = y + i;

					// Correct neighbour position in case if it is out of image borders according to border policy
					switch (mBorderPolicy)
					{
					case Filters::MedianFilter::ExtendEdge:
						// Extending edge policy
						if (xpos < 0)
							xpos = 0;
						else if (xpos >= colsCount)
							xpos = colsCount - 1;
						if (ypos < 0)
							ypos = 0;
						else if (ypos >= rowsCount)
							ypos = rowsCount - 1;
						break;
					case Filters::MedianFilter::Toroidal:
						// Toroidal edge policy
						xpos = abs(xpos % colsCount);
						ypos = abs(ypos % rowsCount);
						break;
					default:
						break;
					}

					int neighbourIndex = ypos * colsCount + xpos;
					QColor neighbourColor(srcImageData[neighbourIndex]);

					// X window pass
					if (abs(i) == abs(j))
					{
						windowX[0][fillX] = neighbourColor.redF();
						windowX[1][fillX] = neighbourColor.greenF();
						windowX[2][fillX] = neighbourColor.blueF();
						fillX++;
					}
					// Cross window pass
					// Note: we are missing central pixel here
					else if (i == 0 || j == 0)
					{
						windowC[0][fillC] = neighbourColor.redF();
						windowC[1][fillC] = neighbourColor.greenF();
						windowC[2][fillC] = neighbourColor.blueF();
						fillC++;
					}
				}
			}

			// Add missing central pixel to cross window
			int neighbourIndex = y * colsCount + x;
			QColor neighbourColor(srcImageData[neighbourIndex]);
			windowC[0][fillC] = neighbourColor.redF();
			windowC[1][fillC] = neighbourColor.greenF();
			windowC[2][fillC] = neighbourColor.blueF();
			fillC++;

			// Final pass
			windowR[0][0] = median(windowC[0], fillC);
			windowR[1][0] = median(windowC[1], fillC);
			windowR[2][0] = median(windowC[2], fillC);

			windowR[0][1] = median(windowX[0], fillX);
			windowR[1][1] = median(windowX[1], fillX);
			windowR[2][1] = median(windowX[2], fillX);

			// Current pixel index
			int index = y * colsCount + x;
			QColor src(srcImageData[index]);

			windowR[0][2] = src.redF();
			windowR[1][2] = src.greenF();
			windowR[2][2] = src.blueF();

			float r = median(windowR[0], 3);
			float g = median(windowR[1], 3);
			float b = median(windowR[2], 3);

			// Set result color of current pixel
			resImageData[index] = qRgba(Auxiliary::floatToIntColor(r),
				Auxiliary::floatToIntColor(g),
				Auxiliary::floatToIntColor(b), src.alpha());
		}
	}

	return SafeImagePointer(new SafeImage(resImage.convertToFormat(initialFormat)));
}

/* public */

const QString& MedianFilter::getName() const
{
	// FIXME
	static const QString name("Median");
	return name;
}


const char* MedianFilter::getIdName() const
{
	// FIXME
	return "median";
}


void MedianFilter::setParameters( const Data::NamedVariantMap& params )
{
	// Dimension
	auto it = params.find("radius");
	if (it != params.end()) {
		// Dimension = Radius * 2 + 1
		setDimension((*it).asVariant().toInt() * 2 + 1);
	}

	// Type
	it = params.find("type");
	if (it != params.end()) {
		// FIXME: check if value is valid
		mType = static_cast<FilterType>((*it).asVariant().toInt());
	}

	// Border policy
	it = params.find("border_policy");
	if (it != params.end()) {
		// FIXME: check if value is valid
		mBorderPolicy = static_cast<BorderPolicy>((*it).asVariant().toInt());
	}
}

void MedianFilter::setDefaultParameters()
{
	setDimension(5);
	mType = MedianFilter::Normal;
	mBorderPolicy = MedianFilter::Toroidal;
}

const Data::NamedVariantMap& MedianFilter::getParameters() const
{
	static Data::NamedVariantMap params;

	params["radius"] = Data::ExtendedVariant::toExtendedVariant(QVariant((mDimension - 1) / 2));
	params["type"] = Data::ExtendedVariant::toExtendedVariant(QVariant(mType));
	params["border_policy"] = Data::ExtendedVariant::toExtendedVariant(QVariant(mBorderPolicy));

	return params;
}