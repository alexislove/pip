/*!
 *\file inversionfilter.cpp
 *\brief Inversion filter definition
 */

#include <QApplication>
#include <QColor>

#include "inversionfilter.h"

using namespace Data;

namespace Filters
{
	namespace Constants
	{
		static const QString FilterName(QApplication::translate("InversionFilter", "Inversion"));
		static const char   *FilterIdName = "Inversion";
	} // namespace Constants

	SafeImagePointer InversionFilter::apply(const SafeImage& image)
	{
		// This routine is not so huge, because I'm stupid
		// It performs safe copy of the image, converting it to image-independent
		// format, modifying bits and the restore format in the new image

		const QImage&  srcImage      = image.getQImage();
		QImage::Format initialFormat = image.getQImage().format();

		QImage resImage = image.prepareForProcessing();

		int bytesPerLine = resImage.bytesPerLine();
		int colsCount    = resImage.bytesPerLine() / 4;
		int rowsCount    = resImage.byteCount() / bytesPerLine; // 4 bytes per color

		// Reinterpret cast is valid here, bits return uchar*, QRgb is actually unsigned int
		QRgb* imageData = reinterpret_cast< QRgb* >(resImage.bits()); // Unfortunately, this will make deep copy of data
		for (int y = 0; y < rowsCount; ++y)
		{
			for (int x = 0; x < colsCount; ++x)
			{
				int index = y * colsCount + x;
				QColor src(imageData[index]);
				imageData[index] = qRgba(255 - src.red(), 255 - src.green(), 255 - src.blue(), src.alpha());
			}
		}

		return SafeImagePointer(new SafeImage(resImage.convertToFormat(initialFormat)));
	}

	const QString& InversionFilter::getName() const
	{
		return Constants::FilterName;
	}

	const char* InversionFilter::getIdName() const
	{
		return Constants::FilterIdName;
	}
} // namespace Filters
