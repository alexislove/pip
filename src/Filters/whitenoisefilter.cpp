#include "WhiteNoiseFilter.h"
#include "../Auxiliary/utils.h"

#include <QColor>

using namespace Filters;
using namespace Data;

WhiteNoiseFilter::WhiteNoiseFilter()
{
	setDefaultParameters();
}

Data::SafeImagePointer WhiteNoiseFilter::apply(const Data::SafeImage& image)
{
	QImage::Format initialFormat = image.getQImage().format();
	QImage srcImage              = image.prepareForProcessing();
	QImage resImage              = srcImage.copy();

	int colsCount = srcImage.bytesPerLine() / 4;
	int rowsCount = srcImage.byteCount() / srcImage.bytesPerLine();

	// Reinterpret cast is valid here, bits return uchar*, QRgb is actually unsigned int
	QRgb* srcImageData = reinterpret_cast< QRgb* >(srcImage.bits());
	QRgb* resImageData = reinterpret_cast< QRgb* >(resImage.bits());


	// Main filter processing loop
	for (int y = 0; y < rowsCount; ++y)
	{
		for (int x = 0; x < colsCount; ++x)
		{ 
			const float scale = 10.0f;
			float whiteNoiseAddition;

			// Probability pass test
			if (rand() / (double) RAND_MAX <= mProbability)
			{
				float g = Auxiliary::gauss(mProbability);
				Q_ASSERT(g <= scale);
				whiteNoiseAddition = g / scale * mMaxDisplacement;
			}
			else
			{
				whiteNoiseAddition = 0.f;
			}

			// Current pixel index
			int index = y * colsCount + x;
			QColor src(srcImageData[index]);

			float r, g, b;
			r = src.redF()   + whiteNoiseAddition;
			g = src.greenF() + whiteNoiseAddition;
			b = src.blueF()  + whiteNoiseAddition;

			// Set result color of current pixel
			resImageData[index] = qRgba(Auxiliary::floatToIntColor(r),
																	Auxiliary::floatToIntColor(g),
																	Auxiliary::floatToIntColor(b), src.alpha());
		}
	}

	return SafeImagePointer(new SafeImage(resImage.convertToFormat(initialFormat)));
}


const QString& WhiteNoiseFilter::getName() const
{
	// FIXME ?
	static const QString name("White noise");
	return name;
}


const char* WhiteNoiseFilter::getIdName() const
{
	// FIXME ?
	return "White noise";
}

void WhiteNoiseFilter::setParameters( const Data::NamedVariantMap& params )
{
	// mProbability
	auto it = params.find("Probability");
	if (it != params.end()) {
		mProbability = (*it).asVariant().toFloat();
	}

	// mMaxDisplacement
	it = params.find("MaxDisplacement");
	if (it != params.end()) {
		mMaxDisplacement = (*it).asVariant().toFloat();
	}
}

void WhiteNoiseFilter::setDefaultParameters()
{
	mProbability = 0.5f;
	mMaxDisplacement = 128 / 255.f;
}

const Data::NamedVariantMap& Filters::WhiteNoiseFilter::getParameters() const
{
	static Data::NamedVariantMap params;

	params["Probability"] = Data::ExtendedVariant::toExtendedVariant(QVariant(mProbability));
	params["MaxDisplacement"] = Data::ExtendedVariant::toExtendedVariant(QVariant(mMaxDisplacement));

	return params;
}
