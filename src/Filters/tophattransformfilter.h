/*!
 *\file tophattransformfilter.h
 *\brief Top-hat transform filter definition
 */

#ifndef FILTER_TOPHATTRANSFORMFILTER_H
	#define FILTER_TOPHATTRANSFORMFILTER_H

	#include "../Interfaces/icustomizable.h"
	#include "../Interfaces/ifilter.h"

	namespace Filters
	{
		class TopHatTransformFilter : public Interfaces::IFilter,
																	public Interfaces::ICustomizable
		{
		public:
			virtual ~TopHatTransformFilter()
			{
			}

			//! See IFilter.apply(const SafeImage&)
			virtual Data::SafeImagePointer apply(const Data::SafeImage& image);

			//! See IFilter.getName()
			virtual const QString& getName() const;

			//! See IFilter.getIdName()
			virtual const char* getIdName() const;

			//! See ICustomizable.setParameters(const Data::NamedVariantMap&)
			virtual void setParameters(const Data::NamedVariantMap& params);

			//! See ICustomizable.setDefaultParameters()
			virtual void setDefaultParameters();

			virtual const Data::NamedVariantMap& getParameters() const;
		};
	} // namespace Filters

#endif // FILTER_TOPHATTRANSFORMFILTER_H