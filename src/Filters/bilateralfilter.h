/*!
 *\file bilateralfilter.h
 *\brief Bilateral filter definition
 */

#ifndef FILTER_BILATERALFILTER_H
	#define FILTER_BILATERALFILTER_H

	#include "../Interfaces/icustomizable.h"
	#include "../Interfaces/ifilter.h"

	namespace Filters
	{
		class BilateralFilter : public Interfaces::IFilter,
														public Interfaces::ICustomizable
		{
		public:
			/*!
			 *\brief Constructor
			 */
			BilateralFilter();

			/*!
			 *\brief Destructor
			 */
			virtual ~BilateralFilter() 
			{
			}

			//! See IFilter.apply(const SafeImage&)
			virtual Data::SafeImagePointer apply(const Data::SafeImage& image);

			//! See IFilter.getName()
			virtual const QString& getName() const;

			//! See IFilter.getIdName()
			virtual const char* getIdName() const;

			//! See ICustomizable.setParameters(const Data::NamedVariantMap&)
			virtual void setParameters(const Data::NamedVariantMap& params);

			//! See ICustomizable.setDefaultParameters()
			virtual void setDefaultParameters();

			//! See ICustomizable.getParameters()
			virtual const Data::NamedVariantMap& getParameters() const;

		private:
			int mRadius;
			float mSigmaRadiometric;
			float mSigmaSpatial;
		};
	}

#endif // FILTER_BILATERALFILTER_H