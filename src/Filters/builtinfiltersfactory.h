/*!
 *\file builtinfiltersfactory.h
 *\brief Factory definition for built-in filters
 */

#ifndef FILTERS_BULTINFILTERSFACTORY_H
	#define FILTERS_BULTINFILTERSFACTORY_H

	#include "../Data/namedvariantmap.h"
	#include "../Interfaces/ifilter.h"
	#include "../Filters/linearfilterdescriptor.h"

	namespace Filters
	{
		/*!
		 *\class BuiltinFiltersFactory
		 *\brief Application built-in filters set factory
		 */
		struct BuiltinFiltersFactory
		{
			//! Inversion filter
			Interfaces::IFilterPointer createInversionFilter() const;

			//! Dilation filter
			Interfaces::IFilterPointer createDilationFilter(const Data::NamedVariantMap& params = Data::NamedVariantMap()) const;

			//! Erosion filter
			Interfaces::IFilterPointer createErosionFilter(const Data::NamedVariantMap& params = Data::NamedVariantMap()) const;

			//! Top-hat transform filter
			Interfaces::IFilterPointer createTopHatTransformFilter(const Data::NamedVariantMap& params = Data::NamedVariantMap()) const;

			//! Bottom-hat transform filter
			Interfaces::IFilterPointer createBottomHatTransformFilter(const Data::NamedVariantMap& params = Data::NamedVariantMap()) const;

			//! Generic linear filter
			Interfaces::IFilterPointer createLinearFilter(const Filters::LinearFilterDescriptor &desc, const Data::NamedVariantMap& params = Data::NamedVariantMap()) const;

			//! Median filter
			Interfaces::IFilterPointer createMedianFilter(const Data::NamedVariantMap& params = Data::NamedVariantMap()) const;

			//! White noise filter
			Interfaces::IFilterPointer createWhiteNoiseFilter(const Data::NamedVariantMap& params = Data::NamedVariantMap()) const;

			//! Dust noise filter
			Interfaces::IFilterPointer createDustNoiseFilter(const Data::NamedVariantMap& params = Data::NamedVariantMap()) const;

			//! Bilateral filter
			Interfaces::IFilterPointer createBilateralFilter(const Data::NamedVariantMap& params = Data::NamedVariantMap()) const;

			//! Wiener filter
			Interfaces::IFilterPointer createWienerFilter(const Data::NamedVariantMap& params = Data::NamedVariantMap()) const;

			//! Warning: do not edit this line!!! $FactoryMethodDecl$
		};
	} // namespace Filters

#endif // FILTERS_BULTINFILTERSFACTORY_H
