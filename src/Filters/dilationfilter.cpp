/*!
 *\file dilationfilter.cpp
 *\brief Dilation filter definition
 */

#include <QApplication>
#include <QColor>

#include "dilationfilter.h"

using namespace Data;

namespace Filters
{
	namespace Constants
	{
		static const QString FilterName(QApplication::translate("DilationFilter", "Dilation"));
		static const char *FilterIdName = "Dilation";
		static const int DelationRadius = 1;
	} // namespace Constants

	SafeImagePointer DilationFilter::apply(const SafeImage& image) 
	{
		QImage::Format initialFormat = image.getQImage().format();
		QImage srcImage              = image.prepareForProcessing();
		QImage resImage              = srcImage.copy();

		int colsCount = srcImage.bytesPerLine() / 4;
		int rowsCount = srcImage.byteCount() / srcImage.bytesPerLine();

		// Reinterpret cast is valid here, bits return uchar*, QRgb is actually unsigned int
		QRgb* srcImageData = reinterpret_cast< QRgb* >(srcImage.bits());
		QRgb* resImageData = reinterpret_cast< QRgb* >(resImage.bits());

		for (int y = 0; y < rowsCount; ++y)
		{
			for (int x = 0; x < colsCount; ++x)
			{ 
				int maxRedComponent   = 0;
				int maxGreenComponent = 0;
				int maxBlueComponent  = 0;

				// Calculate max R, G, and B components of adjacent pixels
				for (int i = -Constants::DelationRadius; i <= Constants::DelationRadius; ++i) 
				{
					for (int j = -Constants::DelationRadius; j <= Constants::DelationRadius; ++j) 
					{
						int neighbourX = x + j;
						int neighbourY = y + i;

						// Skip pixels that are out of image bounds
						if (neighbourX < 0 || neighbourX >= colsCount || neighbourY < 0 || neighbourY >= rowsCount) 
						{
							continue;
						}

						int neighbourIndex = neighbourY * colsCount + neighbourX;
						QColor neighbourColor(srcImageData[neighbourIndex]);

						if (neighbourColor.red() > maxRedComponent) 
						{
							maxRedComponent = neighbourColor.red();
						}

						if (neighbourColor.green() > maxGreenComponent) 
						{
							maxGreenComponent = neighbourColor.green();
						}

						if (neighbourColor.blue() > maxBlueComponent) 
						{
							maxBlueComponent = neighbourColor.blue();
						}
					}
				}

				// Current pixel index
				int index = y * colsCount + x;
				QColor src(srcImageData[index]);
				
				//Set result color of current pixel
				resImageData[index] = qRgba(maxRedComponent, maxGreenComponent, maxBlueComponent, src.alpha());       
			}
		}

		return SafeImagePointer(new SafeImage(resImage.convertToFormat(initialFormat)));
	}

	const QString& DilationFilter::getName() const
	{
		return Constants::FilterName;
	}

	const char* DilationFilter::getIdName() const
	{
		return Constants::FilterIdName;
	}

	void DilationFilter::setParameters(const Data::NamedVariantMap& params)
	{
		
	}

	void DilationFilter::setDefaultParameters()
	{
		
	}

	const Data::NamedVariantMap& DilationFilter::getParameters() const
	{
		Q_ASSERT(! "This method is not implemented!");

		static Data::NamedVariantMap params;
		return params;
	}

} // namespace Filters