#include "linearfilter.h"
#include "../Auxiliary/utils.h"

#include <QColor>

using namespace Filters;
using namespace Data;

LinearFilter::LinearFilter(const LinearFilterDescriptor &desc)
	 : mDesc(desc), mDimension(mDesc.Matrix.size()), mWindowHalfSize(mDimension / 2)
{
	 for each (QVector<float> row in mDesc.Matrix)
	 {
			Q_ASSERT(row.size() == mDimension);
	 }
}

Data::SafeImagePointer LinearFilter::apply(const Data::SafeImage& image)
{
	 QImage::Format initialFormat = image.getQImage().format();
	 QImage srcImage              = image.prepareForProcessing();
	 QImage resImage              = srcImage.copy();

	 int colsCount = srcImage.bytesPerLine() / 4;
	 int rowsCount = srcImage.byteCount() / srcImage.bytesPerLine();

	 // Reinterpret cast is valid here, bits return uchar*, QRgb is actually unsigned int
	 QRgb* srcImageData = reinterpret_cast< QRgb* >(srcImage.bits());
	 QRgb* resImageData = reinterpret_cast< QRgb* >(resImage.bits());

	 for (int y = 0; y < rowsCount; ++y)
	 {
			for (int x = 0; x < colsCount; ++x)
			{ 
				 float r = 0.f, g = 0.f, b = 0.f;

				 for (int i = -mWindowHalfSize; i <= mWindowHalfSize; ++i)
						for (int j = -mWindowHalfSize; j <= mWindowHalfSize; ++j)
						{
							 int xpos = x + j, ypos = y + i;
							 // Correct neighbour position in case if it is out of image borders
							 if (xpos < 0)
							 {
									xpos = 0;
							 }
							 else if (xpos >= colsCount)
							 {
									xpos = colsCount - 1;
							 }

							 if (ypos < 0)
							 {
									ypos = 0;
							 }
							 else if (ypos >= rowsCount)
							 {
									ypos = rowsCount - 1;
							 }
							 
							 int neighbourIndex = ypos * colsCount + xpos;
							 QColor neighbourColor(srcImageData[neighbourIndex]);
							 r += getWeight(i, j) * neighbourColor.redF();
							 g += getWeight(i, j) * neighbourColor.greenF();
							 b += getWeight(i, j) * neighbourColor.blueF();
						}

						r /= mDesc.Denominator;
						g /= mDesc.Denominator;
						b /= mDesc.Denominator;

						// Current pixel index
						int index = y * colsCount + x;
						QColor src(srcImageData[index]);

						// Set result color of current pixel
						resImageData[index] = qRgba(Auxiliary::floatToIntColor(r),
																				Auxiliary::floatToIntColor(g),
																				Auxiliary::floatToIntColor(b), src.alpha());
			}
	 }

	 return SafeImagePointer(new SafeImage(resImage.convertToFormat(initialFormat)));
}


const QString& LinearFilter::getName() const
{
	 return mDesc.Name;
}


const char* LinearFilter::getIdName() const
{
	 return mDesc.Name.toStdString().c_str();
}

void LinearFilter::setParameters( const Data::NamedVariantMap& params )
{
	
}

void LinearFilter::setDefaultParameters()
{
	
}

const Data::NamedVariantMap& Filters::LinearFilter::getParameters() const
{
	Q_ASSERT(! "This method is not implemented!");

	static Data::NamedVariantMap params;
	return params;
}
