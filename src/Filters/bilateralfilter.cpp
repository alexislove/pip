/*!
 *\file bilateralfilter.cpp
 *\brief Bilateral filter definition
 */

#include <QApplication>

#include "bilateralfilter.h"
/*!
 *\note Uncomment for auxiliary routines
 *\ #include "../Auxiliary/utils.h"
 */

using namespace Data;

namespace Filters
{
	namespace Constants
	{
		static const QString FilterName(QApplication::translate("BilateralFilter", "Bilateral"));
		static const char   *FilterIdName = "Bilateral";
	} // namespace Constants

	BilateralFilter::BilateralFilter()
	{
		setDefaultParameters();
	}

	Data::SafeImagePointer BilateralFilter::apply(const Data::SafeImage& image)
	{
		QImage::Format initialFormat = image.getQImage().format();
		QImage srcImage              = image.prepareForProcessing();
		QImage resImage              = QImage(srcImage.width(), srcImage.height(), srcImage.format())/*srcImage.copy()*/;

		int colsCount = srcImage.bytesPerLine() / 4;
		int rowsCount = srcImage.byteCount() / srcImage.bytesPerLine();

		// Reinterpret cast is valid here, bits return uchar*, QRgb is actually unsigned int
		QRgb* srcImageData = reinterpret_cast< QRgb* >(srcImage.bits());
		QRgb* resImageData = reinterpret_cast< QRgb* >(resImage.bits());

		float invSigmaSpatial = 1.0f / (2.0f * mSigmaSpatial * mSigmaSpatial);
		float invSigmaRadiometric  = 1.0f / (2.0f * mSigmaRadiometric * mSigmaRadiometric);

		int x, y, mx, my, indexPrecalc;
		float dx, dy;
		float distSpat, wSpat, w;
		float spatialPrecalcValues[81];

		// Calc pre spatial array
		x = 12;
		y = 12;
		indexPrecalc = 0;
		for (my = y - mRadius; my < y + mRadius; my++)
		{
			for (mx = x - mRadius; mx < x + mRadius; mx++)
			{
				dx = (float)(mx - x);
				dy = (float)(my - y);
				distSpat  = sqrt(dx * dx + dy * dy);
				wSpat     = 1.0f / exp( distSpat * invSigmaSpatial );
				spatialPrecalcValues[indexPrecalc] = wSpat;
				indexPrecalc++;
			}
		}

		// Main filter processing loop
		for (int y = 0; y < rowsCount; ++y)
		{
			for (int x = 0; x < colsCount; ++x)
			{
				// Current pixel index
				int index = y * colsCount + x;

				// Resulting color for each channel will be stored here
				float result[3];
				int srcAlpha = (srcImageData[index] >> 24) & 0xff;

				// Process channels separately
				for (int ch = 0; ch < 3; ++ch)
				{
					float srcColor = ((srcImageData[index] >> (16 - 8 * ch)) & 0xff) / 255.f;

					float sumTop = 0.0f;
					float sumBot = 0.0f;

					indexPrecalc = 0;

					for (int i = - mRadius; i < mRadius; ++i)
					{
						for (int j = - mRadius; j < mRadius; ++j)
						{
							int my = y + i, mx = x + j;

							// Extending edge policy
							if (mx < 0)
								mx = 0;
							else if (mx >= colsCount)
								mx = colsCount - 1;
							if (my < 0)
								my = 0;
							else if (my >= rowsCount)
								my = rowsCount - 1;

							int neighbourIndex = my * colsCount + mx;
							float neighbourColor = ((srcImageData[neighbourIndex] >> (16 - 8 * ch)) & 0xff) / 255.f;
							float distRad = (srcColor - neighbourColor) * (srcColor - neighbourColor);
							float wRad = 1.0f / exp( distRad * invSigmaRadiometric );

							wSpat = spatialPrecalcValues[indexPrecalc++];
							w = wSpat * wRad;
							sumTop += w * neighbourColor;
							sumBot += w;
						}
					}

					result[ch] = (sumTop / sumBot);
				}

				// Set resulting color of current pixel
				resImageData[index] = qRgba(result[0] * 255.f, result[1] * 255.f, result[2] * 255.f, srcAlpha);
			}
		}

		return SafeImagePointer(new SafeImage(resImage.convertToFormat(initialFormat)));
	}

	const QString& BilateralFilter::getName() const
	{
		return Constants::FilterName;
	}

	const char* BilateralFilter::getIdName() const
	{
		return Constants::FilterIdName;
	}

	void BilateralFilter::setParameters(const Data::NamedVariantMap& params)
	{
		// Radius
		auto it = params.find("Radius");
		if (it != params.end())
			mRadius = (*it).asVariant().toInt();
		// SigmaSpatial
		it = params.find("SigmaSpatial");
		if (it != params.end())
			mSigmaSpatial = (*it).asVariant().toFloat();
		// SigmaRadiometric
		it = params.find("SigmaRadiometric");
		if (it != params.end())
			mSigmaRadiometric = (*it).asVariant().toFloat();
	}

	void BilateralFilter::setDefaultParameters()
	{
		mRadius = 2;
		mSigmaSpatial = 1.0f;
		mSigmaRadiometric = 0.08f;
	}

	const Data::NamedVariantMap& BilateralFilter::getParameters() const
	{
		static Data::NamedVariantMap params;

	  params["Radius"] = Data::ExtendedVariant::toExtendedVariant(QVariant(mRadius));
		params["SigmaSpatial"] = Data::ExtendedVariant::toExtendedVariant(QVariant(mSigmaSpatial));
		params["SigmaRadiometric"] = Data::ExtendedVariant::toExtendedVariant(QVariant(mSigmaRadiometric));

		return params;
	}
} // namespace Filters