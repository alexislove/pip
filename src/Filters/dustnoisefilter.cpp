#include "DustNoiseFilter.h"
#include "../Auxiliary/utils.h"

#include <QColor>

using namespace Filters;
using namespace Data;

DustNoiseFilter::DustNoiseFilter()
{
	setDefaultParameters();
}

Data::SafeImagePointer DustNoiseFilter::apply(const Data::SafeImage& image)
{
	QImage::Format initialFormat = image.getQImage().format();
	QImage srcImage              = image.prepareForProcessing();
	QImage resImage              = srcImage.copy();

	int colsCount = srcImage.bytesPerLine() / 4;
	int rowsCount = srcImage.byteCount() / srcImage.bytesPerLine();

	// Reinterpret cast is valid here, bits return uchar*, QRgb is actually unsigned int
	QRgb* srcImageData = reinterpret_cast< QRgb* >(srcImage.bits());
	QRgb* resImageData = reinterpret_cast< QRgb* >(resImage.bits());


	// Main filter processing loop
	for (int y = 0; y < rowsCount; ++y)
	{
		for (int x = 0; x < colsCount; ++x)
		{ 
			const float scale = 10.0f;
			float noiseAddition;

			// Probability pass test
			if (rand() / (double) RAND_MAX <= mProbability)
			{
				float g = Auxiliary::gauss(mProbability);
				Q_ASSERT(g <= scale);
				noiseAddition = mMinAddition + g / scale * (1.f - mMinAddition);
			}
			else
			{
				noiseAddition = 0.f;
			}

			// Current pixel index
			int index = y * colsCount + x;
			QColor src(srcImageData[index]);

			float r, g, b;
			r = src.redF()   + noiseAddition;
			g = src.greenF() + noiseAddition;
			b = src.blueF()  + noiseAddition;

			// Set result color of current pixel
			resImageData[index] = qRgba(Auxiliary::floatToIntColor(r),
																	Auxiliary::floatToIntColor(g),
																	Auxiliary::floatToIntColor(b), src.alpha());
		}
	}

	return SafeImagePointer(new SafeImage(resImage.convertToFormat(initialFormat)));
}


const QString& DustNoiseFilter::getName() const
{
	// FIXME ?
	static const QString name("Dust noise");
	return name;
}


const char* DustNoiseFilter::getIdName() const
{
	// FIXME ?
	return "Dust noise";
}

void DustNoiseFilter::setParameters( const Data::NamedVariantMap& params )
{
	// mProbability
	auto it = params.find("Probability");
	if (it != params.end()) {
		mProbability = (*it).asVariant().toFloat();
	}

	// mMinAddition
	it = params.find("MinAddition");
	if (it != params.end()) {
		mMinAddition = (*it).asVariant().toFloat();
	}
}

void DustNoiseFilter::setDefaultParameters()
{
	mProbability = 0.1f;
	mMinAddition = 128 / 255.f;
}

const Data::NamedVariantMap& Filters::DustNoiseFilter::getParameters() const
{
	static Data::NamedVariantMap params;

	params["Probability"] = Data::ExtendedVariant::toExtendedVariant(QVariant(mProbability));
	params["MinAddition"] = Data::ExtendedVariant::toExtendedVariant(QVariant(mMinAddition));

	return params;
}
