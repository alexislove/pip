/*!
 *\file builtinfiltersfactory.h
 *\brief Factory definition for built-in filters
 */

#include "inversionfilter.h"
#include "dilationfilter.h"
#include "erosionfilter.h"
#include "tophattransformfilter.h"
#include "bottomhattransformfilter.h"
#include "linearfilter.h"
#include "medianfilter.h"
#include "whitenoisefilter.h"
#include "dustnoisefilter.h"
#include "Bilateralfilter.h"
#include "wienerfilter.h"
//! Warning: do not edit this line!!! $IncludeFilterHeader$

#include "builtinfiltersfactory.h"

using namespace Filters;
using namespace Interfaces;

namespace Filters
{
	//! Avoid copy&paste
	template <class T>
	IFilterPointer createCustomizableFilter(const Data::NamedVariantMap& params)
	{
		T *filter = new T;
		filter->setParameters(params);
		return IFilterPointer(filter);
	}

	IFilterPointer BuiltinFiltersFactory::createInversionFilter() const
	{
		return IFilterPointer(new InversionFilter);
	}

	IFilterPointer BuiltinFiltersFactory::createDilationFilter(const Data::NamedVariantMap& params/* = Data::NamedVariantMap() */) const
	{
		return createCustomizableFilter<DilationFilter>(params);
	}

	IFilterPointer BuiltinFiltersFactory::createErosionFilter(const Data::NamedVariantMap& params/* = Data::NamedVariantMap() */) const
	{
		return createCustomizableFilter<ErosionFilter>(params);
	}

	IFilterPointer BuiltinFiltersFactory::createTopHatTransformFilter(const Data::NamedVariantMap& params/* = Data::NamedVariantMap() */) const
	{
		return createCustomizableFilter<TopHatTransformFilter>(params);
	}

	IFilterPointer BuiltinFiltersFactory::createBottomHatTransformFilter(const Data::NamedVariantMap& params/* = Data::NamedVariantMap() */) const
	{
		return createCustomizableFilter<BottomHatTransformFilter>(params);
	}

	IFilterPointer BuiltinFiltersFactory::createLinearFilter(const Filters::LinearFilterDescriptor &desc, const Data::NamedVariantMap& params/* = Data::NamedVariantMap() */) const
	{
		LinearFilter *filter = new LinearFilter(desc);
		filter->setParameters(params);
		return IFilterPointer(filter);
	}

	IFilterPointer BuiltinFiltersFactory::createMedianFilter(const Data::NamedVariantMap& params/* = Data::NamedVariantMap() */) const	
	{
		return createCustomizableFilter<MedianFilter>(params);
	}

	IFilterPointer BuiltinFiltersFactory::createWhiteNoiseFilter(const Data::NamedVariantMap& params/* = Data::NamedVariantMap() */) const
	{
		return createCustomizableFilter<WhiteNoiseFilter>(params);
	}

	IFilterPointer BuiltinFiltersFactory::createDustNoiseFilter(const Data::NamedVariantMap& params/* = Data::NamedVariantMap() */) const
	{
		return createCustomizableFilter<DustNoiseFilter>(params);
	}

	IFilterPointer BuiltinFiltersFactory::createBilateralFilter(const Data::NamedVariantMap& params/* = Data::NamedVariantMap() */) const
	{
		return createCustomizableFilter<BilateralFilter>(params);
	}

	IFilterPointer BuiltinFiltersFactory::createWienerFilter(const Data::NamedVariantMap& params/* = Data::NamedVariantMap() */) const
	{
		return createCustomizableFilter<WienerFilter>(params);
	}

	//! Warning: do not edit this line!!! $FactoryMethodImpl$
}