/*!
 *\file tophattransformfilter.cpp
 *\brief Top-hat transform filter definition
 */

#include <QApplication>

#include "tophattransformfilter.h"
#include "erosionfilter.h"
#include "dilationfilter.h"
#include "../Tools/goldenimagecomparator.h"

using namespace Data;

namespace Filters
{
	namespace Constants
	{
		static const QString FilterName(QApplication::translate("TopHatTransformFilter", "Top-hat transform"));
		static const char   *FilterIdName = "TopHatTransform";
	} // namespace Constants

	SafeImagePointer TopHatTransformFilter::apply(const SafeImage& image)
	{
		ErosionFilter erosionFilter;
		SafeImagePointer erodedImage = erosionFilter.apply(image);

		DilationFilter dilationFilter;
		SafeImagePointer openedImage = dilationFilter.apply(*erodedImage);

		QImage::Format initialFormat = image.getQImage().format();
		QImage srcImage              = image.prepareForProcessing();
		QImage subtructedImage       = openedImage->prepareForProcessing();
		QImage resImage;

		Tools::GoldenImageComparator comparator;
		bool ok = true;
		comparator.compare(srcImage, subtructedImage, &resImage, &ok);

		return SafeImagePointer(new SafeImage(resImage.convertToFormat(initialFormat)));
	}

	const QString& TopHatTransformFilter::getName() const
	{
		return Constants::FilterName;
	}

	const char* TopHatTransformFilter::getIdName() const
	{
		return Constants::FilterIdName;
	}

	void TopHatTransformFilter::setParameters(const Data::NamedVariantMap& params)
	{
		
	}

	void TopHatTransformFilter::setDefaultParameters()
	{
		
	}

	const Data::NamedVariantMap& TopHatTransformFilter::getParameters() const
	{
		Q_ASSERT(! "This method is not implemented!");

		static Data::NamedVariantMap params;
		return params;
	}
} // namespace Filters