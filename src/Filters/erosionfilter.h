/*!
 *\file erosionfilter.h
 *\brief Erosion filter definition
 */

#ifndef FILTER_EROSIONFILTER_H
	#define FILTER_EROSIONFILTER_H

	#include "../Interfaces/icustomizable.h"
	#include "../Interfaces/ifilter.h"

	namespace Filters
	{
    class ErosionFilter : public Interfaces::IFilter,
													public Interfaces::ICustomizable
    {
    public:
			/*!
			 *\brief Destructor
			 */
      virtual ~ErosionFilter() 
      {
      }

			//! See IFilter.apply(const SafeImage&)
			virtual Data::SafeImagePointer apply(const Data::SafeImage& image);

			//! See IFilter.getName()
			virtual const QString& getName() const;

			//! See IFilter.getIdName()
			virtual const char* getIdName() const;

			//! See ICustomizable.setParameters(const Data::NamedVariantMap&)
			virtual void setParameters(const Data::NamedVariantMap& params);

			//! See ICustomizable.setDefaultParameters()
			virtual void setDefaultParameters();

			virtual const Data::NamedVariantMap& getParameters() const;

		};
  } // namespace Filters
#endif // FILTER_EROSIONFILTER_H