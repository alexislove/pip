/*!
 *\file medianfilter.h
 *\brief Median filter definition
 */

#ifndef FILTER_MEDIANFILTER_H
	#define FILTER_MEDIANFILTER_H

	#include "../Interfaces/icustomizable.h"
	#include "../Interfaces/ifilter.h"

	namespace Filters
	{
		class MedianFilter : public Interfaces::IFilter,
												 public Interfaces::ICustomizable
		{
		public:
			/*!
			 *\brief Constructor
			 */
			MedianFilter();

			/*!
			 *\brief Destructor
			 */
			virtual ~MedianFilter() 
			{
			}

			//! See IFilter.apply(const SafeImage&)
			virtual Data::SafeImagePointer apply(const Data::SafeImage& image);

			//! See IFilter.getName()
			virtual const QString& getName() const;

			//! See IFilter.getIdName()
			virtual const char* getIdName() const;

			//! See ICustomizable.setParameters(const Data::NamedVariantMap&)
			virtual void setParameters(const Data::NamedVariantMap& params);

			//! See ICustomizable.setDefaultParameters()
			virtual void setDefaultParameters();

			//! See ICustomizable.getParameters()
			virtual const Data::NamedVariantMap& getParameters() const;

			enum FilterType {
				Normal = 0,
				Hybrid = 1
			};

			enum BorderPolicy {
				ExtendEdge = 0,
				Toroidal = 1,
				//AddZeros = 2
			};

		private:
			int mDimension;
			int mWindowHalfSize;
			FilterType mType;
			BorderPolicy mBorderPolicy;

		private:
			void setDimension(int dim);
			Data::SafeImagePointer applyNormal(const Data::SafeImage& image);
			Data::SafeImagePointer applyHybrid(const Data::SafeImage& image);
		};
	}

#endif // FILTER_MEDIANFILTER_H