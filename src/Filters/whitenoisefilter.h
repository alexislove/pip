/*!
 *\file whitenoisefilter.h
 *\brief White noise filter definition
 */

#ifndef FILTER_WHITENOISEFILTER_H
#define FILTER_WHITENOISEFILTER_H

#include "../Interfaces/icustomizable.h"
#include "../Interfaces/ifilter.h"

#include <QString>
#include <QVector>

namespace Filters
{

	 class WhiteNoiseFilter : public Interfaces::IFilter,
														public Interfaces::ICustomizable
	 {
	 public:
			/*!
			*\brief Constructor
			*/
			WhiteNoiseFilter();

			/*!
			*\brief Destructor
			*/
			virtual ~WhiteNoiseFilter() 
			{
			}

			//! See IFilter.apply(const SafeImage&)
			virtual Data::SafeImagePointer apply(const Data::SafeImage& image);

			//! See IFilter.getName()
			virtual const QString& getName() const;

			//! See IFilter.getIdName()
			virtual const char* getIdName() const;

			//! See ICustomizable.setParameters(const Data::NamedVariantMap&)
			virtual void setParameters(const Data::NamedVariantMap& params);

			//! See ICustomizable.setDefaultParameters()
			virtual void setDefaultParameters();

			//! See ICustomizable.getParameters()
			virtual const Data::NamedVariantMap& getParameters() const;

	 private:
			float mProbability;
			float	mMaxDisplacement;
	 };
}

#endif // FILTER_WHITENOISEFILTER_H