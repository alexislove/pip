/*!
 *\file wienerfilter.cpp
 *\brief Wiener filter definition
 */

#include <QApplication>
#include <QDebug>

#include "wienerfilter.h"
#include "../Data/colormodelconversions.h"
#include "../Auxiliary/utils.h"

using namespace Data;
using namespace Auxiliary;

namespace Filters
{
	namespace Constants
	{
		static const QString FilterName(QApplication::translate("WienerFilter", "Wiener"));
		static const char   *FilterIdName = "Wiener";
	} // namespace Constants

	WienerFilter::WienerFilter()
	{
		setDefaultParameters();
	}

	inline float norm(int radius)
	{
		return (2 * radius) * (2 * radius);
	}

	void WienerFilter::buildAverage(uchar *Isrc, uchar *Idst, int colsCount, int rowsCount)
	{
		int avg[3];

		for (int y = 0; y < rowsCount; ++y)
		{
			for (int x = 0; x < colsCount; ++x)
			{
				avg[0] = avg[1] = avg[2] = 0;

				for (int i = - mRadius; i < mRadius; ++i)
				{
					for (int j = - mRadius; j < mRadius; ++j)
					{
						int my = y + i, mx = x + j;

						// Extending edge policy
						if (mx < 0)
							mx = 0;
						else if (mx >= colsCount)
							mx = colsCount - 1;
						if (my < 0)
							my = 0;
						else if (my >= rowsCount)
							my = rowsCount - 1;

						int neighbourIndex = (my * colsCount + mx) * 4;
						avg[0] += Isrc[neighbourIndex + IDX_CHAN1];
						avg[1] += Isrc[neighbourIndex + IDX_CHAN2];
						avg[2] += Isrc[neighbourIndex + IDX_CHAN3];
					}
				}

				avg[0] /= (float)( norm(mRadius) );
				avg[1] /= (float)( norm(mRadius) );
				avg[2] /= (float)( norm(mRadius) );

				int index = (y * colsCount + x) * 4;
				Idst[index + IDX_CHAN1] = avg[0];
				Idst[index + IDX_CHAN2] = avg[1];
				Idst[index + IDX_CHAN3] = avg[2];
				Idst[index + IDX_ALPHA] = Isrc[index + IDX_ALPHA];
			}
		}
	}

	void WienerFilter::buildSigma2(uchar *Isrc, uchar *Iavg, uchar *Idst, int colsCount, int rowsCount)
	{
		int avg[3];
		int center[3];		// Center pixel color

		int max = 0;

		for (int y = 0; y < rowsCount; ++y)
		{
			for (int x = 0; x < colsCount; ++x)
			{
				int index = (y * colsCount + x) * 4;
				center[0] = Iavg[index + IDX_CHAN1];
				center[1] = Iavg[index + IDX_CHAN2];
				center[2] = Iavg[index + IDX_CHAN3];

				avg[0] = avg[1] = avg[2] = 0;

				for (int i = - mRadius; i < mRadius; ++i)
				{
					for (int j = - mRadius; j < mRadius; ++j)
					{
						int my = y + i, mx = x + j;

						// Extending edge policy
						if (mx < 0)
							mx = 0;
						else if (mx >= colsCount)
							mx = colsCount - 1;
						if (my < 0)
							my = 0;
						else if (my >= rowsCount)
							my = rowsCount - 1;

						int neighbourIndex = (my * colsCount + mx) * 4;

						int cur[3], diff[3];
						cur[0] = Isrc[neighbourIndex + IDX_CHAN1];
						cur[1] = Isrc[neighbourIndex + IDX_CHAN2];
						cur[2] = Isrc[neighbourIndex + IDX_CHAN3];

						diff[0] = cur[0] - center[0];
						diff[1] = cur[1] - center[1];
						diff[2] = cur[2] - center[2];

						avg[0] += diff[0] * diff[0];
						avg[1] += diff[1] * diff[1];
						avg[2] += diff[2] * diff[2];
					}
				}

				// Map values to (0, 255)
				avg[0] /= 50.f * ( norm(mRadius) );
				avg[1] /= 50.f * ( norm(mRadius) );
				avg[2] /= 50.f * ( norm(mRadius) );

				if (avg[0] > max)
					max = avg[0];
				if (avg[1] > max)
					max = avg[1];
				if (avg[2] > max)
					max = avg[2];
				
				Idst[index + IDX_CHAN1] = avg[0];
				Idst[index + IDX_CHAN2] = avg[1];
				Idst[index + IDX_CHAN3] = avg[2];
				Idst[index + IDX_ALPHA] = Isrc[index + IDX_ALPHA];
			}
		}

		//qDebug() << "Max sigma: " << max;
	}

	void WienerFilter::buildWiener(uchar *Isrc, uchar *Iavg, uchar *Isigma2, uchar *Idst, int colsCount, int rowsCount)
	{
		int avg[3];
		int sigma2[3];
		float realSigma, ss2;

		realSigma = mSigma * 5.f;// * 1024.0f * 10.0f;
		ss2 = realSigma * realSigma;

		for (int y = 0; y < rowsCount; ++y)
		{
			for (int x = 0; x < colsCount; ++x)
			{
				int index = (y * colsCount + x) * 4;
				int i1 = index + IDX_CHAN1;
				int i2 = index + IDX_CHAN2;
				int i3 = index + IDX_CHAN3;

				sigma2[0] = Isigma2[i1];
				sigma2[1] = Isigma2[i2];
				sigma2[2] = Isigma2[i3];

				sigma2[0] -= ss2;
				sigma2[1] -= ss2;
				sigma2[2] -= ss2;

				sigma2[0] = clamp(sigma2[0], 0, 1000);
				sigma2[1] = clamp(sigma2[1], 0, 1000);
				sigma2[2] = clamp(sigma2[2], 0, 1000);


				Idst[i1] = (int)(Iavg[i1] + (Isrc[i1] - Iavg[i1]) * ( sigma2[0] / (sigma2[0] + ss2) ) );
				Idst[i2] = (int)(Iavg[i2] + (Isrc[i2] - Iavg[i2]) * ( sigma2[1] / (sigma2[1] + ss2) ) );
				Idst[i3] = (int)(Iavg[i3] + (Isrc[i3] - Iavg[i3]) * ( sigma2[2] / (sigma2[2] + ss2) ) );
				Idst[index + IDX_ALPHA] = Isrc[index + IDX_ALPHA];
			}
		}
	}

	Data::SafeImagePointer WienerFilter::apply(const Data::SafeImage& image)
	{
		QImage::Format initialFormat = image.getQImage().format();
		QImage srcImage              = image.prepareForProcessing();
		QImage avgImage              = QImage(srcImage.width(), srcImage.height(), srcImage.format());
		QImage sigma2Image           = QImage(srcImage.width(), srcImage.height(), srcImage.format());

		int colsCount = srcImage.bytesPerLine() / 4;
		int rowsCount = srcImage.byteCount() / srcImage.bytesPerLine();

		buildAverage(srcImage.bits(), avgImage.bits(), colsCount, rowsCount);
		buildSigma2(srcImage.bits(), avgImage.bits(), sigma2Image.bits(), colsCount, rowsCount);
		buildWiener(srcImage.bits(), avgImage.bits(), sigma2Image.bits(), srcImage.bits(), colsCount, rowsCount);

		return SafeImagePointer(new SafeImage(srcImage.convertToFormat(initialFormat)));

	}

	const QString& WienerFilter::getName() const
	{
		return Constants::FilterName;
	}

	const char* WienerFilter::getIdName() const
	{
		return Constants::FilterIdName;
	}

	void WienerFilter::setParameters(const Data::NamedVariantMap& params)
	{
		// Radius
		auto it = params.find("radius");
		if (it != params.end())
			mRadius = (*it).asVariant().toInt();
		// Sigma
		it = params.find("sigma");
		if (it != params.end())
			mSigma = (*it).asVariant().toFloat();
	}

	void WienerFilter::setDefaultParameters()
	{
	  mRadius = 3;
		mSigma = 0.3f;
	}

	const Data::NamedVariantMap& WienerFilter::getParameters() const
	{
		static Data::NamedVariantMap params;

	  params["radius"] = Data::ExtendedVariant::toExtendedVariant(QVariant(mRadius));
		params["sigma"] = Data::ExtendedVariant::toExtendedVariant(QVariant(mSigma));

		return params;
	}
} // namespace Filters