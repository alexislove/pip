/*!
 *\file wienerfilter.h
 *\brief Wiener filter definition
 */

#ifndef FILTER_WIENERFILTER_H
	#define FILTER_WIENERFILTER_H

	#include "../Interfaces/icustomizable.h"
	#include "../Interfaces/ifilter.h"


	namespace Filters
	{
		class WienerFilter : public Interfaces::IFilter,
														public Interfaces::ICustomizable
		{
		public:
			/*!
			 *\brief Constructor
			 */
			WienerFilter();

			/*!
			 *\brief Destructor
			 */
			virtual ~WienerFilter() 
			{
			}

			//! See IFilter.apply(const SafeImage&)
			virtual Data::SafeImagePointer apply(const Data::SafeImage& image);

			//! See IFilter.getName()
			virtual const QString& getName() const;

			//! See IFilter.getIdName()
			virtual const char* getIdName() const;

			//! See ICustomizable.setParameters(const Data::NamedVariantMap&)
			virtual void setParameters(const Data::NamedVariantMap& params);

			//! See ICustomizable.setDefaultParameters()
			virtual void setDefaultParameters();

			//! See ICustomizable.getParameters()
			virtual const Data::NamedVariantMap& getParameters() const;

		private:
			void buildAverage(uchar *src, uchar *dst, int colsCount, int rowsCount);
			void buildSigma2(uchar *src, uchar *ave, uchar *dst, int colsCount, int rowsCount);
			void buildWiener(uchar *Isrc, uchar *Iavg, uchar *Isigma2, uchar *Idst, int colsCount, int rowsCount);

		private:
			int mRadius;
			float mSigma;
		};
	}

#endif // FILTER_WIENERFILTER_H