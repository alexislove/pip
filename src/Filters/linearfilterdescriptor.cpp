/*!
 *\file linearfilterdescriptor.cpp
 *\brief Descriptor for linear filter serializable capabilities implementation
 */

#include <QIODevice>
#include <QStringList>

#include "linearfilterdescriptor.h"

namespace Filters
{
	void LinearFilterDescriptor::read(QIODevice* device)
	{
		// Read dimensions
		int N, M;

		QString current = device->readLine();

		QStringList dims = current.split(" ", QString::SkipEmptyParts);
		if (dims.size() != 2) 
		{
			return;
		}

		N = dims[0].toInt();
		M = dims[1].toInt();

		// Allocate matrix for desired dimensions
		Matrix.resize(N);
		for (int row = 0; row < N; ++row)
		{
			Matrix[row].resize(M);
		}

		// Read matrix for descriptor
		for (int row = 0; row < N; ++row)
		{
			current = device->readLine();
			if (current.isEmpty() || current.startsWith("\n"))
			{
				--row; // Don't update row, because of the empty one
				continue;
			}
			QStringList elements = current.split(QRegExp("\\s+"), QString::SkipEmptyParts);
			if (elements.size() != M)
			{
				return;
			}

			for (int col = 0; col < M; ++col)
			{
				float currentMatrixValue;

				// Process rational fraction or even expression like 4.0/5*3.14*2/16.0
				if (elements[col].contains('/') || elements[col].contains('*'))
				{
				  QRegExp operation("([/*])");
					QRegExp number("[0-9.]");

					QStringList operands = elements[col].split(operation, QString::SkipEmptyParts);
					QStringList operators = elements[col].split(number, QString::SkipEmptyParts);

					// FIXME: such solution should be faster, but i dunno why it's not working
					//int pos = operation.indexIn(elements[col]);
					//QStringList operators = operation.capturedTexts();
					//int i1 = operands.size(), i2 = operators.size();

					Q_ASSERT(operands.size() == operators.size() + 1);

					currentMatrixValue = operands[0].toFloat();

					for (int i = 0; i < operators.size(); ++i)
					{
						if (operators[i] == "*")
						{
							currentMatrixValue *= operands[i + 1].toFloat();
						}
						else if (operators[i] == "/")
						{
							currentMatrixValue /= operands[i + 1].toFloat();
						}
					}
				}
				// Single floating point number element
				else
				{
					currentMatrixValue = elements[col].toFloat();
				}

				Matrix[row][col] = currentMatrixValue;
			}
		}

		// Read denominator
		current = device->readLine();
		Denominator = current.toFloat();

		// Read name
		Name = device->readLine();
	}

	void LinearFilterDescriptor::write(QIODevice* device) const
	{
		// Don't write anything, if matrix is empty
		if (Matrix.isEmpty())
		{
			return;
		}
		// Write matrix dimensions
		device->write(QString("%1 %2\n").arg(Matrix.size()).arg(Matrix[0].size()).toUtf8());
		for (int row = 0, rowsCount = Matrix.size(); row < rowsCount; ++row)
		{
			for (int col = 0, colsCount = Matrix[row].size(); col < colsCount; ++col)
			{
				device->write(QString::number(Matrix[row][col]).toUtf8());
				if (col < colsCount - 1)
					device->write(" ");
			}
			device->write("\n");
		}
		// Write denominator
		device->write((QString::number(Denominator) + "\n").toUtf8());
		// Write name
		device->write(Name.toUtf8());
	}
} // namespace Filters
