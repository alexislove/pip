/*!
 *\file linearfilterdescriptor.h
 *\brief Descriptor for linear filter definition
 */

#ifndef FILTERS_LINEARFILTERDESCRIPTOR_H
	#define FILTERS_LINEARFILTERDESCRIPTOR_H

	#include <QString>
	#include <QVector>

	#include "../Interfaces/iserializable.h"

	namespace Filters
	{
		//! Filter matrix definition
		typedef QVector<QVector<float>> FilterMatrix;

		/*!
		 *\class LinearFilterDescriptor
		 *\brief Descriptor for linear filter, containing filter matrix, denominator and name of the filter
		 *\			 Descriptor implements serializable interface, providing capabilities to be loaded or saved using QIODevice
		 *\			 Descriptor file has naive and dummy format:
		 *\			    First line contains dimensions of the filter - 2 positive integers (let them be N and M), separated by spaces
		 *\					Next N lines contains M values, separated by spaces
		 *\					Pre-last line contains denominator of the filter
		 *\					Last line contains filter name
		 *\			 Example:
		 *\			 
		 *\			 3 3
		 *\			 1 0 0 
		 *\			 0 1 0
		 *\			 0 0 1
		 *\			 1
		 *\			 MyFilter
		 *\
		 */
		struct LinearFilterDescriptor : Interfaces::ISerializable
		{
			FilterMatrix Matrix;
			float				 Denominator;
			QString			 Name;

			/*!
			 *\brief See ISerializable.read(QIODevice*)
			 */
			virtual void read(QIODevice* device);

			/*!
			 *\brief See ISerializable.write(QIODevice*) const
			 */
			virtual void write(QIODevice* device) const;
		};
	} // namespace Filters

#endif // FILTERS_LINEARFILTERDESCRIPTOR_H

