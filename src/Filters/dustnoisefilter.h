/*!
 *\file dustnoisefilter.h
 *\brief Dust noise filter definition
 */

#ifndef FILTER_DUSTNOISEFILTER_H
#define FILTER_DUSTNOISEFILTER_H

#include "../Interfaces/icustomizable.h"
#include "../Interfaces/ifilter.h"

#include <QString>
#include <QVector>

namespace Filters
{

	 class DustNoiseFilter : public Interfaces::IFilter,
													 public Interfaces::ICustomizable
	 {
	 public:
			/*!
			*\brief Constructor
			*/
			DustNoiseFilter();

			/*!
			*\brief Destructor
			*/
			virtual ~DustNoiseFilter() 
			{
			}

			//! See IFilter.apply(const SafeImage&)
			virtual Data::SafeImagePointer apply(const Data::SafeImage& image);

			//! See IFilter.getName()
			virtual const QString& getName() const;

			//! See IFilter.getIdName()
			virtual const char* getIdName() const;

			//! See ICustomizable.setParameters(const Data::NamedVariantMap&)
			virtual void setParameters(const Data::NamedVariantMap& params);

			//! See ICustomizable.setDefaultParameters()
			virtual void setDefaultParameters();

			virtual const Data::NamedVariantMap& getParameters() const;

	 private:
			float mProbability;
			float	mMinAddition;
	 };
}

#endif // FILTER_DUSTNOISEFILTER_H