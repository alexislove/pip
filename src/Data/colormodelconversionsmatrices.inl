/*!
 *\file colormodelconversions.h
 *\brief Color model conversion routines
 */

#ifndef COLOR_MODEL_CONVERSIONS_MATRICES_H
	#define COLOR_MODEL_CONVERSIONS_MATRICES_H

	#define SHIFT_YUV_Y 16
	#define SHIFT_YUV_U 128
	#define SHIFT_YUV_V 128


	#define SHIFT_YCBCR_Y		0
	#define SHIFT_YCBCR_CB	128
	#define SHIFT_YCBCR_CR	128

	const double MatrixRGBToYCbCr [][3][3] =
	{
		// Dummy : Index 0 : don't use => 0
		{
			{0.0,0.0,0.0},
			{0.0,0.0,0.0},
			{0.0,0.0,0.0}
		},

		// 1
		{
			{0.2990		,  0.5870  ,  0.1140},
			{-0.1687  , -0.3313  ,  0.5000},
			{ 0.5000	, -0.4187  , -0.0813}
		}
	};

	const double MatrixYCbCrToRGB [][3][3] =
	{
		// Dummy : Index 0 : don't use => 0
		{
			{0.0,0.0,0.0},
			{0.0,0.0,0.0},
			{0.0,0.0,0.0}
		},

		// 1
		{
			{1.0	,  0.0		 ,  1.40200},
			{1.0	, -0.34414 , -0.71414},
			{1.0	,  1.77200 ,  0.0}
		}
	};



#endif // COLOR_MODEL_CONVERSIONS_MATRICES_H