/*!
 *\file colormodelconversions.cpp
 *\brief Color model conversion routines implementation
 */

#include "colormodelconversions.h"
#include "colormodelconversionsmatrices.inl"
#include "..\Auxiliary\utils.h"

using namespace Auxiliary;

namespace Data {

	typedef unsigned char uchar;

	void RGBtoYUV(uchar *in, uchar *out, int size, int channels)
	{
		for (int i = 0; i < size * channels; i += channels)
		{
			uchar r = in[i + IDX_CHAN1];
			uchar g = in[i + IDX_CHAN2];
			uchar b = in[i + IDX_CHAN3];
			uchar a = in[i + IDX_ALPHA];

			int y =  (0.257 * r) + (0.504 * g) + (0.098 * b) + SHIFT_YUV_Y;
			int u = -(0.148 * r) - (0.291 * g) + (0.439 * b) + SHIFT_YUV_U;
			int v =  (0.439 * r) - (0.368 * g) - (0.071 * b) + SHIFT_YUV_V;			

			y = clamp(y, 0, 255);
			u = clamp(u, 0, 255);
			v = clamp(v, 0, 255);

			out[i + IDX_CHAN1] = uchar(y);
			out[i + IDX_CHAN2] = uchar(u);
			out[i + IDX_CHAN3] = uchar(v);
			out[i + IDX_ALPHA] = a;
		}
	}

	void YUVtoRGB(uchar *in, uchar *out, int size, int channels)
	{
		for (int i = 0; i < size * channels; i += channels)
		{
			uchar a = in[i + IDX_ALPHA];
			// May go negative
			int y = in[i + IDX_CHAN1] - SHIFT_YUV_Y;
			int u = in[i + IDX_CHAN2] - SHIFT_YUV_U;
			int v = in[i + IDX_CHAN3] - SHIFT_YUV_V;

			//float r = 1 * y + 0 * u + 1.13983 * v;
			//float g = 1 * y + -0.39465 * u + -0.58060 * v;
			//float b = 1 * y + 2.03211 * u + 0 * v;

			// FIXME: this forward-inverse transformation gives ~0.45 percent error
			int r = 1.164 * y							+ 1.596 * v;
			int g = 1.164 * y - 0.391 * u - 0.813 * v;
			int b = 1.164 * y + 2.018 * u;

			r = clamp(r, 0, 255);
			g = clamp(g, 0, 255);
			b = clamp(b, 0, 255);

			out[i + IDX_ALPHA] = a;
			out[i + IDX_CHAN1] = uchar(r);
			out[i + IDX_CHAN2] = uchar(g);
			out[i + IDX_CHAN3] = uchar(b);
		}
	}

	void RGBtoHSV(uchar *in, uchar *out, int size, int channels)
	{
		for (int i = 0; i < size * channels; i += channels)
		{
			uchar a = in[i + IDX_ALPHA];
			float r = in[i + IDX_CHAN1] / 255.f;
			float g = in[i + IDX_CHAN2] / 255.f;
			float b = in[i + IDX_CHAN3] / 255.f;

			// Algorithm definition:
			// http://en.wikipedia.org/wiki/HSV_color_space#Hue_and_chroma
			// http://en.literateprograms.org/RGB_to_HSV_color_space_conversion_%28C%29
			float M = max(r, g, b);
			float m = min(r, g, b);
			float C = M - m;
			float H, S, V;

			// Black-gray-white
			// C == 0
			if (abs(M - m) < 1e-5)
			{
				out[i + IDX_ALPHA] = a;
				out[i + IDX_CHAN1] = 0;
				out[i + IDX_CHAN2] = 0;
				out[i + IDX_CHAN3] = m * 255.f;
				continue;
			}

			//float d, h;
			//if (m == R) {
			//	d = g - b;
			//	h = 3;
			//}
			//else if (m == G) {
			//	d = b - r;
			//	h = 5;
			//}
			//else if (m == B) {
			//	d = r - g;
			//	h = 1;
			//}

			//H = 60 * (h - d) / C;
			
			// Hue [0, 360]
			if (abs(m - r) < 1e-5)
			{
				H = 0.0 + 60.0*(g - b);
				if (H < 0.0)
					H += 360.0;
			}
			else if (abs(m - g) < 1e-5)
			{
				H = 120.0 + 60.0*(b - r);
			}
			else
			{
				H = 240.0 + 60.0*(r - g);
			}

			// Value
			V = M;

			// Saturation
			S = C / V;

			out[i + IDX_ALPHA] = a;
			// FIXME: HACK :)
			out[i + IDX_CHAN1] = clamp(int((255 - H / 360.f * 255.f) * 2.0), 0, 255);
			out[i + IDX_CHAN2] = S * 255.f;
			out[i + IDX_CHAN3] = V * 255.f;
		}
	}

	void RGBtoYCbCr(uchar *in, uchar *out, int size, int channels)
	{
		const int ColorModelIndex = 1;

		for (int i = 0; i < size * channels; i += channels)
		{
			uchar a = in[i + IDX_ALPHA];
			uchar r = in[i + IDX_CHAN1];
			uchar g = in[i + IDX_CHAN2];
			uchar b = in[i + IDX_CHAN3];

			int y  =	MatrixRGBToYCbCr[ColorModelIndex][0][0] * r +
								MatrixRGBToYCbCr[ColorModelIndex][0][1] * g +
								MatrixRGBToYCbCr[ColorModelIndex][0][2] * b + SHIFT_YUV_Y;
			int cb =	MatrixRGBToYCbCr[ColorModelIndex][1][0] * r +
								MatrixRGBToYCbCr[ColorModelIndex][1][1] * g +
								MatrixRGBToYCbCr[ColorModelIndex][1][2] * b + SHIFT_YCBCR_CB;
			int cr =	MatrixRGBToYCbCr[ColorModelIndex][2][0] * r +
								MatrixRGBToYCbCr[ColorModelIndex][2][1] * g +
								MatrixRGBToYCbCr[ColorModelIndex][2][2] * b + SHIFT_YCBCR_CR;

			y  = clamp(y,  0, 255);
			cb = clamp(cb, 0, 255);
			cr = clamp(cr, 0, 255);

			out[i + IDX_ALPHA] = a;
			out[i + IDX_CHAN1] = y;
			out[i + IDX_CHAN2] = cb;
			out[i + IDX_CHAN3] = cr;
		}
	}

	void YCbCrtoRGB(uchar *in, uchar *out, int size, int channels)
	{
		const int ColorModelIndex = 1;

		for (int i = 0; i < size * channels; i += channels)
		{
			uchar a  = in[i + 0];
			int y  = in[i + IDX_CHAN1] - SHIFT_YCBCR_Y;
			int cb = in[i + IDX_CHAN2] - SHIFT_YCBCR_CB;
			int cr = in[i + IDX_CHAN3] - SHIFT_YCBCR_CR;

			int r  =	MatrixYCbCrToRGB[ColorModelIndex][0][0] * y +
								MatrixYCbCrToRGB[ColorModelIndex][0][1] * cb +
								MatrixYCbCrToRGB[ColorModelIndex][0][2] * cr;
			int g =		MatrixYCbCrToRGB[ColorModelIndex][1][0] * y +
								MatrixYCbCrToRGB[ColorModelIndex][1][1] * cb +
								MatrixYCbCrToRGB[ColorModelIndex][1][2] * cr;
			int b =		MatrixYCbCrToRGB[ColorModelIndex][2][0] * y +
								MatrixYCbCrToRGB[ColorModelIndex][2][1] * cb +
								MatrixYCbCrToRGB[ColorModelIndex][2][2] * cr;

			r = clamp(r, 0, 255);
			g = clamp(g, 0, 255);
			b = clamp(b, 0, 255);

			out[i + IDX_ALPHA] = a;
			out[i + IDX_CHAN1] = r;
			out[i + IDX_CHAN2] = g;
			out[i + IDX_CHAN3] = b;
		}
	}

	void extractChannel( uchar *in, uchar *out, int size, int channels /*= 4*/, bool channel1 /*= true*/, bool channel2 /*= true*/, bool channel3 /*= true*/ )
	{
		for (int i = 0; i < size * channels; i += channels)
		{
			uchar a = in[i + IDX_ALPHA];
			uchar r = in[i + IDX_CHAN1];
			uchar g = in[i + IDX_CHAN2];
			uchar b = in[i + IDX_CHAN3];

			out[i + 3] = a;
			
			if (channel1 && !channel2 && !channel3)
			{
				out[i + IDX_CHAN1] = r;
				out[i + IDX_CHAN2] = r;
				out[i + IDX_CHAN3] = r;
			}
			else if (!channel1 && channel2 && !channel3)
			{
				out[i + IDX_CHAN1] = g;
				out[i + IDX_CHAN2] = g;
				out[i + IDX_CHAN3] = g;
			}
			else if (!channel1 && !channel2 && channel3)
			{
				out[i + IDX_CHAN1] = b;
				out[i + IDX_CHAN2] = b;
				out[i + IDX_CHAN3] = b;
			}
			//else if (channel1 && channel2 && !channel3) {
			//	out[i + 1] = bytes[1];
			//	out[i + 2] = bytes[2];
			//	out[i + 3] = 0;
			//}

			//else if (channel1 && !channel2 && channel3) {
			//	out[i + 1] = bytes[1];
			//	out[i + 2] = 0;
			//	out[i + 3] = bytes[3];
			//}

			//else if (!channel1 && channel2 && channel3) {
			//	out[i + 1] = 0;
			//	out[i + 2] = bytes[2];
			//	out[i + 3] = bytes[3];
			//}

			//else if (!channel1 && !channel2 && !channel3) {
			//	out[i + 1] = 0;
			//	out[i + 2] = 0;
			//	out[i + 3] = 0;
			//}

			//else {
			//	out[i + 1] = 255;
			//	out[i + 2] = 255;
			//	out[i + 3] = 255;
			//}
		}
	}

} // namespace Data