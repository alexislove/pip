/*!
 *\file namedvariantmap.h
 *\brief Named variant map definition
 */

#ifndef DATA_NAMEDVARIANTMAP_H
	#define DATA_NAMEDVARIANTMAP_H

	#include <QColor>
	#include <QList>
	#include <QMap>
	#include <QString>
	#include <QVariant>

	namespace Data
	{
		/*!
		 *\brief This class wraps QVariant with capabilites to store multiple values
		 *\			 and make architecture scalable.
		 *\			 For example, it can store color value, whenever QVariant can't
		 */
		class ExtendedVariant
		{
		public:
			//! Variant list type
			typedef QList<QVariant> VariantList;

		public:
			/*!
			 *\brief Create variant value from single QVariant
			 *\param var Desired value
			 *\return ExtendedVariant instance
			 */
			static ExtendedVariant toExtendedVariant(const QVariant& var)
			{
				return ExtendedVariant(var);
			}

			/*!
			 *\brief Create variant value from list of QVariants
			 *\param vars Desired values
			 *\return ExtendedVariant instance
			 */
			static ExtendedVariant toExtendedVariant(const VariantList& vars)
			{
				return ExtendedVariant(vars);
			}
	
		public:

			/*!
			 *\brief Default constructor
			 */
			explicit ExtendedVariant()
			{
			}

			/*!
			 *\brief Constructor from single value
			 *\param var Desired value
			 */
			explicit ExtendedVariant(const QVariant& var)
				: mValue(var)
			{

			}

			/*!
			 *\brief Constructor from multiple values
			 *\param vars Desired values
			 */
			explicit ExtendedVariant(const VariantList& vars)
				: mValues(vars)
			{

			}

			/*!
			 *\brief Check, whether this instance stores multiple values
			 *\return True, if list of variants isn't empty
			 */
			bool isList() const
			{
				return !mValues.isEmpty();
			}

			/*!
			 *\brief Fill this instance with color values
			 *\param color Desired color instance
			 *\return ExtendedVariant instance
			 */
			ExtendedVariant& fromValue(const QColor& color)
			{
				mValues.push_back(color.red()); mValues.push_back(color.green()); mValues.push_back(color.blue());
				return *this;
			}

			/*!
			 *\brief Get list of values
			 *\return Current values
			 */
			const VariantList& asVariantList() const
			{
				return mValues;
			}

			/*!
			 *\brief Get value
			 *\return Current value
			 */
			const QVariant& asVariant() const
			{
				return mValue;
			}

			/*!
			 *\brief Get color
			 *\return Color, stored in the varian
			 */
			QColor asColor() const
			{
				Q_ASSERT(mValues.size() >= 3);
				return QColor(mValues[0].toInt(), mValues[1].toInt(), mValues[2].toInt());
			}

		private:
			//! Store an array of variants or just a single one
			//! In both cases just a little memory loss
			QList< QVariant > mValues;
			QVariant					mValue;
		};

		// Useful type definitions for scalability
		typedef QString					NamedMapKey;
		typedef ExtendedVariant NamedMapValue;

		typedef QMap< NamedMapKey, NamedMapValue > NamedVariantMap;
	} // namespace Data

#endif // DATA_NAMEDVARIANTMAP_H