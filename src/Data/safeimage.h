/*!
 *\file safeimage.h
 *\brief Safe image implementation
 */

#ifndef DATA_SAFEIMAGE_H
	#define DATA_SAFEIMAGE_H

  #include "colormodelconversions.h"

	#include <QImage>
	#include <QSharedPointer>
	#include <QString>

	class QSize;



	namespace Data
	{
		/*!
		 *\class SafeImage
		 *\brief This class wraps standard QImage, providing capabilities to dump image on the hard drive
		 *\			 and than lazy load it from it, whenever dynamic memory amount is too low to hold the image itself
		 */
		class SafeImage
		{
		public:
			//! Internal uniform format used for image processing
			static const QImage::Format ProcessingFormat = QImage::Format_ARGB32_Premultiplied;

		public:
			/*!
			 *\brief Construct image from QImage instance
			 *\param image Source image
			 *\return Safe image instance
			 */
			static SafeImage fromImage(const QImage& image);
				
		public:
			/*!
			 *\brief Default constructor, see Qt documentation on QImage
			 *\			 http://qt-project.org/doc/qt-4.8/qimage.html
			 */
			SafeImage();

			/*!
			 *\brief Constructs an image with given size and format, see Qt documentation on QImage
			 *\			 http://qt-project.org/doc/qt-4.8/qimage.html
			 *\param size   Desired image size
			 *\param format Desired image format
			 */
			SafeImage(const QSize& size, QImage::Format format);

			/*!
			 *\brief Constructs an image with given dimensions and format, see Qt documentation on QImage
			 *\			 http://qt-project.org/doc/qt-4.8/qimage.html
			 *\param width  Desired width
			 *\param height Desired height
			 *\param format Desired format
			 */
			SafeImage(int width, int height, QImage::Format format);

			/*!
			 *\brief Constructs an image with given bytes of data, dimensions and format, see Qt documentation on QImage
			 *\      http://qt-project.org/doc/qt-4.8/qimage.html
			 *\param data   Byte data
			 *\param width  Desired width
			 *\param height Desired height
			 *\param format Desired format
			 */
			SafeImage(uchar* data, int width, int height, QImage::Format format);

			/*!
			 *\brief Constructs an image with given bytes of data, dimensions and format, see Qt documentation on QImage
			 *\      http://qt-project.org/doc/qt-4.8/qimage.html
			 *\param data   Const pointer on data array
			 *\param width	Desired width
			 *\param height Desired height
			 *\param format Desired format
			 */
			SafeImage(const uchar* data, int width, int height, QImage::Format format);

			/*!
			 *\brief Constructs an image with given bytes of data, dimensions, number of bytes per image line and format, see Qt documentation on QImage
			 *\      http://qt-project.org/doc/qt-4.8/qimage.html
			 *\param data					Pointer on data array
			 *\param width				Desired width
			 *\param height				Desired height
			 *\param bytesPerLine Number of bytes per image line
			 *\param format				Desired format
			 */
			SafeImage(uchar* data, int width, int height, int bytesPerLine, QImage::Format format);

			/*!
			 *\brief Constructs an image with given bytes of data, dimensions, number of bytes per image line and format, see Qt documentation on QImage
			 *\      http://qt-project.org/doc/qt-4.8/qimage.html
			 *\param data					Const pointer on data array
			 *\param width				Desired width
			 *\param height				Desired height
			 *\param bytesPerLine Number of bytes per image line
			 *\param format				Desired format
			 */
			SafeImage(const uchar* data, int width, int height, int bytesPerLine, QImage::Format format);

			/*!
			 *\brief Constructs an image from given xpm image, see Qt documentation on QImage
			 *\      http://qt-project.org/doc/qt-4.8/qimage.html
			 *\param xpm Pointer on xpm image data
			 */
			SafeImage(const char* const xpm[]);

			/*!
			 *\brief Constructs an image from given image file with given format, see Qt documentation on QImage
			 *\      http://qt-project.org/doc/qt-4.8/qimage.html
			 *\param fileName Image file name as QString
			 *\param format		Format string (defaulting to NULL)
			 */
			SafeImage(const QString& fileName, const char* format = NULL);

			/*!
			 *\brief Constructs an image from given image file with given format, see Qt documentation on QImage
			 *\      http://qt-project.org/doc/qt-4.8/qimage.html
			 *\param fileName Image file name as C-string
			 *\param format		Format string (defaulting to NULL)
			 */
			SafeImage(const char* fileName, const char* format = NULL);

			/*!
			*\brief Constructs an image from given QImage instance
			*\param image Image instance
			*/
			SafeImage(const QImage& image);

			/*!
			 *\brief Copy constructor
			 *\param image Another SafeImage instance
			 */
			SafeImage(const SafeImage& image);

			/*!
			 *\brief Assignment operator
			 *\param image Image instance to assign
			 */
			SafeImage& operator=(const SafeImage& image);

			/*!
			 *\brief Reset image
			 */
			void reset();

			/*!
			 *\brief Get reference to internal image. This method will return NULL-image, in case it was dumped on disk.
			 *\return Non-const reference to image
			 */
			QImage& getQImage()
			{
				return *mImage;
			}

			/*!
			 *\brief Get reference to internal image. This method will return NULL-image, in case it was dumped on disk.
			 *\return Const reference to image
			 */
			const QImage& getQImage() const
			{
				return *mImage;
			}

			/*!
			 *\brief Get image state, whether it's dumped or not
			 *\return Current state
			 */
			bool isDumped() 
			{
				return mIsDumped;
			}

			/*!
			 *\brief Check, whether image is null
			 *\return Current state
			 */
			bool isNull() 
			{
				return (!mImage) || (mImage->isNull());
			}

			/*!
			 *\brief Notify image to dump it's contents on the disk
			 */
			void dumpImage();

			/*!
			 *\brief Force image to be loaded from disk
			 */
			void loadImage();

			/*!
			 *\brief Prepare image for processing, converting it uniform pixel format
			 *\return Converted image
			 */
			QImage prepareForProcessing() const;

			/*!
			 *\brief Returns image in specified color mode, with specified channels included (only one of three allowed so far)
			 *\return Converted image
			 */
			QImage inColorModel(ColorModel model, bool channel1 = true, bool channel2 = true, bool channel3 = true) const;

		private:
			void prepare();

		private:
			//! Image pointer
			QSharedPointer< QImage > mImage;
			//! Dump state
			bool										 mIsDumped;
			//! File to dump name
			QString									 mDumpFile;
			//! Image dimensions
			int mWidth, mHeight;
			//! Image size in number of pixels
			int mSize;
		};

		typedef QSharedPointer< SafeImage > SafeImagePointer;
	} // namespace Data

#endif // DATA_SAFEIMAGE_H