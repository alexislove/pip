/*!
 *\file colormodelconversions.h
 *\brief Color model conversion routines
 */

#ifndef COLOR_MODEL_CONVERSIONS_H
	#define COLOR_MODEL_CONVERSIONS_H

	typedef unsigned char uchar;

	namespace Data {

		enum ColorModel {
			MODEL_SRGB = 0,
			MODEL_HSV,
			MODEL_LAB,
			MODEL_YUV,
			MODEL_YCBCR
		};

		enum ColorChannel {
			R = 0,
			G = 1,
			B = 2,
			Y = 0,	// Luminance
			U = 1,	// Chrominance 1
			V = 2		// Chrominance 2
		};

		#define IDX_CHAN1 0	
		#define IDX_CHAN2 1
		#define IDX_CHAN3 2
		#define IDX_ALPHA 3

		void RGBtoYUV(uchar *in, uchar *out, int size, int channels = 4);
		void YUVtoRGB(uchar *in, uchar *out, int size, int channels = 4);

		void RGBtoYCbCr(uchar *in, uchar *out, int size, int channels = 4);
		void YCbCrtoRGB(uchar *in, uchar *out, int size, int channels = 4);

		void RGBtoHSV(uchar *in, uchar *out, int size, int channels = 4);

		void extractChannel(uchar *in, uchar *out, int size, int channels = 4,
												 bool channel1 = true, bool channel2 = true, bool channel3 = true);


	} // namespace Data

#endif COLOR_MODEL_CONVERSIONS_H