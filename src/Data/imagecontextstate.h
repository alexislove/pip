/*!
 *\file imagecontextstate.h
 *\brief Image context state definition
 */

#ifndef DATA_IMAGECONTEXTSTATE_H
	#define DATA_IMAGECONTEXTSTATE_H

	#include <QSharedPointer>
	#include <QString>

	#include "safeimage.h"

	namespace Data
	{
		/*!
		 *\class ImageContextState
		 *\brief Image context state data collection
		 */
		struct ImageContextState
		{
			//! Image instance
			SafeImagePointer mImage;
			//! Name of current file
			QString					 mCurrentImageFile;
			//! Modifications state
			bool						 mIsDirty;
			////! Transformation: scale, translate
			//QTransform			 mTransform;
		};

		typedef QSharedPointer< ImageContextState > ImageContextStatePointer;
	} // namespace Data

#endif // DATA_IMAGECONTEXTSTATE_H