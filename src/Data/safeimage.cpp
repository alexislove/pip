/*!
 *\file safeimage.cpp
 *\brief Safe image implementation
 */

#include <qdebug.h>
#include <QCoreApplication>

#include "safeimage.h"
#include "../Auxiliary/utils.h"

using namespace Auxiliary;

namespace Data
{
	/**
	 public:
	 */
	SafeImage::SafeImage() 
		: mImage(new QImage()),
			mIsDumped(false)
	{
		prepare();
	}

	SafeImage::SafeImage(const QSize& size, QImage::Format format)
		: mImage(new QImage(size, format)),
			mIsDumped(false)
	{

	}

	SafeImage::SafeImage(int width, int height, QImage::Format format)
		: mImage(new QImage(width, height, format)),
			mIsDumped(false)
	{

	}

	SafeImage::SafeImage(uchar* data, int width, int height, QImage::Format format)
		: mImage(new QImage(data, width, height, format)),
			mIsDumped(false)
	{

	}

	SafeImage::SafeImage(const uchar* data, int width, int height, QImage::Format format)
		: mImage(new QImage(data, width, height, format)),
			mIsDumped(false)
	{

	}

	SafeImage::SafeImage(uchar* data, int width, int height, int bytesPerLine, QImage::Format format)
		: mImage(new QImage(data, width, height, bytesPerLine, format)),
			mIsDumped(false)
	{

	}

	SafeImage::SafeImage(const uchar* data, int width, int height, int bytesPerLine, QImage::Format format)
		: mImage(new QImage(data, width, height, bytesPerLine, format)),
			mIsDumped(false)
	{

	}

	SafeImage::SafeImage(const char* const xpm[])
		: mImage(new QImage(xpm)),
			mIsDumped(false)
	{

	}

	SafeImage::SafeImage(const QString& fileName, const char *format /* = NULL */)
		: mImage(new QImage(fileName, format)),
			mIsDumped(false)
	{
		prepare();
	}

	SafeImage::SafeImage(const char* fileName, const char *format /* = NULL */)
		: mImage(new QImage(fileName, format)),
			mIsDumped(false)
	{

	}

	SafeImage::SafeImage(const QImage& image)
		: mImage(new QImage(image)),
			mIsDumped(false)
	{
		prepare();
	}

	SafeImage::SafeImage(const SafeImage& image)
		: mImage(image.mImage),
			mIsDumped(image.mIsDumped),
			mDumpFile(image.mDumpFile)
	{

	}

	SafeImage& SafeImage::operator =(const SafeImage& image)
	{
		if (this == &image)
			return *this;

		mImage		= image.mImage;
		mIsDumped = image.mIsDumped;
		mDumpFile = image.mDumpFile;

		return *this;
	}

	void SafeImage::reset()
	{
		// TODO: Removed dump file, if necessary

		mImage		= QSharedPointer< QImage >();
		mIsDumped = false;
		mDumpFile = QString();
	}

	void SafeImage::dumpImage()
	{
		// [alexv:] TODO: implement me
	}

	void SafeImage::loadImage()
	{
		// [alexv:] TODO: implement me
	}

	QImage SafeImage::prepareForProcessing() const
	{
		return QImage(mImage->convertToFormat(ProcessingFormat));
	}

	QImage SafeImage::inColorModel(ColorModel model, bool channel1, bool channel2, bool channel3) const
	{
		QImage sourceImage = prepareForProcessing();
		uchar *src = sourceImage.bits();

		switch (model)
		{
		case MODEL_SRGB:
			break;
		case MODEL_YUV:
			RGBtoYUV(src, src, mSize, 4);
			//YUVtoRGB(src, src, mSize, 4);
			break;
		case MODEL_YCBCR:
			RGBtoYCbCr(src, src, mSize, 4);
			//YCbCrtoRGB(src, src, mSize, 4);
			break;
		case MODEL_HSV:
			RGBtoHSV(src, src, mSize, 4);
			break;
		default:
			qDebug() << "Wrong color model: " << model;
		}

		if (channel1 && channel2 && channel3)
			return sourceImage;

		extractChannel(src, src, mSize, 4, channel1, channel2, channel3);

		return sourceImage;
	}

	/**
	 private:
	 */
	void SafeImage::prepare()
	{
		mWidth = mImage->width();
		mHeight = mImage->height();
		mSize = mWidth * mHeight;
	}

} // namespace Data