/*!
 *\file utils.h
 *\brief Auxiliary routines
 */

#ifndef AUXILIARY_UTILS_H
	#define AUXILIARY_UTILS_H

  #include <algorithm>

	namespace Auxiliary {

		const float th = 1e-5;
		const float PI = 3.141592653589793238462643f;

		template<typename T>
		inline T clamp(T x, T a, T b)
		{
			//return std::max(std::max(x, a), std::min(x, b));
			if (x > b)
				return b;
			if (x < a)
				return a;
			return x;
		}

		inline int clampColor(int color)
		{
			if (color > 255)
				return 255;
			if (color < 0)
				return 0;
			return color;
		}

		inline float clampColor(float color)
		{
			if (std::abs(color + th) >= 1.f)
				return 1.f;
			if (color <= th)
				return 0.f;
			return color;
		}

		inline int floatToIntColor(float color)
		{
			if (std::abs(color + th) >= 1.f)
				return 255;
			if (color <= th)
				return 0;
			return static_cast<int>(color * 255);
		}

		/* Returns normal distributed random number
		 * Params: 
		 *					sigma2 - variance of normal distribution

		 * Source: http://en.wikipedia.org/wiki/Box_Muller_transform
		 */
		inline float gauss(float sigma2 = 1.f)
		{
			// u, v are independent random variables that are uniformly distributed in the interval (0, 1]
			double u, v;
			while ((u = rand () / (double) RAND_MAX) == 0.0);
			v = rand () / (double) RAND_MAX;

			/* FIXME: rounding bias here! */
			// z0 and z1 are independent random variables with a standard normal distribution
			//float z0 = sigma2 * sqrt (-2 * log (u)) * cos (2 * PI * v);
			float z1 = sigma2 * sqrt (-2 * log (u)) * sin (2 * PI * v);

			return z1;
		}

		template <typename T>
		inline T max(const T &a, const T &b)
		{
			return (a > b) ? a : b;
		}

		template <typename T>
		inline T max(const T &a, const T &b, const T &c)
		{
			return max(max(a, b), c);
		}

		template <typename T>
		inline T min(const T &a, const T &b)
		{
			return (a < b) ? a : b;
		}

		template <typename T>
		inline T min(const T &a, const T &b, const T &c)
		{
			return min(min(a, b), c);
		}

		template <typename T>
		inline T sign(T a)
		{
			return a < T(0) ? -1 : 1;
		}

		struct ivec2
		{
			ivec2() : x(0), y(0) { }
			ivec2(int v) : x(v), y(v) { }
			ivec2(int x_, int y_) : x(x_), y(y_) { }
			int x;
			int y;
			const ivec2& clamp(const ivec2 &xRange, const ivec2 &yRange)
			{
				x = Auxiliary::clamp(x, xRange.x, xRange.y);
				y = Auxiliary::clamp(y, yRange.x, yRange.y);
				return *this;
			}
		};

		inline ivec2 operator * (const ivec2 &v, int i) { return ivec2(v.x * i, v.y * i); }
		inline ivec2 operator + (const ivec2 &x, const ivec2 &y) { return ivec2(x.x + y.x, x.y + y.y); }

	} // namespace Auxiliary

#endif // AUXILIARY_UTILS_H