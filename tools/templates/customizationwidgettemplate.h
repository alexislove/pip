/*!
 *\file $name$filtercustomizationwidget.h
 *\brief This file contains definition of $NameTitle$ filter customization widget
 */

#ifndef UI_$NAME$FILTERCUSTOMIZATIONWIDGET_H
	#define UI_$NAME$FILTERCUSTOMIZATIONWIDGET_H

	#include "abstractcustomizationwidget.h"

	#include "ui_$name$filtercustomizationwidgetform.h"

	namespace Ui
	{
		/*!
		 *\class $Name$FilterCustomizationWidget
		 *\brief This class implements customization widget for $NameTitle$ filter, show over image
		 */
		class $Name$FilterCustomizationWidget : public AbstractCustomizationWidget, 
																							 private $Name$FilterCustomizationWidgetForm
		{
			Q_OBJECT
		public:
			/*!
			 *\brief Constructor
			 *\param parent Parent widget
			 */
			explicit $Name$FilterCustomizationWidget(QWidget *parent = NULL);
			
		public slots:

			//! See AbstractCustomizationWidget.setParams(const Data::NamedVariantMap&)
			virtual void setParams(const Data::NamedVariantMap& params);

		private slots:

			void onRadiusChanged(int value);

		private:
			bool mIgnoreChanges;
			Data::NamedVariantMap mParams;
		};
	}
	

#endif // UI_$NAME$FILTERCUSTOMIZATIONWIDGET_H