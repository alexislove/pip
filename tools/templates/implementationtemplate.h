/*!
 *\file $name$filter.h
 *\brief $NameTitle$ filter definition
 */

#ifndef FILTER_$NAME$FILTER_H
	#define FILTER_$NAME$FILTER_H

	#include "../Interfaces/icustomizable.h"
	#include "../Interfaces/ifilter.h"

	namespace Filters
	{
		class $Name$Filter : public Interfaces::IFilter,
														public Interfaces::ICustomizable
		{
		public:
			/*!
			 *\brief Constructor
			 */
			$Name$Filter();

			/*!
			 *\brief Destructor
			 */
			virtual ~$Name$Filter() 
			{
			}

			//! See IFilter.apply(const SafeImage&)
			virtual Data::SafeImagePointer apply(const Data::SafeImage& image);

			//! See IFilter.getName()
			virtual const QString& getName() const;

			//! See IFilter.getIdName()
			virtual const char* getIdName() const;

			//! See ICustomizable.setParameters(const Data::NamedVariantMap&)
			virtual void setParameters(const Data::NamedVariantMap& params);

			//! See ICustomizable.setDefaultParameters()
			virtual void setDefaultParameters();

			//! See ICustomizable.getParameters()
			virtual const Data::NamedVariantMap& getParameters() const;

		private:
			/*!
       *\note Feel free to place filter specific private members here, e.g.
       *\ int mRadius;
       */
		};
	}

#endif // FILTER_$NAME$FILTER_H