/*!
 *\file $name$filter.cpp
 *\brief $NameTitle$ filter definition
 */

#include <QApplication>

#include "$name$filter.h"
/*!
 *\note Uncomment for auxiliary routines
 *\ #include "../Auxiliary/utils.h"
 */

using namespace Data;

namespace Filters
{
	namespace Constants
	{
		static const QString FilterName(QApplication::translate("$Name$Filter", "$NameTitle$"));
		static const char   *FilterIdName = "$NameTitle$";
	} // namespace Constants

	$Name$Filter::$Name$Filter()
	{
		setDefaultParameters();
	}

	Data::SafeImagePointer $Name$Filter::apply(const Data::SafeImage& image)
	{
		QImage::Format initialFormat = image.getQImage().format();
		QImage srcImage              = image.prepareForProcessing();
		QImage resImage              = srcImage.copy();

		int colsCount = srcImage.bytesPerLine() / 4;
		int rowsCount = srcImage.byteCount() / srcImage.bytesPerLine();

		// Reinterpret cast is valid here, bits return uchar*, QRgb is actually unsigned int
		QRgb* srcImageData = reinterpret_cast< QRgb* >(srcImage.bits());
		QRgb* resImageData = reinterpret_cast< QRgb* >(resImage.bits());

		// Main filter processing loop
		//! You may place here algorithm description reference
		// Image inversion
		for (int y = 0; y < rowsCount; ++y)
		{
			for (int x = 0; x < colsCount; ++x)
			{
				// Current pixel index
				int index = y * colsCount + x;
				QColor src(srcImageData[index]);

				// Set resulting color of current pixel
				resImageData[index] = qRgba(255 - src.red(), 255 - src.green(), 255 - src.blue(), src.alpha());
			}
		}

		return SafeImagePointer(new SafeImage(resImage.convertToFormat(initialFormat)));
	}

	const QString& $Name$Filter::getName() const
	{
		return Constants::FilterName;
	}

	const char* $Name$Filter::getIdName() const
	{
		return Constants::FilterIdName;
	}

	void $Name$Filter::setParameters(const Data::NamedVariantMap& params)
	{
		/*!
		 *\note Example:

		// Radius
		auto it = params.find("radius");
		if (it != params.end())
			mRadius = (*it).asVariant().toInt();

		 */
	}

	void $Name$Filter::setDefaultParameters()
	{
		/*!
	   *\note Example:

	  mRadius = 5;

	   */
	}

	const Data::NamedVariantMap& $Name$Filter::getParameters() const
	{
		static Data::NamedVariantMap params;

		/*!
	   *\note Example:

	  params["radius"] = Data::ExtendedVariant::toExtendedVariant(QVariant(mRadius));

	   */

		return params;
	}
} // namespace Filters