/*!
 *\file $name$filtercustomizationwidget.cpp
 *\brief This file contains implementation of $NameTitle$ filter customization widget
 */

#include "$name$filtercustomizationwidget.h"
#include "../Filters/$name$filter.h"

namespace Ui
{
	/**
	 public:
	 */
	$Name$FilterCustomizationWidget::$Name$FilterCustomizationWidget(QWidget *parent /* = NULL */)
		: AbstractCustomizationWidget(parent),
		  mIgnoreChanges(false),
			mParams(Data::NamedVariantMap())
	{
		setupUi(this);
		setFixedSize(size());

		hsRadius->setRange(0, 100);

		connect(hsRadius,  SIGNAL( valueChanged(int) ), this, SLOT( onRadiusChanged(int) ));
	}

	/**
	 public slots:
	 */
	void $Name$FilterCustomizationWidget::setParams(const Data::NamedVariantMap& params)
	{
		mIgnoreChanges = true;

		if (params.find("Radius") != params.end())
		{
			hsRadius->setValue(params["Radius"].asVariant().toInt());
		}

		mParams = params;
		mIgnoreChanges = false;
	}

	/**
	 private slots:
	 */

	void $Name$FilterCustomizationWidget::onRadiusChanged(int value)
	{
		lRadius->setText(QVariant(value).toString());

		if (mIgnoreChanges)
			return;

		mParams["Radius"] = Data::ExtendedVariant::toExtendedVariant(QVariant(value));
		emit paramsChanged(mParams);
	}

} // namespace Ui