"""
    PIP filter base code generator by Lev Panov
"""

"""
    We need to do the following in order to add a new filter stub into PIP:
        add     Filters/somefilter.{h,cpp}
        modify  Ui/imageprocessingwidget.{h,cpp}
        add     Ui/somefiltercustomization{h,cpp}
        {TBD}
"""

"""
    Definition format is based on xml and goes like:
    <filter name=some customizer=true>
    </filter>
    {TBD}
"""


import sys
import os
from xml.dom.minidom import parse
import stringtemplate3


# Overwrite or preserve existing files
overwrite = True
# Prefixes, tag comment format
pip_prefix = './'
templates_prefix = ''.join([pip_prefix, 'tools/templates/'])
tag = '//! Warning: do not edit this line!!! '

NameTitle, Name, name, NAME = '', '', '', ''


mapping_imageprocessingwidget_hpp =\
    {'WidgetForwardDecl'        : 'class $Name$FilterCustomizationWidget;$end$',
     'TreeWidgetItem'           : '//! $NameTitle$ filter item\nQTreeWidgetItem* m$Name$;\n$end$',
     'OnParamsChanged'          : '/*!\n *\\brief Handle $NameTitle$ filter params change\n *\\param params New params\n */\nvoid on$Name$FilterParamsChanged(const Data::NamedVariantMap& params);\n$end$',
     'CustomizationWidgetPtr'   : '//! $NameTitle$ filter customization widget instance\n$Name$FilterCustomizationWidget *m$Name$FilterCustomizer;$end$'
    }

mapping_imageprocessingwidget_cpp = \
    {'CustomizationWidgetInclude'        :  '#include "$name$filtercustomizationwidget.h"$end$',
     'CustomizableFilterInclude'         :  '#include "../Filters/$name$filter.h"$end$',
     'CustomizerCreateInstance'          :  'm$Name$FilterCustomizer(new $Name$FilterCustomizationWidget(this)),$end$',
     'CustomizerSetDefaultParams'        :  '$Name$Filter $name$Filter;\nm$Name$FilterCustomizer->setParams($name$Filter.getParameters());\n$end$',
     'OnParamsChangedImpl'               :  'void ImageProcessorWidget::on$Name$FilterParamsChanged(const Data::NamedVariantMap& params)\n'
                                            '{\n'
                                            '\t// Make undo action\n'
                                            '\tManagers::ImageContextsManager manager;\n'
                                            '\tInterfaces::IImageContextPointer context = manager.getContext(this);\n'
                                            '\tQ_ASSERT(context);\n'
                                            '\tcontext->undo();\n'
                                            '\n'
                                            '\t// Apply filter with new params\n'
                                            '\tapplyFilter("$NameTitle$", params);\n'
                                            '}\n$end$',
     'ConnectCustomizerSignalToSlot'     :  'connect(m$Name$FilterCustomizer, SIGNAL( paramsChanged(Data::NamedVariantMap) ), this, SLOT( on$Name$FilterParamsChanged(Data::NamedVariantMap) ));$end$',
     'CreateFilterTreeNode'              :  'mBuiltinItems.m$Name$ = new QTreeWidgetItem(mOtherTop, QStringList(tr("$NameTitle$")));$end$',
     'CreateFilterInstanceUsingFactory'  :  'else if (name == tr("$NameTitle$"))\n'
                                            '{\n'
                                            '\tfilter = factory.create$Name$Filter(params);\n'
                                            '\tmCurrentFilterCustomizer = m$Name$FilterCustomizer;\n'
                                            '}$end$'
    }


mapping_builtinfiltersfactory_hpp = \
    {'FactoryMethodDecl'                 :  '//! $NameTitle$ filter\n'
                                            'Interfaces::IFilterPointer create$Name$Filter(const Data::NamedVariantMap& params = Data::NamedVariantMap()) const;\n$end$'
    }

mapping_builtinfiltersfactory_cpp = \
    {'IncludeFilterHeader'               :  '#include "$name$filter.h"$end$',
     'FactoryMethodImpl'                 :  'IFilterPointer BuiltinFiltersFactory::create$Name$Filter(const Data::NamedVariantMap& params/* = Data::NamedVariantMap() */) const\n'
                                            '{\n'
                                            '\treturn createCustomizableFilter<$Name$Filter>(params);\n'
                                            '}\n$end$'
    }

mapping_mainwindow_cpp = \
    {
        'QActionDef'                     :  '//! $NameTitle$ filter action\n'
                                            'QAction *m$Name$FilterAction;$end$',
        'QActionCreate'                  :  'mUi->mBuiltinFilters->m$Name$FilterAction = new QAction(tr("$NameTitle$"), this);\n'
                                            'mUi->mBuiltinFilters->m$Name$FilterAction->setToolTip(tr("$NameTitle$ filter"));\n'
                                            'mUi->mBuiltinFilters->m$Name$FilterAction->setStatusTip(tr("$NameTitle$ filter"));\n'
                                            'mUi->mBuiltinFilters->m$Name$FilterAction->setWhatsThis(tr("$NameTitle$ filter"));\n'
                                            'mUi->mBuiltinFilters->m$Name$FilterAction->setShortcut(QKeySequence("Ctrl+Shift+7"));\n$end$',
        'QActionAddToMenu'               :  'mUi->mMenus->mFiltersMenu->addAction(mUi->mBuiltinFilters->m$Name$FilterAction);$end$',
        'QActionAddToDispatcher'         :  'dispatcher.addFilterAction(mUi->mBuiltinFilters->m$Name$FilterAction);$end$',
        'QActionEnable'                  :  'mUi->mBuiltinFilters->m$Name$FilterAction->setEnabled(enable);$end$'
    }


# More mappings go here


def exists(filename):
    try:
        with open(filename):
            return True
    except IOError:
        return False


def read(filename):
    try:
        with open(filename) as f:
            content = f.read()
            return content
    except IOError:
        raise IOError


def add_template(name, destfilename, templatefilename):
    destpath = os.path.abspath(''.join([pip_prefix, destfilename]))

    if not overwrite:
        assert not exists(destpath)

    templatepath = os.path.abspath(''.join([templates_prefix, templatefilename]))
    assert exists(templatepath)

    content = read(templatepath)

    content = stringtemplate3.StringTemplate(content)
    content['name'] = name
    content['Name'] = Name
    content['NAME'] = NAME
    content['NameTitle'] = NameTitle

    with open(destpath, "w") as cpp:
        cpp.write(content.toString())


def preprocess(content):
    pos = 0
    oldpos = pos
    parts = []
    while 1:
        pos = content.find(tag, pos)
        if pos == -1:
            break
        parts.append(content[oldpos:pos])
        pos += len(tag)
        oldpos = pos

    parts.append(content[oldpos:])
    return ''.join(parts)


def get_mapping(name, mapping):
    for key, value in mapping.iteritems():
        value = stringtemplate3.StringTemplate(value)
        value['name'] = name
        value['Name'] = Name
        value['NAME'] = NAME
        value['NameTitle'] = NameTitle
        value['end'] = ''.join(['\n', tag, '$', key, '$']) # Tag rebirth
        mapping[key] = value.toString()

    return mapping


def apply_mapping(content, mapping):
    content = stringtemplate3.StringTemplate(content)
    for key, value in mapping.iteritems():
        content[key] = value

    return content.toString()


def patch(name, sourcefilename, mapping):
    path = os.path.abspath(''.join([pip_prefix, sourcefilename]))

    assert exists(path)

    content = read(path)
    content = preprocess(content)
    content = apply_mapping(content, get_mapping(name, mapping))

    with open(path, "w") as out:
        out.write(content)


def generate_filter_stub(filename):
    assert exists(filename)
    desc = parse(filename)
    filter_node = desc.getElementsByTagName("filter")[0]

    if filter_node.hasAttribute('name'):
        nameRaw = filter_node.getAttribute('name')
    else:
        raise EnvironmentError

    if filter_node.hasAttribute('customizer'):
        hasCustomizer = bool(filter_node.getAttribute('customizer'))
    else:
        hasCustomizer = False

    global NameTitle
    global Name
    global name
    global NAME

    NameTitle = ''.join([nameRaw[:1].upper(), nameRaw[1:]])
    Name = NameTitle.replace(' ', '')
    name = Name.lower()
    NAME = name.upper()

    print NameTitle, Name, name, NAME

    #return

    add_template(name, 'src/Filters/%sfilter.h' % name, 'implementationtemplate.h')
    add_template(name, 'src/Filters/%sfilter.cpp' % name, 'implementationtemplate.cpp')
    patch(name, 'src/Ui/imageprocessorwidget.h', mapping_imageprocessingwidget_hpp)
    patch(name, 'src/Ui/imageprocessorwidget.cpp', mapping_imageprocessingwidget_cpp)
    patch(name, 'src/Filters/builtinfiltersfactory.h', mapping_builtinfiltersfactory_hpp)
    patch(name, 'src/Filters/builtinfiltersfactory.cpp', mapping_builtinfiltersfactory_cpp)
    patch(name, 'src/Ui/mainwindow.cpp', mapping_mainwindow_cpp)
    add_template(name, 'src/Forms/%sfiltercustomizationwidgetform.ui' % name, 'customizationwidgetformtemplate.ui')
    add_template(name, 'src/Ui/%sfiltercustomizationwidget.h' % name, 'customizationwidgettemplate.h')
    add_template(name, 'src/Ui/%sfiltercustomizationwidget.cpp' % name, 'customizationwidgettemplate.cpp')


def main():
    if len(sys.argv) != 2:
        print "Usage: %1 <filter_desc.xml>", sys.argv[0]
        return

    generate_filter_stub(sys.argv[1])


if __name__ == "__main__":
    main()